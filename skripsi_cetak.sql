-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 09, 2021 at 06:45 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skripsi_cetak`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_barang` int(11) NOT NULL,
  `nama_barang` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `kategori_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `nama_barang`, `deskripsi`, `gambar`, `kategori_id`) VALUES
(12, 'Banner', 'Bahan 300gr\r\n18.000/meter', 'dfc74084712ea9855ab56c8a1286d7cf.png', 3),
(13, 'Brosur', 'Ap150\r\nHVS\r\nMinimal 1rim (500lembar)', '5db44e57f341bbcbd82c4c94663bfe05.png', 5),
(14, 'Roll Banner', 'Bahan Albatros', 'fba2913a7a712922699a3f88f16029bf.jpg', 3),
(18, 'Y Banner', 'Bahan banner 300gr', '0d927743f07e6751cb077b16177ec3e5.jpg', 3),
(19, 'X Banner', 'Bahan Banner 300gr', '7237442c523b626478ade55e0ea10bd1.jpg', 3),
(20, 'Kartu Nama ', '1 Box isi 100pcs', 'd20e921d6f13827ce11e9c760fa29d62.png', 1),
(21, 'Buku Yasin', 'Minimal pesan 50pcs', 'e5d217e9f2f7e8fe685175eba5ff6558.jpg', 4),
(22, 'Nota Kwitansi', 'Minimal 1 Rim\r\nBisa request ukuran', 'b1e9d00becc39b452b6e32b9c8acf31c.jpg', 4),
(23, 'Gantungan Kunci Pin', 'Minimal Pesan 50pcs', '8c9c8d2a120a441f2c36c8706fbcf4f0.jpg', 2),
(24, 'Pin', 'Minimal Pesan 50pcs', 'ac24d058df90ce810c6121baa1568738.jpg', 2);

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Card'),
(2, 'Pin'),
(3, 'Banner'),
(4, 'Buku'),
(5, 'Brosur');

-- --------------------------------------------------------

--
-- Table structure for table `master_harga`
--

CREATE TABLE `master_harga` (
  `id_harga` int(11) NOT NULL,
  `barang_id` int(10) UNSIGNED NOT NULL,
  `ukuran` varchar(100) NOT NULL,
  `harga` double NOT NULL,
  `harga_member` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `master_harga`
--

INSERT INTO `master_harga` (`id_harga`, `barang_id`, `ukuran`, `harga`, `harga_member`) VALUES
(2, 14, '80x180', 290000, 280000),
(3, 14, '60x160', 250000, 240000),
(4, 13, '10R', 20000, 19000),
(5, 13, '10R Jumbo', 25000, 24000),
(6, 18, '60x160', 75000, 74000),
(7, 18, '80x180', 90000, 80000),
(8, 19, '60x160', 75000, 74000),
(9, 19, '80x180', 90000, 85000),
(10, 20, '9 cm x 5,5 cm (1 muka) ', 35000, 32000),
(11, 20, '9 cm x 5,5 cm (2 muka) ', 60000, 55000),
(12, 21, 'A6 (dengan arti)', 5000, 4000),
(13, 21, 'A6 (arab saja)', 4500, 4000),
(14, 23, '4,4cm', 3500, 3000),
(15, 23, '5,4cm', 5000, 4500),
(16, 24, '4,4cm', 3500, 3000),
(17, 24, '5,4cm', 4000, 3500),
(18, 22, 'f4 (tanpa rangkap)', 150000, 145000),
(19, 22, 'f4 (2 rangkap)', 230000, 220000),
(20, 22, 'f4 (3 rangkap)', 340000, 320000),
(21, 22, 'a5 (tanpa rangkap)', 150000, 145000),
(22, 22, 'a5 (2 rangkap)', 230000, 220000),
(23, 22, 'a5 (3 rangkap)', 340000, 320000),
(24, 22, 'a6 (tanpa rangkap)', 150000, 140000),
(25, 22, 'a6 (2 rangkap)', 230000, 210000),
(26, 22, 'a6 (3 rangkap)', 340000, 320000),
(27, 22, 'request ukuran (tanpa rangkap)', 150000, 140000),
(28, 22, 'request ukuran (2 rangkap)', 230000, 210000),
(29, 22, 'request ukuran (3 rangkap)', 340000, 310000);

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id_pelanggan` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `nomor_hp` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `foto_ktp` varchar(100) NOT NULL,
  `swafoto` varchar(100) NOT NULL,
  `tipe_pelanggan` enum('Reguler','Member') NOT NULL DEFAULT 'Reguler'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`id_pelanggan`, `username`, `password`, `nama_lengkap`, `email`, `nomor_hp`, `alamat`, `foto_ktp`, `swafoto`, `tipe_pelanggan`) VALUES
(2, 'pelanggan', '7f78f06d2d1262a0a222ca9834b15d9d', 'Pelanggan 1', 'pelanggan@gmail.com', '0987654321', 'Jombang', 'ktp_d2cc08ca1de9496af5474f0b2883dab1.png', 'swafoto_d2cc08ca1de9496af5474f0b2883dab1.png', 'Reguler'),
(4, 'andikyou', '7725f528c52c9ece01193cfa56c0ca35', 'Andik Yulianto', 'andikyou@gmail.com', '081568890090', 'Kabuh- Jombang', 'ktp_e535009bbd6db6aadf444be779b43e7d.png', 'swafoto_e535009bbd6db6aadf444be779b43e7d.png', 'Reguler'),
(5, 'ristaarya', 'fc9d513c42a54e5560fa99bd62854a4a', 'rista arya agustina', 'ristaar@gmail.com', '081568890090', 'mojokerto ', 'ktp_0fb4316f2b2dcca60892bae15771c5e6.png', 'swafoto_0fb4316f2b2dcca60892bae15771c5e6.png', 'Reguler'),
(6, 'nilarahma', 'e7542a2c91700b0fd88ce7b8a5a5d2d4', 'nila rahmawati', 'nilaaa@gmail.com', '081568890090', 'jogoroto jombang', 'ktp_e988b2ef414a7c8823087f6293e3fa4b.png', 'swafoto_e988b2ef414a7c8823087f6293e3fa4b.png', 'Reguler'),
(7, 'nadhiroh', 'ad8fe6d175639cbf173552900003c1bb', 'qoirotun nadhiroh', 'nadhirohh@gmail.com', '081568890090', 'mojokerto', 'ktp_8c44cf818853f906a94dc70d5b35c97f.png', 'swafoto_8c44cf818853f906a94dc70d5b35c97f.png', 'Member'),
(8, 'Hwaweng', '97f164b4c1ad3bbf876b4b322ea1a68b', 'Hendro sugiono', 'hendrowaweng94@gmail.com', '082257670033', 'Jl flamboyan no 143c candimulyo jombang', 'ktp_80b45ee1b21a60ded219440eda378da9.jpg', 'swafoto_80b45ee1b21a60ded219440eda378da9.jpg', 'Reguler');

-- --------------------------------------------------------

--
-- Table structure for table `pemesanan`
--

CREATE TABLE `pemesanan` (
  `id_pemesanan` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `barang_id` int(10) UNSIGNED NOT NULL,
  `jumlah` int(11) NOT NULL DEFAULT 1,
  `ukuran` varchar(50) NOT NULL,
  `total_harga` double NOT NULL,
  `no_wa` varchar(20) NOT NULL,
  `catatan` text DEFAULT NULL,
  `pelanggan_id` int(10) UNSIGNED NOT NULL,
  `desain` varchar(255) DEFAULT NULL,
  `biaya_tambahan` double NOT NULL DEFAULT 0,
  `sudah_bayar` double NOT NULL DEFAULT 0,
  `kode` varchar(255) NOT NULL,
  `status` varchar(50) NOT NULL,
  `karyawan_id` int(10) UNSIGNED DEFAULT NULL,
  `bukti` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pemesanan`
--

INSERT INTO `pemesanan` (`id_pemesanan`, `tanggal`, `barang_id`, `jumlah`, `ukuran`, `total_harga`, `no_wa`, `catatan`, `pelanggan_id`, `desain`, `biaya_tambahan`, `sudah_bayar`, `kode`, `status`, `karyawan_id`, `bukti`) VALUES
(23, '2020-11-25', 14, 20, 'A4', 140000, '080716582451', NULL, 2, NULL, 5000, 0, 'P25112020050158', 'Selesai', 4, 'c28857bdde5bb02cccb0ce00f68956c8.jpg'),
(29, '2020-12-12', 13, 1, '10R', 20000, '097867565432', NULL, 0, NULL, 5000, 0, 'P12122020045204', 'Selesai', 4, NULL),
(30, '2020-12-18', 13, 1, '10R', 20000, '-', NULL, 2, NULL, 5000, 0, 'P18122020082330', 'Selesai', 4, NULL),
(31, '2020-12-19', 13, 12, '10R', 240000, '0987654321', 'cepet ya', 2, NULL, 0, 0, 'P19122020081227', 'dibuat', NULL, NULL),
(32, '2021-01-09', 18, 2, '60x160', 150000, '081568890090', 'saya belum ada desain tolong didesainkan nanti dikirim ke nomor wa yaa terimakasih tolong fast respons nya :)', 4, NULL, 10000, 0, 'P09012021201940', 'Di Proses', 7, 'c48720f481fb9ede44a6cb3a6bd47901.png'),
(33, '2021-01-09', 20, 1, '9 cm x 5,5 cm (2 muka) ', 60000, '081568890090', 'yang ini sudah ada desain besok saya ambil', 4, '0b9460fbe94eceee193787e6808f904a.png', 0, 0, 'P09012021202047', 'dibuat', NULL, NULL),
(34, '2021-01-17', 21, 100, 'A6 (dengan arti)', 500000, '081568890090', 'saya belum ada desain \r\ndisini hanya saya lampirkan foto untuk cover', 5, '110f8c7754347ad34b2648a6f2b62340.png', 0, 0, 'P17012021085811', 'dibuat', NULL, NULL),
(35, '2021-01-17', 20, 1, '9 cm x 5,5 cm (1 muka) ', 35000, '081568890090', 'belum ada desain mohon didesainkan', 5, NULL, 15000, 0, 'P17012021085854', 'Selesai', 8, 'b14f7c80f86580b72b976cd3d8c7f0ea.jpg'),
(36, '2021-01-17', 19, 2, '60x160', 150000, '081568890090', 'sudah ada desain tinggal cetak', 6, '38e4fcf5527dac482a6cb86280bc2849.png', 0, 0, 'P17012021092351', 'Selesai', 8, '54291ff7db3ab8905eb704b524e1cd0e.jpeg'),
(37, '2021-01-17', 21, 100, 'A6 (dengan arti)', 500000, '081568890090', 'saya belum punya desain disini hanya saya lampirkan foto untuk cover', 7, '42442ea4fc7457ea480c0992ef6cb93d.png', 30000, 0, 'P17012021101500', 'Di Proses', 7, 'c5429e910aa668280b9c33e101fac191.png'),
(38, '2021-01-28', 14, 1, '80x180', 290000, '', '', 4, NULL, 0, 90000, 'P28012021181016', 'Selesai', 11, NULL),
(39, '2021-02-05', 23, 99, '5,4cm', 495000, '082257670033', '', 8, NULL, 15000, 0, 'P05022021195721', 'Selesai', 11, NULL),
(40, '2021-02-06', 20, 2, '9 cm x 5,5 cm (1 muka) ', 70000, '081568890090', '', 4, '35b010d9fba984c080690f43d636e098.png', 10000, 0, 'P06022021114858', 'Selesai', 11, '2ebad87b6b4c0c70693ff2a645ab6878.png');

-- --------------------------------------------------------

--
-- Table structure for table `status_pemesanan`
--

CREATE TABLE `status_pemesanan` (
  `id_status` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `pemesanan_id` int(10) UNSIGNED NOT NULL,
  `user` varchar(50) NOT NULL,
  `tanggal` datetime NOT NULL,
  `catatan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `status_pemesanan`
--

INSERT INTO `status_pemesanan` (`id_status`, `status`, `pemesanan_id`, `user`, `tanggal`, `catatan`) VALUES
(23, 'dibuat', 23, 'Pelanggan 1', '2020-11-25 05:01:58', NULL),
(24, 'Di Proses', 23, 'Ntis', '2020-12-12 04:16:07', 'Sedang di desain'),
(25, 'Di Proses', 23, 'Ntis', '2020-12-12 04:27:18', 'Finishing'),
(26, 'dibuat', 29, 'Jono', '2020-12-12 04:52:04', NULL),
(27, 'Selesai', 23, 'Ntis', '2020-12-14 15:55:23', 'mari boss'),
(29, 'dibuat', 30, 'Pelanggan 1', '2020-12-18 08:23:30', NULL),
(30, 'Di Proses', 29, 'Administrator', '2020-12-18 13:39:35', 'gass'),
(33, 'Di Proses', 30, 'Administrator', '2020-12-18 15:52:32', '-'),
(34, 'Selesai', 30, 'Ntis', '2020-12-18 22:44:19', 'Syudah'),
(35, 'Selesai', 29, 'Ntis', '2020-12-18 22:44:36', 'Selesai'),
(36, 'dibuat', 31, 'Pelanggan 1', '2020-12-19 08:12:33', NULL),
(37, 'dibuat', 32, 'Andik Yulianto', '2021-01-09 20:19:40', NULL),
(38, 'dibuat', 33, 'Andik Yulianto', '2021-01-09 20:20:47', NULL),
(39, 'Di Proses', 32, 'Titis Oktalia Reptanti', '2021-01-09 20:22:33', 'Desain'),
(40, 'Di Proses', 32, 'Fitri Ning Tutiul Qoniah', '2021-01-17 07:12:37', 'desain'),
(41, 'dibuat', 34, 'rista arya agustina', '2021-01-17 08:58:11', NULL),
(42, 'dibuat', 35, 'rista arya agustina', '2021-01-17 08:58:54', NULL),
(43, 'dibuat', 36, 'nila rahmawati', '2021-01-17 09:23:51', NULL),
(44, 'Di Proses', 36, 'Titis Oktalia Reptanti', '2021-01-17 09:24:27', 'proses'),
(45, 'Di Proses', 36, 'M. Falah', '2021-01-17 09:45:24', 'ripping'),
(46, 'dibuat', 37, 'qoirotun nadhiroh', '2021-01-17 10:15:00', NULL),
(47, 'Di Proses', 37, 'Titis Oktalia Reptanti', '2021-01-17 10:21:50', 'desain'),
(48, 'Di Proses', 37, 'Fitri Ning Tutiul Qoniah', '2021-01-17 10:23:07', 'proses'),
(49, 'Selesai', 36, 'M. Falah', '2021-01-17 10:26:42', 'selesai'),
(50, 'Di Proses', 35, 'Titis Oktalia Reptanti', '2021-01-26 16:12:00', 'desain'),
(51, 'Di Proses', 37, 'Fitri Ning Tutiul Qoniah', '2021-01-26 16:32:42', 'desain'),
(52, 'dibuat', 38, 'Andik Yulianto', '2021-01-28 18:10:16', NULL),
(53, 'Selesai', 35, 'M. Falah', '2021-02-05 16:40:44', 'sudah selesai bisa diambil :)'),
(54, 'dibuat', 39, 'Hendro sugiono', '2021-02-05 19:57:21', NULL),
(55, 'Di Proses', 39, 'Titis Oktalia Reptanti', '2021-02-05 20:24:22', 'di desain dulu ya mas :D'),
(56, 'Di Proses', 39, 'Arif Kurniawan', '2021-02-05 22:07:08', 'cetak'),
(57, 'Di Proses', 39, 'Arif Kurniawan', '2021-02-05 22:07:25', 'finishing'),
(58, 'Selesai', 39, 'Arif Kurniawan', '2021-02-05 22:08:23', 'sudah selesai bisa diambil'),
(59, 'dibuat', 40, 'Andik Yulianto', '2021-02-06 11:48:58', NULL),
(60, 'Di Proses', 40, 'Titis Oktalia Reptanti', '2021-02-06 11:50:41', 'desain'),
(61, 'Di Proses', 40, 'Arif Kurniawan', '2021-02-06 11:52:30', 'cetak'),
(62, 'Selesai', 40, 'Arif Kurniawan', '2021-02-06 11:52:59', 'selesai bisa diambil'),
(63, 'Di Proses', 38, 'Titis Oktalia Reptanti', '2021-02-09 12:29:54', '-'),
(64, 'Selesai', 38, 'Arif Kurniawan', '2021-02-09 12:36:06', 'sudah pak');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jenis_kelamin` enum('Laki-Laki','Perempuan') NOT NULL,
  `alamat` text NOT NULL,
  `no_telp` varchar(45) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `level_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `nama`, `jenis_kelamin`, `alamat`, `no_telp`, `foto`, `level_user`) VALUES
(4, 'titisor', 'oktaliaa.titis@gmail.com', '02bbf9d7512a28909f726a3e659d5d5c', 'Titis Oktalia Reptanti', 'Perempuan', 'Kabuh Jombang', '081515850067', '79a8afa51f387f8d23b1d5054f1cd113.png', 1),
(6, 'owner', 'owner@mail.com', '72122ce96bfec66e2396d2e25225d70a', 'Owner R-Tech', 'Perempuan', 'Jombang', '-', '', 3),
(7, 'fitrining', 'fitrining@gmail.com', 'c09320b6bd917708f05a3a2be1e5d36d', 'Fitri Ning Tutiul Qoniah', 'Perempuan', 'Perak Jombang', '083856985086', '', 2),
(8, 'fal', 'falah@gmail.com', 'cd163c8ced276359d763ec1a31301a42', 'M. Falah', 'Laki-Laki', 'Mojoagung Jombang', '083888999777', '', 2),
(9, 'anggisep', 'anggisep@gmail.com', '4885d095db8bf9c0a21fef43ce625e3f', 'Anggi Septa Mujahadah', 'Perempuan', 'Perak Jombang', '081515876234', '', 2),
(10, 'herus', 'hruu@gmail.com', '8c1bbe1af55238f4ed6b804699979d17', 'Heru S.', 'Laki-Laki', 'Jombang', '089090456789', '', 2),
(11, 'Arifff', 'arifkurniawan@gmail.com', '85a7885fa5875ee82c42933ab56589d3', 'Arif Kurniawan', 'Laki-Laki', 'Magelang', '087577345780', '', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `master_harga`
--
ALTER TABLE `master_harga`
  ADD PRIMARY KEY (`id_harga`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id_pelanggan`);

--
-- Indexes for table `pemesanan`
--
ALTER TABLE `pemesanan`
  ADD PRIMARY KEY (`id_pemesanan`);

--
-- Indexes for table `status_pemesanan`
--
ALTER TABLE `status_pemesanan`
  ADD PRIMARY KEY (`id_status`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `master_harga`
--
ALTER TABLE `master_harga`
  MODIFY `id_harga` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pemesanan`
--
ALTER TABLE `pemesanan`
  MODIFY `id_pemesanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `status_pemesanan`
--
ALTER TABLE `status_pemesanan`
  MODIFY `id_status` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
