USER:

owner, karyawan, cs

Flow:
- Cek status pesanan @if(pesanan = selesai) ada sms/wa otomatis 
- katalog harga
- target (bonus kinerja)
- barcode 

ref: 
percetakanbandung.com

Judul: rancang bangun sistem informasi tracking dan pelayanan percetakan Rtech(SITPPER Rtech) berbasis web menggunakan ci 

Permasalahan : sebelum adanya sistem ini customer service kewalahan membalas setiap chat yang masuk dari banyaknya pelanggan dengan adanya sistem ini akan meringankan pekerjaan customer service dan memudahkan pelanggan mengetahui status pesanan. 

Berbentuk web, ada kategori untuk pelanggan dan untuk customer service/karyawan, khusus untuk menu cs/karyawan ada id+password untuk login. 

Fitur : 
- tracking : pelanggan biasanya spam chat untuk menanyakan status pesanan, setiap hari bahkan ada yang sehari beberapa kali spam chat, jadi dibuat tracking status pesanan, pelanggan yang datang mendapat id dan mengisikan id tersebut ke dalam web untuk mengetahui status pesanan nya, jika pesanan sudah jadi akan ada sms/wa otomatis pemberitahuan kepada pelanggan bahwa pesanan nya sudah bisa diambil, jadi customer service tidak perlu membalas satu persatu chat pertanyaan status tersebut setiap hari nya, sehingga memudahkan kedua belah pihak 
- daftar harga : customer service selalu mengulang menjelaskan kepada satu persatu pelanggan yg bertanya harga tiap item, maka dari itu dibuat daftar harga tiap item, pelanggan bisa melihat langsung di dalam web, misalkan harga brosur 1rim berapa 
- manajemen kinerja karyawan : disini pemilik telah mencantumkan gaji dan bonus per desain, jadi karyawan menginput sendiri pada hari itu telah mengerjakan berapa desain maka akan keluar otomatis gaji dan bonusnya berapa, jadi karyawan dapat mengatahui rincian gaji per bulan mereka dan pemilik tidak perlu mengira2 bonus untuk karyawan. Karena biasanya manajemen yang tidak tertata membuat gaji dan bonus karyawan tidak tentu