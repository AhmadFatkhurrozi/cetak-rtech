<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_user');
        $this->load->helper(array('form', 'url'));
    }

    public function is__login()
    {
        if ($this->session->userdata('auth_id') =='' and $this->session->userdata('auth_username') =='') {

            $this->session->set_flashdata('msg', 
                '<p class="text-danger" style="margin-top: 10px;">
                    <span><i class="fa fa-times"></i>Silahkan Login Terlebih Dahulu</span>
                </p');

            redirect(base_url('admin'));
        }
    }

    public function kembali()
    {

        return $this->input->server('HTTP_REFERER');
    }

    function logout(){
        $this->session->sess_destroy();
        redirect(base_url('admin'));
    }

    function index()
    {
        $this->is__login();
        
        $data['title1'] = 'Manage User';
        $data['title2'] = 'User';

        $this->load->view('pengguna/main', $data);
    }

    public function editMyprofil()
    {        
        $data['title1'] = 'My Profile';
        $data['title2'] = 'Edit Profile';
        $data['user']	= $this->db->where('id', $this->session->userdata('auth_id'))->get('users')->row();

        $this->load->view('pengguna/editProfil', $data);
    }

    public function data()
    {
        header('Content-Type: application/json');
        echo json_encode($this->M_user->getJson($this->input->post()));
    }

    public function form()
    {
        $data['user'] = (!empty($this->input->post('id'))) ? $this->db->where('id', $this->input->post('id'))->get('users')->row() : "";
        $content = $this->load->view('pengguna/formAdd', $data, TRUE);

        $callback = ['status'=>'success', 'content'=>$content];
        echo json_encode($callback);
    }

    public function detail()
    {
        $data['title']= 'Detail';
        $data['user'] = $this->db->where('id', $this->input->post('id'))->get('users')->row();

        $content = $this->load->view('pengguna/modal',$data, TRUE);
        
        $callback = ['status'=>'success', 'content'=>$content];
        echo json_encode($callback);
    }

    public function save()
    {
        $id = (!empty($this->input->post('id'))) ? $this->input->post('id') : ""; 

        $data = array(
            'email'         => $this->input->post('email'),
            'username'      => $this->input->post('username'),
            'password'      => md5($this->input->post('username')),
            'nama'          => $this->input->post('nama'),
            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
            'no_telp'       => $this->input->post('no_telp'),
            'alamat'        => $this->input->post('alamat'),
            'level_user'    => $this->input->post('level'),
        );

        if ($id == '') {
            $this->db->insert('users', $data);   
        }else{
            $this->db->where('id', $id);
            $this->db->update('users',$data);
        }

        $this->session->set_flashdata('msg', 
                '<div class="alert alert-success" role="alert" style="margin-top: 10px;">
                    <span><i class="fa fa-check"></i> Berhasil menyimpan data!</span>
                </div>');

        redirect(base_url('user'));
    }

    public function saveMyprofil()
    {
    	$id = (!empty($this->input->post('id'))) ? $this->input->post('id') : ""; 

    	$config['upload_path']      = './dist/upload/';
        $config['allowed_types']    = 'jpg|png|jpeg|gif';
        $config['file_name']        = md5(date('YmdHis'));
        $config['max_size']         = '4056';
        $config['overwrite']        = TRUE;
        
        $this->load->library('upload', $config);

        $data = array(
            'email'         => $this->input->post('email'),
            'nama'          => $this->input->post('nama'),
            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
            'no_telp'       => $this->input->post('no_telp'),
            'alamat'        => $this->input->post('alamat'),
        );
    	
    	if ($this->input->post('password') != '') {
    		$data['password'] = md5($this->input->post('password'));
    	}

    	if ($this->upload->do_upload('photo')) {
            $this->upload->do_upload('photo');
            $data['foto'] = $this->upload->data("file_name");
        }

        $this->db->where('id', $id);
        $this->db->update('users',$data);

        $this->session->set_flashdata('msg', 
                '<div class="alert alert-success" role="alert" style="margin-top: 10px;">
                    <span><i class="fa fa-check"></i> Berhasil menyimpan data!</span>
                </div>');

        redirect(base_url('admin'));
    }

    public function hapus()
    {
        $this->db->where('id', $this->input->post('id'))->delete('users');
        
        $callback = ['status'=>'success', 'message'=>"hapus berhasil!"];
        echo json_encode($callback);
    }
}
