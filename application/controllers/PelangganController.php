<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PelangganController extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_user');
        $this->load->model('M_pelanggan');
        $this->load->helper(array('form', 'url'));
    }

    public function is__login()
    {
        if ($this->session->userdata('auth_id') =='' and $this->session->userdata('auth_username') =='') {

            $this->session->set_flashdata('msg', 
                '<p class="text-danger" style="margin-top: 10px;">
                    <span><i class="fa fa-times"></i>Silahkan Login Terlebih Dahulu</span>
                </p');

            redirect(base_url('admin'));
        }
    }

    public function kembali()
    {

        return $this->input->server('HTTP_REFERER');
    }

    function logout(){
        $this->session->sess_destroy();
        redirect(base_url('admin'));
    }

    function index()
    {
        $this->is__login();
        
        $data['title1'] = 'Data Pelanggan';
        $data['title2'] = 'Pelanggan';

        $this->load->view('pelanggan/main', $data);
    }

    public function data()
    {
        header('Content-Type: application/json');
        echo json_encode($this->M_pelanggan->getJson($this->input->post()));
    }

    public function detail()
    {
        $data['title']= 'Detail';
        $data['pelanggan'] = $this->db->where('id_pelanggan', $this->input->post('id'))->get('pelanggan')->row();

        $content = $this->load->view('pelanggan/modal',$data, TRUE);
        
        $callback = ['status'=>'success', 'content'=>$content];
        echo json_encode($callback);
    }

    public function member()
    {
        $data = array(
	        'tipe_pelanggan' => "Member"
		);

		$this->db->where('id_pelanggan', $this->input->post('id'));
		$this->db->update('pelanggan', $data);
        
        $callback = ['status'=>'success', 'message'=>"Update berhasil!"];
        echo json_encode($callback);
    }

    public function hapusmember()
    {
    	$data = array(
	        'tipe_pelanggan' => "Reguler"
		);

		$this->db->where('id_pelanggan', $this->input->post('id'));
		$this->db->update('pelanggan', $data);
        
        $callback = ['status'=>'success', 'message'=>"Update berhasil!"];
        echo json_encode($callback);
    }
}
