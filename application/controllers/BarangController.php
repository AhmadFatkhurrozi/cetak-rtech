<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BarangController extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_barang');
        $this->load->helper(array('form', 'url'));
    }

    public function is__login()
    {
        if ($this->session->userdata('auth_id') =='' and $this->session->userdata('auth_username') =='') {

            $this->session->set_flashdata('msg', 
                '<p class="text-danger" style="margin-top: 10px;">
                    <span><i class="fa fa-times"></i>Silahkan Login Terlebih Dahulu</span>
                </p');

            redirect(base_url('admin'));
        }
    }

    public function kembali()
    {
        return $this->input->server('HTTP_REFERER');
    }

    function logout(){
        $this->session->sess_destroy();
        redirect(base_url('admin'));
    }

    function index()
    {
        $this->is__login();
        
        $data['title1'] = 'Data Barang';
        $data['title2'] = 'Barang';

        $this->load->view('barang/main', $data);
    }

    public function data()
    {
        header('Content-Type: application/json');
        echo json_encode($this->M_barang->getJson($this->input->post()));
    }

    public function form()
    {
        $data['barang'] = (!empty($this->input->post('id'))) ? $this->db->where('id_barang', $this->input->post('id'))->get('barang')->row() : "";
        $data['kategori'] = $this->db->get('kategori')->result();
        $content = $this->load->view('barang/formAdd', $data, TRUE);

        $callback = ['status'=>'success', 'content'=>$content];
        echo json_encode($callback);
    }

    public function detail()
    {
        $data['title']= 'Detail';
        $data['barang'] = $this->db->join('kategori', 'kategori.id_kategori=barang.kategori_id', 'left')
        						->where('id_barang', $this->input->post('id'))->get('barang')->row();

        $content = $this->load->view('barang/modal',$data, TRUE);
        
        $callback = ['status'=>'success', 'content'=>$content];
        echo json_encode($callback);
    }

    public function save()
    {   

        $id = (!empty($this->input->post('id_barang'))) ? $this->input->post('id_barang') : ""; 

        $config['upload_path']      = './dist/img/barang/';
        $config['allowed_types']    = 'jpg|png';
        $config['file_name']        = md5(date('YmdHis'));
        $config['max_size']         = '4056';
        $config['overwrite']        = TRUE;
        
        $this->load->library('upload', $config);

        $data = array(
            'nama_barang'   => $this->input->post('nama_barang'),
            'kategori_id'   => $this->input->post('kategori'),
            'deskripsi'     => $this->input->post('deskripsi'),
        );

        if ($this->upload->do_upload('photo')) {
            $this->upload->do_upload('photo');
            $data['gambar'] = $this->upload->data("file_name");
        }

        if ($id == '') {
            $this->db->insert('barang', $data);   
        }else{
            $this->db->where('id_barang', $id);
            $this->db->update('barang',$data);
        }

        $this->session->set_flashdata('msg', 
                '<div class="alert alert-success" role="alert" style="margin-top: 10px;">
                    <span><i class="fa fa-check"></i> Berhasil menyimpan data!</span>
                </div>');

        redirect(base_url('barang'));
    }

    public function hapus()
    {
        $this->db->where('id_barang', $this->input->post('id'))->delete('barang');
        
        $callback = ['status'=>'success', 'message'=>"hapus berhasil!"];
        echo json_encode($callback);
    }
}
