<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HargaController extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_barang');
        $this->load->model('M_harga');
        $this->load->helper(array('form', 'url'));
    }

    public function is__login()
    {
        if ($this->session->userdata('auth_id') =='' and $this->session->userdata('auth_username') =='') {

            $this->session->set_flashdata('msg', 
                '<p class="text-danger" style="margin-top: 10px;">
                    <span><i class="fa fa-times"></i>Silahkan Login Terlebih Dahulu</span>
                </p');

            redirect(base_url('admin'));
        }
    }

    public function kembali()
    {
        return $this->input->server('HTTP_REFERER');
    }

    function logout(){
        $this->session->sess_destroy();
        redirect(base_url('admin'));
    }

    function index()
    {
        $this->is__login();
        
        $data['id'] = $this->uri->segment(2);
        $produk = $this->db->where('id_barang', $this->uri->segment(2))->get('barang')->row();
        $data['title1'] = 'Setting Harga Produk - '.$produk->nama_barang;
        $data['title2'] = 'harga';

        $this->load->view('harga/main', $data);
    }

    public function data()
    {
        header('Content-Type: application/json');
        echo json_encode($this->M_harga->getJson($this->input->post()));
    }

    public function form()
    {	
        $data['id_barang'] = $this->uri->segment(2);
        $data['harga'] = (!empty($this->input->post('id'))) ? $this->db->where('id_harga', $this->input->post('id'))->get('master_harga')->row() : "";
        $content = $this->load->view('harga/formAdd', $data, TRUE);

        $callback = ['status'=>'success', 'content'=>$content];
        echo json_encode($callback);
    }


    public function save()
    {   
        $id = (!empty($this->input->post('id'))) ? $this->input->post('id') : ""; 

        $data = array(
            'barang_id'   => $this->input->post('id_barang'),
            'ukuran'     => $this->input->post('ukuran'),
            'harga'     => $this->input->post('harga'),
            'harga_member'     => $this->input->post('harga_member'),
        );      

        if ($id == '') {
            $this->db->insert('master_harga', $data);   
        }else{
            $this->db->where('id_harga', $id);
            $this->db->update('master_harga',$data);
        }

        $this->session->set_flashdata('msg', 
                '<div class="alert alert-success" role="alert" style="margin-top: 10px;">
                    <span><i class="fa fa-check"></i> Berhasil menyimpan data!</span>
                </div>');

        redirect(base_url('setting-harga/'.$this->input->post('id_barang')));
    }

    public function hapus()
    {
        $this->db->where('id_harga', $this->input->post('id'))->delete('master_harga');
        
        $callback = ['status'=>'success', 'message'=>"hapus berhasil!"];
        echo json_encode($callback);
    }
}
