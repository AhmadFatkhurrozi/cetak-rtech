<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NeracaController extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_pemesanan');
        $this->load->helper(array('form', 'url'));
    }

    public function is__login()
    {
        if ($this->session->userdata('auth_id') =='' and $this->session->userdata('auth_barangname') =='') {

            $this->session->set_flashdata('msg', 
                '<p class="text-danger" style="margin-top: 10px;">
                    <span><i class="fa fa-times"></i>Silahkan Login Terlebih Dahulu</span>
                </p');

            redirect(base_url('admin'));
        }
    }

    public function kembali()
    {
        return $this->input->server('HTTP_REFERER');
    }

    function logout(){
        $this->session->sess_destroy();
        redirect(base_url('admin'));
    }

    function index()
    {
        $this->is__login();
        
        $data['title1'] = 'Rekap Pesanan';
        $data['title2'] = 'Rekap Pesanan';

        $this->load->view('neraca/main', $data);
    }


    public function data()
    {
        header('Content-Type: application/json');
        echo json_encode($this->M_pemesanan->neraca($this->input->post()));
    }

    function rekap()
    {
        $this->is__login();
        
        $data['title1'] = 'Rekap Pesanan';
        $data['title2'] = 'Rekap Pesanan';

        $this->load->view('neraca/rekap', $data);
    }

    public function cetak()
    {
    	if($this->input->GET('bulan') != ""){
            $bulanFull = explode("-", $this->input->GET('bulan'));
        	$data['bulan'] =$bulanFull[1];
        	$data['tahun'] = $bulanFull[0];
    
        	$data['judul']   = "<br> Bulan ".$data['bulan']."-".$data['tahun'];
        }else{
            $data['bulan'] = "0";
        	$data['tahun'] = "0";
        }
        
        $data['data']    = $this->M_pemesanan->cetak()->result();
                                
        $this->load->view('neraca/cetak', $data, FALSE);
        
        $html = $this->output->get_output();
        
        $this->load->library('dompdf_gen');
        
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        ob_end_clean();
        $this->dompdf->stream("rekap-pesanan-".$data['bulan']."-".$data['tahun'].".pdf", array('Attachment' => 0));
    }

    public function total()
    {	
        $data = $this->M_pemesanan->total();

    	$callback = ['status'=>'success', 'data'=>$data];
        echo json_encode($callback);
    }
}
