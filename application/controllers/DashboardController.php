<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardController extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

 		$this->load->library('session');
		$this->load->helper('me');
		$this->load->model('M_user');
		$this->load->helper(array('form', 'url'));
	}

	public function is__login()
	{
		if ($this->session->userdata('auth_id') =='' and $this->session->userdata('auth_username') =='') {

			$this->session->set_flashdata('msg', 
				'<p class="text-danger" style="margin-top: 10px;">
					<span><i class="fa fa-times"></i> Silahkan Login Terlebih Dahulu</span>
				</p');

			redirect(base_url('admin'));
		}
	}

	public function kembali()
	{
		return $this->input->server('HTTP_REFERER');
	}

	function index()
	{
		$this->is__login();
		
		$data['title2']	= 'Dashboard';
		$data['title3']	= '';
		$data['jumlah_pengguna'] = $this->db->count_all('users');
		$data['jumlah_barang'] = $this->db->count_all('barang');
		$data['total_pelanggan'] = $this->db->count_all('pelanggan');
		$data['total_pesanan'] = $this->db->count_all('pemesanan');

		$isiProses = array('Selesai', 'dibuat');

        if($this->session->userdata('auth_level_user') == 2) { 
			$data['sudah'] = $this->db->where('YEAR(tanggal)', date('Y'))
									->where('karyawan_id', $this->session->userdata('auth_id'))
									->where('status', 'Selesai')->get('pemesanan')->num_rows();
        }else{
        	$data['sudah'] = $this->db->where('YEAR(tanggal)', date('Y'))
									->where('status', 'Selesai')->get('pemesanan')->num_rows();
        }

        if($this->session->userdata('auth_level_user') == 2) { 
			$data['proses'] = $this->db->where('YEAR(tanggal)', date('Y'))
								->where('karyawan_id', $this->session->userdata('auth_id'))
								->where_not_in('status', $isiProses)->get('pemesanan')->num_rows();
        }else{
        	$data['proses'] = $this->db->where('YEAR(tanggal)', date('Y'))
								->where_not_in('status', $isiProses)->get('pemesanan')->num_rows();
        }

        // if($this->session->userdata('auth_level_user') == 2) { 
			// $data['belum'] = $this->db->where('YEAR(tanggal)', date('Y'))
			// 					->where('karyawan_id', $this->session->userdata('auth_id'))
			// 					->where('status', 'dibuat')->get('pemesanan')->num_rows();
        // }else{
        	$data['belum'] = $this->db->where('YEAR(tanggal)', date('Y'))
								->where('status', 'dibuat')->get('pemesanan')->num_rows();
        // }

		$this->load->view('admin/dashboard', $data);
	}

	public function chartBulanan()
	{
		$tahun = date("Y");
      	$title = 'Grafik Pesanan';
      	$subTitle = 'Tahun : '.$tahun;
      	$dtGrafik = [];

    


        $arrSelesai = [];

        for ($i=1; $i <= 12 ; $i++) {

          $selesai = $this->db->where('YEAR(tanggal)', $tahun)
                              		->where('MONTH(tanggal)', $i)
									->where('status', 'Selesai')->get('pemesanan')->num_rows();

          array_push($arrSelesai, (int)$selesai);

        }

        $dtGrafik[] = [
          'no' => 1,
          'nama' => "Selesai",
          'jumlah' => $arrSelesai,
        ];


      $callback =  ['dtGrafik'=>$dtGrafik, 'title'=>$title, 'subTitle'=>$subTitle];
      
      echo json_encode($callback);
	}
}
