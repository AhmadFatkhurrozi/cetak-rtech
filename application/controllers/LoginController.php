<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

 		$this->load->library('session');
		$this->load->helper('me');
		$this->load->model('M_user');
		$this->load->helper(array('form', 'url'));
	}

	public function kembali()
	{
		return $this->input->server('HTTP_REFERER');
	}

	public function index()
	{
		if (!empty($this->session->userdata('auth_id')) and !empty($this->session->userdata('auth_username'))) {

			redirect('dashboard');
		}else{
			$this->load->view('admin/login');
		}
	}

	function doLogin()
	{
		$username 	= $this->input->post('username');
		$pass		= $this->input->post('password');

		$cek = $this->M_user->auth($username, $pass);

		if( $cek->num_rows() != 0 ){

			$id = $cek->row()->id;
			
			$query = $this->db->where('id', $id)->get('users');

			if( $query->num_rows() != 0 )
			{
				$array = array(
					'auth_id' 		 => $id,
					'auth_username'	 => $query->row()->username,
					'auth_nama' 	 => $query->row()->nama,
					'auth_email'	 => $query->row()->email,
					'auth_alamat'	 => $query->row()->alamat,
					'auth_no_telp'	 => $query->row()->no_telp,
					'auth_foto'		 => $query->row()->foto,
					'auth_level_user'=> $query->row()->level_user ,
				);
				
				$this->session->set_userdata( $array );

				if ($query->row()->level_user == '1' || $query->row()->level_user == '2') {
					redirect('dashboard','refresh');
				}else{
					$this->session->set_flashdata('msg', 
						'<p class="text-danger" style="margin-top: 10px;">
							<span><i class="fa fa-times"></i> Anda Tidak Memiliki Akses</span>
						</p');

					redirect('admin');
				}

			}
		}
		else
		{
			$this->session->set_flashdata('msg', 
				'<p class="text-danger" style="margin-top: 10px;">
					<span><i class="fa fa-times"></i>Upps! username atau password Salah.</span>
				</p');	
			
			redirect($this->kembali());
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect('admin');
	}
}