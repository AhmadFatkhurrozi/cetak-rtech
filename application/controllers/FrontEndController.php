<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FrontEndController extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_user');
        $this->load->model('M_pelanggan');
        $this->load->helper(array('form', 'url'));
    }

    public function kembali()
    {
        return $this->input->server('HTTP_REFERER');
    }

    function index()
    {   
        $data['title'] = 'Dashboard';
        $data['produk'] = $this->db->join('kategori', 'kategori.id_kategori=barang.kategori_id')->get('barang', 4, 0);


        $this->load->view('front/main', $data);
    }

    function login()
    {   
        if (!empty($this->session->userdata('user_id')) and !empty($this->session->userdata('user_username')) && $this->session->userdata('user_level_user') == 3  ) {

			redirect(base_url());
		}else{
			$this->load->view('front/login');
		}
    }

    function register()
    {   
        if (!empty($this->session->userdata('user_id')) and !empty($this->session->userdata('user_username')) && $this->session->userdata('user_level_user') == 3  ) {

			redirect(base_url());
		}else{
			$this->load->view('front/register');
		}
    }

    public function simpanPelanggan()
    {
	    
        $config['allowed_types']    = 'jpg|png|pdf';
        $config['max_size']         = '4056';
        $config['overwrite']        = TRUE;

        $data = array(
            'username'  => $this->input->post('username'),
            'password'  => md5($this->input->post('password')),
            'nama_lengkap'=> $this->input->post('nama'),
            'email'     => $this->input->post('email'),
            'nomor_hp'  => $this->input->post('no_hp'),
            'alamat'    => $this->input->post('alamat'),
        );

        if($_FILES["foto_ktp"]["name"]){
    		$config['upload_path']          = './upload/ktp/';
	        $config["file_name"] = "ktp_".md5(date('YmdHis'));
	        $this->load->library('upload', $config);
	        $foto_ktp = $this->upload->do_upload('foto_ktp');
	        if (!$foto_ktp){
	            $error = array('error' => $this->upload->display_errors());
	            $this->session->set_flashdata("error", ".");
	        }else{
	            $foto_ktp = $this->upload->data("file_name");
	            $data['foto_ktp'] = $foto_ktp;
	            $this->session->set_flashdata("success", ".");
	        }
	    }

	    if($_FILES["swafoto"]["name"]){
    		$config['upload_path']          = './upload/swafoto/';
	        $config["file_name"] = "swafoto_".md5(date('YmdHis'));;
	        if($_FILES["swafoto"]["name"]){
	            $this->upload->initialize($config);
	        }else{
	            $this->loadl->library('upload', $config);
	        }
	        $swafoto = $this->upload->do_upload('swafoto');
	        if (!$swafoto){
	            $error = array('error' => $this->upload->display_errors());
	            $this->session->set_flashdata("error", "" );
	        }else{
	            $swafoto = $this->upload->data("file_name");
	            $data['swafoto'] = $swafoto;
	            $this->session->set_flashdata("success", ".");
	        }
	    }
        
        $this->db->insert('pelanggan', $data);   

        $this->session->set_flashdata('msg', 
                '<div class="alert alert-success" role="alert" style="margin-top: 10px;">
                    <span><i class="fa fa-check"></i> Berhasil menyimpan data!</span>
                </div>');

        redirect(base_url('login'));
    }

    public function cekEmail()
    {
		$data = $this->db->where('email', $this->input->post('email'))->get('pelanggan');

		if ($data->num_rows() != 0) {
			$callback = ['status'=>'success', 'row'=>$data];
		}else{
			$callback = ['status'=>'error', 'row'=>""];
		}
        
        echo json_encode($callback);
    }

    public function cekUsername()
    {
    	$data = $this->db->where('username', $this->input->post('username'))->get('pelanggan');

		if ($data->num_rows() != 0) {
			$callback = ['status'=>'success', 'row'=>$data];
		}else{
			$callback = ['status'=>'error', 'row'=>""];
		}
        
        echo json_encode($callback);
    }

    function doLogin()
	{
		$username 	= $this->input->post('username');
		$pass		= $this->input->post('password');

		$cek = $this->M_pelanggan->auth($username, $pass);

		if( $cek->num_rows() != 0 ){

			$id = $cek->row()->id_pelanggan;
			
			$query = $this->db->where('id_pelanggan', $id)->get('pelanggan');

			if( $query->num_rows() != 0 )
			{
				$array = array(
					'pelanggan_id' 		 => $id,
					'pelanggan_username' => $query->row()->username,
					'pelanggan_nama' 	 => $query->row()->nama_lengkap,
					'pelanggan_alamat'	 => $query->row()->alamat,
					'pelanggan_email'	 => $query->row()->email,
					'pelanggan_nohp'	 => $query->row()->nomor_hp,
					'pelanggan_tipe'	 => $query->row()->tipe_pelanggan,
				);
				
				$this->session->set_userdata( $array );

				redirect(base_url(),'refresh');
			}
		}
		else
		{
			$this->session->set_flashdata('msg', 
				'<p class="text-danger" style="margin-top: 10px;">
					<span><i class="fa fa-times"></i>Upps! username atau password Salah.</span>
				</p');	
			
			redirect($this->kembali());
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}
}
