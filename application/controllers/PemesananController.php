<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PemesananController extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_pemesanan');
        $this->load->helper(array('form', 'url'));
    }

    public function is__login()
    {
        if ($this->session->userdata('auth_id') =='' and $this->session->userdata('auth_barangname') =='') {

            $this->session->set_flashdata('msg', 
                '<p class="text-danger" style="margin-top: 10px;">
                    <span><i class="fa fa-times"></i>Silahkan Login Terlebih Dahulu</span>
                </p');

            redirect(base_url('admin'));
        }
    }

    public function kembali()
    {
        return $this->input->server('HTTP_REFERER');
    }

    function logout(){
        $this->session->sess_destroy();
        redirect(base_url('admin'));
    }

    function index()
    {
        $this->is__login();
        
        $data['title1'] = 'Data Pemesanan';
        $data['title2'] = 'Pemesanan';
        if (!empty($this->input->get('idKaryawan'))) {
        	$data['karyawan_id'] = $this->input->get('idKaryawan');
        }else{
        	$data['karyawan_id'] = 0;
        }
        $this->load->view('pemesanan/main', $data);
    }

    function pekerjaanSaya()
    {
        $this->is__login();
        
        $data['title1'] = 'Pekerjaan Saya';
        $data['title2'] = 'Pekerjaan Saya';

        $this->load->view('pemesanan/pekerjaanSaya/main', $data);
    }

    function detailCapaian()
    {
        $this->is__login();
     	
     	if (!empty($this->input->get('idKaryawan'))) {
        	$data['karyawan_id'] = $this->input->get('idKaryawan');
        	$nama = $this->db->where('id', $data['karyawan_id'])->get('users')->row()->nama;
        }else{
        	$data['karyawan_id'] = 0;
        	$nama = "-";
        }

        $data['title1'] = 'Detail Capaian Karyawan : '.$nama;
        $data['title2'] = 'Detail Capaian Karyawan : '.$nama;

        $this->load->view('pemesanan/pekerjaanSaya/capaianKaryawan', $data);
    }

    function listproduk()
    {
        $this->is__login();
        
        $data['title1'] = 'Tambah Pemesanan';
        $data['title2'] = 'Pemesanan';
        $data['title'] = 'Produk Kami';
        $data['produk'] = $this->db->get('barang');

        $this->load->view('pemesanan/list_produk', $data);
    }

    public function checkout()
    {
        $this->is__login();

    	$data['title1'] = 'Checkout';
        $data['title2'] = 'Pemesanan';
    	$data['title'] = 'Checkout';
        $data['harga'] = $this->db->where('barang_id', $this->uri->segment(2))->get('master_harga');
        $data['pemesanan'] = $this->db->where('id_barang', $this->uri->segment(2))->get('barang')->row();

        $this->load->view('pemesanan/checkout', $data);
    }

    public function data()
    {
        header('Content-Type: application/json');
        echo json_encode($this->M_pemesanan->getJson($this->input->post()));
    }

    public function dataKu()
    {
    	header('Content-Type: application/json');
        echo json_encode($this->M_pemesanan->getMyWork($this->input->post()));
    }

    public function getDetailCapaian()
    {
    	header('Content-Type: application/json');
        echo json_encode($this->M_pemesanan->getDetailCapaian($this->input->post()));
    }

    // public function form()
    // {
    //     $data['barang'] = (!empty($this->input->post('id'))) ? $this->db->where('id_barang', $this->input->post('id'))->get('barang')->row() : "";
    //     $content = $this->load->view('barang/formAdd', $data, TRUE);

    //     $callback = ['status'=>'success', 'content'=>$content];
    //     echo json_encode($callback);
    // }

    // public function detail()
    // {
    //     $data['title']= 'Detail';
    //     $data['barang'] = $this->db->where('id_barang', $this->input->post('id'))->get('barang')->row();

    //     $content = $this->load->view('barang/modal',$data, TRUE);
        
    //     $callback = ['status'=>'success', 'content'=>$content];
    //     echo json_encode($callback);
    // }

    // public function save()
    // {   

    //     $id = (!empty($this->input->post('id_barang'))) ? $this->input->post('id_barang') : ""; 

    //     $config['upload_path']      = './dist/img/barang/';
    //     $config['allowed_types']    = 'jpg|png';
    //     $config['file_name']        = md5(date('YmdHis'));
    //     $config['max_size']         = '4056';
    //     $config['overwrite']        = TRUE;
        
    //     $this->load->library('upload', $config);

    //     $data = array(
    //         'nama_barang'   => $this->input->post('nama_barang'),
    //         'deskripsi'     => $this->input->post('deskripsi'),
    //     );

    //     if ($this->upload->do_upload('photo')) {
    //         $this->upload->do_upload('photo');
    //         $data['gambar'] = $this->upload->data("file_name");
    //     }

    //     if ($id == '') {
    //         $this->db->insert('barang', $data);   
    //     }else{
    //         $this->db->where('id_barang', $id);
    //         $this->db->update('barang',$data);
    //     }

    //     $this->session->set_flashdata('msg', 
    //             '<div class="alert alert-success" role="alert" style="margin-top: 10px;">
    //                 <span><i class="fa fa-check"></i> Berhasil menyimpan data!</span>
    //             </div>');

    //     redirect(base_url('barang'));
    // }

    // public function hapus()
    // {
    //     $this->db->where('id_barang', $this->input->post('id'))->delete('barang');
        
    //     $callback = ['status'=>'success', 'message'=>"hapus berhasil!"];
    //     echo json_encode($callback);
    // }

    public function ubahStatus()
    {
    	$cekPemesanan = $this->db->where('id_pemesanan', $this->input->post('id'))->get('pemesanan')->row();
    	if($cekPemesanan->status == 'Selesai' || $cekPemesanan->status == 'Di Tolak'){
	    	$callback = ['status'=>'error', 'code'=>'500', 'message'=>"pesanan selesai diproses"];
	    }else{
	    	$data['title']= 'Ubah Status';
	        $data['pemesanan'] = $this->db->join('pelanggan as p', 'p.id_pelanggan = pemesanan.pelanggan_id', 'LEFT')
	        						->join('barang as b', 'b.id_barang = pemesanan.barang_id')
	        						->where('id_pemesanan', $this->input->post('id'))->get('pemesanan')->row();
			$data['user'] = $this->db->where('status', 'dibuat')->where('pemesanan_id', $this->input->post('id'))->get('status_pemesanan')->row()->user;
			$data['karyawan'] = $this->db->where('level_user', 2)->get('users');

	        $content = $this->load->view('pemesanan/modal',$data, TRUE);
	        
	        $callback = ['status'=>'success', 'content'=>$content];
	    }
	    echo json_encode($callback);
    }

    public function uploadBukti()
    {
    	$data['title']= 'Upload Bukti';
        $data['pemesanan'] = $this->db->join('pelanggan as p', 'p.id_pelanggan = pemesanan.pelanggan_id', 'LEFT')
        						->join('barang as b', 'b.id_barang = pemesanan.barang_id')
        						->where('id_pemesanan', $this->input->post('id'))->get('pemesanan')->row();

        $content = $this->load->view('pemesanan/pekerjaanSaya/modal',$data, TRUE);
        
        $callback = ['status'=>'success', 'content'=>$content];

	    echo json_encode($callback);
    }

    public function doUploadBukti()
    {
    	$id = $this->input->post('id_pemesanan');
    	$config['upload_path']      = './upload/bukti/';
        $config['allowed_types']    = 'jpg|png|jpeg';
        $config['file_name']        = md5(date('YmdHis'));
        $config['max_size']         = '4056';
        $config['overwrite']        = TRUE;
        
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('bukti')) {
            $this->upload->do_upload('bukti');
            $data['bukti'] = $this->upload->data("file_name");
        }

       	$this->db->where('id_pemesanan', $id);
        $this->db->update('pemesanan',$data);

        $this->session->set_flashdata('msg', 
                '<div class="alert alert-success" role="alert" style="margin-top: 10px;">
                    <span><i class="fa fa-check"></i> Berhasil menyimpan data!</span>
                </div>');

        redirect(base_url('pekerjaan-saya'));
    }

    public function show()
    {
    	$data['title']= 'Detail Pemesanan';
        $data['pemesanan'] = $this->db->join('pelanggan as p', 'p.id_pelanggan = pemesanan.pelanggan_id', 'LEFT')
        						->join('barang as b', 'b.id_barang = pemesanan.barang_id')
        						->where('id_pemesanan', $this->input->post('id'))->get('pemesanan')->row();
		$data['user'] = $this->db->where('status', 'dibuat')->where('pemesanan_id', $this->input->post('id'))->get('status_pemesanan')->row()->user;

        $content = $this->load->view('pemesanan/show',$data, TRUE);
        
        $callback = ['status'=>'success', 'content'=>$content];
        echo json_encode($callback);
    }

    public function valStatus()
    {
    	// $cekPemesanan = $this->db->where('id_pemesanan', $this->input->post('id'))->get('pemesanan')->row();
    	// if ($cekPemesanan->karyawan_id == 0) {

    		if (!empty($this->input->post('karyawan_id'))) {
		    	$data = array(
		            'status'   		=> $this->input->post('status'),
		            'karyawan_id'   => $this->input->post('karyawan_id'),
		        );

		        if (!empty($this->input->post('biaya_tambahan'))) {
		        	$data['biaya_tambahan'] = $this->input->post('biaya_tambahan');
		        }

		        if (!empty($this->input->post('sudah_bayar'))) {
		        	$data['sudah_bayar'] = $this->input->post('sudah_bayar');
		        }

		        if (!empty($this->input->post('total_harga'))) {
		        	$data['total_harga'] = $this->input->post('total_harga');
		        }
    		}else{
    			$data = array(
		            'status'   => $this->input->post('status'),
		        );
    		}

			$this->db->where('id_pemesanan', $this->input->post('id'))->update('pemesanan', $data);   
			
			$data2 = array(
	            'status'  		=> $this->input->post('status'),
	            'pemesanan_id'	=> $this->input->post('id'),
	            'user'     		=> $this->session->userdata('auth_nama'),
	            'tanggal'       => date('Y-m-d H:i:s'),
	            'catatan'   	=> $this->input->post('catatan'),
	        );

	        $this->db->insert('status_pemesanan', $data2);   
	        
	        $callback = ['status'=>'success', 'code'=>'200', 'message'=>"ubah status berhasil!"];
	    // }else{
	    // 	$nama = $this->db->where('id', $cekPemesanan->karyawan_id)->get('users')->row()->nama;
	    // 	$callback = ['status'=>'error', 'code'=>'500', 'message'=>"pesanan sudah diproses oleh ".$nama];
	    // }
		
		echo json_encode($callback);
    }

    public function simpanpesanan()
    {
    	$data = array(
            'barang_id'  	=> $this->input->post('id_barang'),
            'ukuran'		=> $this->input->post('ukuran'),
            'jumlah'     	=> $this->input->post('jumlah'),
            'no_wa'  		=> $this->input->post('no_wa'),
            'pelanggan_id'  => 0,
            'kode'    		=> "P".date('dmYHis'),
            'total_harga'   => preg_replace("/[^0-9]/", "", $this->input->post('total')),
            'status'    	=> "dibuat",
            'tanggal'    	=> date('Y-m-d'),
        );

		$this->db->insert('pemesanan', $data);   
		$id_pemesanan = $this->db->insert_id();
		$pesanan = $this->db->get_where('pemesanan', array('id_pemesanan' => $id_pemesanan));

		$data2 = array(
            'status'  		=> "dibuat",
            'pemesanan_id'	=> $pesanan->row()->id_pemesanan,
            'user'     		=> $this->input->post('nama'),
            'tanggal'       => date('Y-m-d H:i:s'),
        );

        $this->db->insert('status_pemesanan', $data2);   
   		
   		$callback = ['status'=>'success', 'message'=>"pemesanan berhasil!", 'id'=>""];

   		echo json_encode($callback);
    }
}
