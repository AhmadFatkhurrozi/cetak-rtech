<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProdukController extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_user');
        $this->load->helper(array('form', 'url'));
    }

    public function kembali()
    {
        return $this->input->server('HTTP_REFERER');
    }

    function login()
    {   
       	if (empty($this->session->userdata('pelanggan_id'))) {

			$this->load->view('front/login');
		}else{
			$this->kembali();
		}
    }

    function index()
    {   
    	// $this->login();	
        $data['title'] = 'Produk Kami';
        $data['produk'] = $this->db->join('kategori', 'kategori.id_kategori=barang.kategori_id')->get('barang');

        $this->load->view('front/produk/main', $data);
    }

    public function kategori()
    {
    	$data['title'] = $this->db->where('id_kategori', $this->uri->segment(2))->get('kategori')->row()->nama_kategori;
        $data['produk'] = $this->db->join('kategori', 'kategori.id_kategori=barang.kategori_id')->where('kategori_id', $this->uri->segment(2))->get('barang');

        $this->load->view('front/produk/perkategori', $data);
    }

    public function detail()
    {
    	// $this->login();
    	$data['title'] = 'Produk Kami';
        $data['produk'] = $this->db->where('id_barang', $this->uri->segment(2))->get('barang')->row();

        $this->load->view('front/produk/detail', $data);
    }

}
