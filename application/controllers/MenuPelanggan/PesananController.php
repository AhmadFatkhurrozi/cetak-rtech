<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PesananController extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_user');
        $this->load->helper(array('form', 'url'));
    }

    public function kembali()
    {
        return $this->input->server('HTTP_REFERER');
    }

    function login()
    {   
        if (empty($this->session->userdata('pelanggan_id'))) {

			$this->load->view('front/login');
		}else{
			$this->kembali();
		}
    }

    function index()
    {   
    	$this->login();
        $data['title'] = 'Pesanan Saya';
        $data['pemesanan'] = $this->db->join('barang', 'barang.id_barang = pemesanan.barang_id')
        							->where('pelanggan_id', $this->session->userdata('pelanggan_id'))->get('pemesanan');

        $this->load->view('front/pemesanan/main', $data);
    }

    public function checkout()
    {
    	// $this->login();
        $data['title'] = 'Checkout';
        $data['harga'] = $this->db->where('barang_id', $this->uri->segment(2))->get('master_harga');
        $data['pemesanan'] = $this->db->where('id_barang', $this->uri->segment(2))->get('barang')->row();

        $this->load->view('front/pemesanan/checkout', $data);
    }

    public function simpan()
    {
    	if(!empty($this->input->post('id_pelanggan')) || $this->input->post('id_pelanggan') != ''){
			$config['upload_path']      = './upload/desain/';
			$config['allowed_types']    = '*';
			$config['file_name']        = md5(date('YmdHis'));
			$config['max_size']         = '4056';
			$config['overwrite']        = TRUE;

			$data = array(
	            'barang_id'  	=> $this->input->post('id_barang'),
	            'ukuran'		=> $this->input->post('ukuran'),
	            'jumlah'     	=> $this->input->post('jumlah'),
	            'no_wa'  		=> $this->input->post('no_wa'),
	            'pelanggan_id'  => $this->input->post('id_pelanggan'),
	            'kode'    		=> "P".date('dmYHis'),
	            'total_harga'   => preg_replace("/[^0-9]/", "", $this->input->post('total')),
	            'status'    	=> "dibuat",
            	'tanggal'    	=> date('Y-m-d'),
	            'catatan'  		=> $this->input->post('catatan'),
			);
        
			$this->load->library('upload', $config);

			if ($this->upload->do_upload('desain')) {
				$this->upload->do_upload('desain');
				$data['desain'] = $this->upload->data("file_name");
			}

			$this->db->insert('pemesanan', $data);   
			$id_pemesanan = $this->db->insert_id();
			$pesanan = $this->db->get_where('pemesanan', array('id_pemesanan' => $id_pemesanan));

			$data2 = array(
	            'status'  		=> "dibuat",
	            'pemesanan_id'	=> $pesanan->row()->id_pemesanan,
	            'user'     		=> $this->session->userdata('pelanggan_nama'),
	            'tanggal'       => date('Y-m-d H:i:s'),
	        );

	        $this->db->insert('status_pemesanan', $data2);   
       		
       		$callback = ['status'=>'success', 'message'=>"pemesanan berhasil!", 'id'=>$pesanan->row()->id_pemesanan];
	  	}else{
       		$callback = ['status'=>'error', 'message'=>"Silahkan Login Terlebih dahulu!", 'id'=>''];
	  	}
        echo json_encode($callback);
    }

    public function cekStatus()
    {
    	$this->login();
    	$data['title'] = 'Status Pemesanan';
        $data['status'] = $this->db->where('pemesanan_id', $this->uri->segment(2))->get('status_pemesanan');
        $data['pemesanan'] = $this->db->join('pemesanan as p', 'p.id_pemesanan = status_pemesanan.pemesanan_id')
        						->join('barang as b', 'b.id_barang = p.barang_id')
        						// ->where('pelanggan_id', $this->session->userdata('pelanggan_id'))
        						->where('pemesanan_id', $this->uri->segment(2))->get('status_pemesanan')->row();

        $this->load->view('front/pemesanan/status', $data);
    }

    public function cekPesanan()
    {
    	$this->login();
    	$id = $this->db
    	// ->where('pelanggan_id', $this->session->userdata('pelanggan_id'))
    	->where('kode', $this->input->post('kode'))->get('pemesanan')->row();

    	if (!empty($id)) {
    		redirect(base_url('status/'.$id->id_pemesanan),'refresh');
    	}else{
    		$this->session->set_flashdata('msg', 
			'<p class="text-danger" style="margin-top: 10px;">
				<span><i class="fa fa-times"></i> data tidak ditemukan</span>
			</p');

			redirect(base_url(),'refresh');
    	}

    }

    public function getHarga()
    {
		$data = $this->db->where('barang_id', $this->input->post('id_barang'))->where('ukuran', $this->input->post('ukuran'))->get('master_harga')->row();

        if (!empty($data)) {
        	$callback = ['status'=>'success', 'tipe_pelanggan' => $this->session->userdata('pelanggan_tipe'), 'data'=>$data];
        }else{
        	$callback = ['status'=>'error', 'tipe_pelanggan' => '', 'data'=>""];
        }
        
        echo json_encode($callback);
    }
}
