<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KategoriController extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_kategori');
        $this->load->helper(array('form', 'url'));
    }

    public function is__login()
    {
        if ($this->session->userdata('auth_id') =='' and $this->session->userdata('auth_username') =='') {

            $this->session->set_flashdata('msg', 
                '<p class="text-danger" style="margin-top: 10px;">
                    <span><i class="fa fa-times"></i>Silahkan Login Terlebih Dahulu</span>
                </p');

            redirect(base_url('admin'));
        }
    }

    public function kembali()
    {
        return $this->input->server('HTTP_REFERER');
    }

    function logout(){
        $this->session->sess_destroy();
        redirect(base_url('admin'));
    }

    function index()
    {
        $this->is__login();
        
        $data['title1'] = 'Data Kategori';
        $data['title2'] = 'Kategori';

        $this->load->view('kategori/main', $data);
    }

    public function data()
    {
        header('Content-Type: application/json');
        echo json_encode($this->M_kategori->getJson($this->input->post()));
    }

    public function form()
    {
        $data['kategori'] = (!empty($this->input->post('id'))) ? $this->db->where('id_kategori', $this->input->post('id'))->get('kategori')->row() : "";
        $content = $this->load->view('kategori/formAdd', $data, TRUE);

        $callback = ['status'=>'success', 'content'=>$content];
        echo json_encode($callback);
    }

    public function detail()
    {
        $data['title']= 'Detail';
        $data['kategori'] = $this->db->where('id_kategori', $this->input->post('id'))->get('kategori')->row();

        $content = $this->load->view('kategori/modal',$data, TRUE);
        
        $callback = ['status'=>'success', 'content'=>$content];
        echo json_encode($callback);
    }

    public function save()
    {   

        $id = (!empty($this->input->post('id_kategori'))) ? $this->input->post('id_kategori') : ""; 

        $data = array(
            'nama_kategori'   => $this->input->post('nama_kategori'),
        );

        if ($id == '') {
            $this->db->insert('kategori', $data);   
        }else{
            $this->db->where('id_kategori', $id);
            $this->db->update('kategori',$data);
        }

        $this->session->set_flashdata('msg', 
                '<div class="alert alert-success" role="alert" style="margin-top: 10px;">
                    <span><i class="fa fa-check"></i> Berhasil menyimpan data!</span>
                </div>');

        redirect(base_url('kategori'));
    }

    public function hapus()
    {
        $this->db->where('id_kategori', $this->input->post('id'))->delete('kategori');
        
        $callback = ['status'=>'success', 'message'=>"hapus berhasil!"];
        echo json_encode($callback);
    }
}
