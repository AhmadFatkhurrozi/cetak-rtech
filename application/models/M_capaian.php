<?php

class M_capaian extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $this->load->library('datagrid');
    }

    function tampil_data($table){
        return $this->db->get($table);
    }

    public function getJson($input)
    {
        $table  = 'users as u';
        $select = 'u.id, u.nama, count(p.id_pemesanan) as jumlah';
        
        $replace_field  = [
            // ['old_name' => 'images', 'new_name' => 'a.gambar'],
        ];

        $param = [
            'input'         => $input,
            'select'        => $select,
            'table'         => $table,
            'replace_field' => $replace_field
        ];

        $data = $this->datagrid->query($param, function($data) use ($input) {
        	$data->join('pemesanan as p', 'p.karyawan_id=u.id', 'left');
            
	    	if (!empty($input['periode'])) {
	            $data->like('tanggal', $input['periode']);
	        }

            return $data->where('u.level_user', 2)
                        ->group_by('u.id');
        });

        return $data;
    }

    public function cetak()
    {	
    	// $awal = $this->input->GET('awal');
    	// $akhir = $this->input->GET('akhir');

    	// $this->db->select('u.id, u.nama, count(p.id_pemesanan) as jumlah');
    	// $this->db->join('pemesanan as p', 'p.karyawan_id=u.id', 'left');
     //    $this->db->where('p.tanggal >=', $awal);
     //    $this->db->where('p.tanggal <=', $akhir);
        $this->db->where('u.level_user', 2);
        // $this->db->group_by('u.id');

        $query = $this->db->get('users as u');
        return $query;
    }

}