<?php

class M_user extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $this->load->library('datagrid');
    }

    function auth($username, $pass)
    {
        $this->db->where('username', $username);
        $this->db->where('password',md5($pass));
        $query = $this->db->get('users');
        return $query;
    }
    
    function tampil_data($table){
        return $this->db->get($table);
    }

    public function getJson($input)
    {
        $table  = 'users as a';
        $select = 'a.*';
        
        $replace_field  = [
            ['old_name' => 'level', 'new_name' => 'a.level_user'],
        ];

        $param = [
            'input'         => $input,
            'select'        => $select,
            'table'         => $table,
            'replace_field' => $replace_field
        ];

        $data = $this->datagrid->query($param, function($data) use ($input) {
            return $data;
        });

        return $data;
    }

}