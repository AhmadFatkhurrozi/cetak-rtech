<?php

class M_barang extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $this->load->library('datagrid');
    }

    function tampil_data($table){
        return $this->db->get($table);
    }

    public function getJson($input)
    {
        $table  = 'barang as a';
        $select = 'a.*, kategori.*';
        
        $replace_field  = [
            ['old_name' => 'images', 'new_name' => 'a.gambar'],
        ];

        $param = [
            'input'         => $input,
            'select'        => $select,
            'table'         => $table,
            'replace_field' => $replace_field
        ];

        $data = $this->datagrid->query($param, function($data) use ($input) {
            return $data->join('kategori', 'kategori.id_kategori=a.kategori_id', 'left');
        });

        return $data;
    }

}