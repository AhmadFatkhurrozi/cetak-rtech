<?php

class M_pemesanan extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $this->load->library('datagrid');
    }

    function tampil_data($table){
        return $this->db->get($table);
    }

    public function getJson($input)
    {
        $table  = 'pemesanan as a';
        $select = 'a.*, b.*, u.nama';
        
        $replace_field  = [
            ['old_name' => 'images', 'new_name' => 'b.gambar'],
            ['old_name' => 'harga', 'new_name' => 'a.total_harga'],
            ['old_name' => 'tambahan', 'new_name' => 'a.biaya_tambahan'],
            ['old_name' => 'tgl', 'new_name' => 'a.tanggal'],
        ];

        $param = [
            'input'         => $input,
            'select'        => $select,
            'table'         => $table,
            'replace_field' => $replace_field
        ];

        $data = $this->datagrid->query($param, function($data) use ($input) {

        	if (!empty($this->input->get('idKaryawan')) || $this->input->get('idKaryawan') != 0) {
	            $data->where('karyawan_id', $this->input->get('idKaryawan'));
	        }

        	if ($this->session->userdata('auth_level_user') == '1' || $this->session->userdata('auth_level_user') == '3') {
            	return $data->join('barang as b', 'b.id_barang = a.barang_id')
            			->join('users as u', 'u.id = a.karyawan_id', 'left');
        	}else{
        		 return $data->join('barang as b', 'b.id_barang = a.barang_id')
            			->join('users as u', 'u.id = a.karyawan_id', 'left')
            			->where('karyawan_id', $this->session->userdata('auth_id'));
        	}
        });

        return $data;
    }

    public function getDetailCapaian($input)
    {
    	$table  = 'pemesanan as a';
        $select = 'a.*, b.*, u.nama';
        
        $replace_field  = [
            ['old_name' => 'images', 'new_name' => 'b.gambar'],
           ['old_name' => 'harga', 'new_name' => 'a.total_harga'],
            ['old_name' => 'tambahan', 'new_name' => 'a.biaya_tambahan'],
            ['old_name' => 'tgl', 'new_name' => 'a.tanggal'],
            ['old_name' => 'buktinya', 'new_name' => 'a.bukti'],
        ];

        $param = [
            'input'         => $input,
            'select'        => $select,
            'table'         => $table,
            'replace_field' => $replace_field
        ];

        $data = $this->datagrid->query($param, function($data) use ($input) {
        
        if (!empty($this->input->get('idKaryawan')) || $this->input->get('idKaryawan') != 0) {
            $data->where('karyawan_id', $this->input->get('idKaryawan'));
        }

            return $data->join('barang as b', 'b.id_barang = a.barang_id')
            			->join('users as u', 'u.id = a.karyawan_id', 'left');
        });

        return $data;
    }

    public function getMyWork($input)
    {
    	$table  = 'pemesanan as a';
        $select = 'a.*, b.*, u.nama';
        
        $replace_field  = [
            ['old_name' => 'images', 'new_name' => 'b.gambar'],
           ['old_name' => 'harga', 'new_name' => 'a.total_harga'],
            ['old_name' => 'tambahan', 'new_name' => 'a.biaya_tambahan'],
            ['old_name' => 'tgl', 'new_name' => 'a.tanggal'],
            ['old_name' => 'buktinya', 'new_name' => 'a.bukti'],
        ];

        $param = [
            'input'         => $input,
            'select'        => $select,
            'table'         => $table,
            'replace_field' => $replace_field
        ];

        $data = $this->datagrid->query($param, function($data) use ($input) {
            return $data->join('barang as b', 'b.id_barang = a.barang_id')
            			->join('users as u', 'u.id = a.karyawan_id', 'left')
            			->where('karyawan_id', $this->session->userdata('auth_id'));
        });

        return $data;
    }

    public function neraca($input)
    {
    	$table  = 'pemesanan as a';
        $select = 'a.*, b.*, u.nama';
        
        $replace_field  = [
            ['old_name' => 'images', 'new_name' => 'b.gambar'],
             ['old_name' => 'harga', 'new_name' => 'a.total_harga'],
            ['old_name' => 'tambahan', 'new_name' => 'a.biaya_tambahan'],
            ['old_name' => 'tgl', 'new_name' => 'a.tanggal'],
            ['old_name' => 'buktinya', 'new_name' => 'a.bukti'],
        ];

        $param = [
            'input'         => $input,
            'select'        => $select,
            'table'         => $table,
            'replace_field' => $replace_field
        ];

        $data = $this->datagrid->query($param, function($data) use ($input) {
	    	
	    	$data->join('barang as b', 'b.id_barang = a.barang_id')
            			->join('users as u', 'u.id = a.karyawan_id', 'left');

	    	if (!empty($input['periode'])) {
	            $data->like('tanggal', $input['periode']);
	        }

            return $data->where('status', 'Selesai');
        });

        return $data;
    }

    public function cetak()
    {
    	$this->db->select('a.*, b.*, u.nama')
    			->join('barang as b', 'b.id_barang = a.barang_id')
    			->join('users as u', 'u.id = a.karyawan_id', 'left')
    			->where('status', 'Selesai');

    	if (!empty($this->input->GET('bulan'))) {
            $this->db->like('tanggal', $this->input->GET('bulan'));
        }

        return $this->db->get('pemesanan as a');
    }

    public function total()
    {
    	$this->db->select('SUM(a.total_harga + a.biaya_tambahan) as total')
    			->join('barang as b', 'b.id_barang = a.barang_id')
    			->join('users as u', 'u.id = a.karyawan_id', 'left')
    			->where('status', 'Selesai');

    	if ($this->input->post('bulan') != '') {
            $this->db->like('tanggal', $this->input->post('bulan'));
        }

        return $this->db->get('pemesanan as a')->row();
    }

}