<?php

class M_kategori extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $this->load->library('datagrid');
    }

    function tampil_data($table){
        return $this->db->get($table);
    }

    public function getJson($input)
    {
        $table  = 'kategori as a';
        $select = 'a.*';
        
        $replace_field  = [
        ];

        $param = [
            'input'         => $input,
            'select'        => $select,
            'table'         => $table,
            'replace_field' => $replace_field
        ];

        $data = $this->datagrid->query($param, function($data) use ($input) {
            return $data;
        });

        return $data;
    }

}