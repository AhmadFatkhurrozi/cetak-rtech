<div class="box box-primary b-t-non" id='panel-add'>
    
    <!-- <hr class="m-t-0"> -->
    <form class="form-save card" action="<?=base_url('save-barang');?>" method="POST" enctype="multipart/form-data">
    	<div class="card-header">
    		<h4 class="labelBlue">
		        <i class="fa fa-plus-square iconLabel m-r-15"></i> 
		        <?php if(!empty($barang)){ echo "Ubah Data"; }else{ echo "Tambah Data"; } ?>
		    </h4>
		</div>

        <div class="card-body">
            <div class="row">
            	<div class="col-md-6">
            		<input type="hidden" name="id_barang" <?php if(!empty($barang)){ ?> value="<?=$barang->id_barang;?>" <?php } ?>>
            		<fieldset class="form-group">
						<label>Nama Barang</label>
						<input type="text" name="nama_barang" <?php if(!empty($barang)){ ?> value="<?=$barang->nama_barang;?>" <?php } ?> class="form-control" required="">
					</fieldset>
					
					<fieldset class="form-group">
						<label>Deskripsi</label>
						<textarea name="deskripsi" class="form-control" required=""><?php if(!empty($barang)){ echo $barang->deskripsi; } ?></textarea>
					</fieldset>

					<fieldset class="form-group">
						<label>Kategori</label>
						<select name="kategori" class="form-control">
							<?php if(empty($barang)){ ?>
							<option selected="" value="" disabled="">.:: pilih kategori ::.</option>
							<?php } ?>
							<?php foreach ($kategori as $k): ?>
							<option value="<?=$k->id_kategori;?>" <?php if (!empty($barang) && $barang->kategori_id == $k->id_kategori) { echo "selected"; }?>><?=$k->nama_kategori;?></option>
							<?php endforeach ?>
						</select>
					</fieldset>
            	</div>
            	<div class="col-md-6">
            		<fieldset class="form-group">
	                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 text-center">
	                        <div class="crop-edit">
	                            <center>
	                                <?php if(empty($barang->gambar)){ ?>
			                          <img id="preview-photo" src="<?=base_url('dist/img/default.jpg')?>" style="object-fit: cover;" class="img-polaroid m-r-4" width="90%">
	                                <?php }else{ ?>
			                          <img id="preview-photo" src="<?=base_url('dist/img/barang/'.$barang->gambar);?>" class="img-polaroid m-r-4" style="object-fit: cover;" height="150px" width="150px">
	                                <?php }?>
	                            </center>
	                        </div>
	                    </div>
	                    <div class='clearfix p-b-5'></div>

	                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'></label>
	                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
	                        <input type="file" class="upload" onchange="loadFilePhoto(event)" name="photo" accept="image/*" class="form-control customInput input-sm col-md-7 col-xs-12">
	                    </div>
	                </fieldset>
            	</div>
            </div>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-primary btn-submit btn-sm pull-right ml-2">Simpan <span class="fa fa-save"></span></button>
            <button type="button" class="btn btn-warning btn-cancel btn-sm pull-right"><span class="fa fa-chevron-left"></span> Kembali</button>
        </div>
    </form>
</div>

<script type="text/javascript">

    $('.btn-cancel').click(function(e){
        e.preventDefault();
        $('.other-page').fadeOut(function(){
            $('.other-page').empty();
            $('.main-layer').fadeIn();
        });
    });

    function loadFilePhoto(event) {
        var image = URL.createObjectURL(event.target.files[0]);
        $('#preview-photo').fadeOut(function(){
            $(this).attr('src', image).fadeIn().css({
                '-webkit-animation' : 'showSlowlyElement 700ms',
                'animation'         : 'showSlowlyElement 700ms'
            });
        });
    };

</script>
