<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Login Admin</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<?php 
	echo __css('fontawesome');
	echo __css('bootstrap');
	echo __css('fontastic');
	echo __css('default');
	echo __css('custom');
	?>

	<style type="text/css">
		.text-login{
			font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
    		font-weight: 400;
    		font-size: 14px;
    		color: #666666;
		}
		body {
		  background-image: url('./dist/img/bg.png');
		  background-repeat: no-repeat;
		  background-attachment: fixed;
		  background-size: cover;
		}
	</style>
</head>
<body>
	<div class="page login-page">
		<div class="container d-flex align-items-center">
			<div class="form-holder">
				<div class="row">

					<div class="col-lg-4 offset-md-4">

						<div class="login-logo text-center m-3">
							<img src="<?=base_url('dist/img/logo-color.png');?>" style="height: 100%; width: 120px;">
						</div>

						<div class="bg-white pt-3" style="border-radius: 5px;">
							<h2 class="text-center m-4">Login use your Account</h2>

							<div class="content pr-4 pl-4 pb-3">
								<form action="<?=base_url('LoginController/doLogin');?>" method="post" class="form-validate">

									<div class="form-group">
										<input id="login-username" type="text" name="username" required data-msg="Please enter your username" class="input-material" placeholder="username">
									</div>

									<div class="form-group">
										<input id="login-password" type="password" name="password" required data-msg="Please enter your password" class="input-material" placeholder="password">
									</div>

									<button type="submit" class="btn btn-primary btn-block"><i class="fa fa-sign-in"></i> Login</button> 
								</form>

								<p id="notifications"><?php echo $this->session->flashdata('msg'); ?></p>

								<hr style="border-color:#ababab;margin: 5px 0;">
						        <div class="social-auth-links text-center pb-2 pt-2 text-login">
						          ©2020 This Apps <br> Dikembangkan oleh <a href="#">Titis Oktalia Reptanti</a><br/>
						        </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

</body>
</html>