<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view("component/head.php") ?>
  	<style type="text/css">
  		.highcharts-credits{
  			display: none;
  		}
 
  	</style>
  </head>
  <body>
  	<div class="wrapper">
    	<?php $this->load->view("component/sidebar.php") ?>

    	<div id="content">
    		<?php $this->load->view("component/navbar.php") ?>
            <h2>Dashboard</h2>
       			
            <div class="row my-4">
              	<p class="text-muted"><?php echo $this->session->flashdata('msg'); ?></p>
            	<div class="col-md-12"></div>
            	
        			<?php if($this->session->userdata('auth_level_user') == 2) { ?>
        			 <div class="col-xl-4 col-md-4 mb-4">
	              <div class="card border-left-info shadow h-100 py-2">
	                <div class="card-body">
	                  <div class="row no-gutters align-items-center">
	                    <div class="col mr-2">
	                      <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Pesanan Baru</div>
	                      <div class="row no-gutters align-items-center">
	                        <div class="col-auto">
	                          <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
	                          	<span style="font-size: 32px;"><?=$belum;?></span>
	                          </div>
	                        </div>
	                      </div>
	                    </div>
	                    <div class="col-auto">
	                      <i class="fa fa-download fa-2x text-gray-300"></i>
	                    </div>
	                  </div>
	                </div>
	              </div>
	            </div>

        			 
	            <div class="col-xl-4 col-md-4 mb-4">
	              <div class="card border-left-info shadow h-100 py-2">
	                <div class="card-body">
	                  <div class="row no-gutters align-items-center">
	                    <div class="col mr-2">
	                      <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Pesanan Di Proses</div>
	                      <div class="row no-gutters align-items-center">
	                        <div class="col-auto">
	                          <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
	                          	<span style="font-size: 32px;"><?=$proses;?></span>
	                          </div>
	                        </div>
	                      </div>
	                    </div>
	                    <div class="col-auto">
	                      <i class="fa fa-spinner fa-spin fa-2x text-gray-300"></i>
	                    </div>
	                  </div>
	                </div>
	              </div>
	            </div>

	            <div class="col-xl-4 col-md-4 mb-4">
	              <div class="card border-left-info shadow h-100 py-2">
	                <div class="card-body">
	                  <div class="row no-gutters align-items-center">
	                    <div class="col mr-2">
	                      <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Pesanan Selesai</div>
	                      <div class="row no-gutters align-items-center">
	                        <div class="col-auto">
	                          <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
	                          	<span style="font-size: 32px;"><?=$sudah;?></span>
	                          </div>
	                        </div>
	                      </div>
	                    </div>
	                    <div class="col-auto">
	                      <i class="fa fa-check fa-2x text-gray-300"></i>
	                    </div>
	                  </div>
	                </div>
	              </div>
	            </div>
	            
        			<?php }else{ ?>
	            <!-- <div class="col-xl-3 col-md-6 mb-4">
	              <div class="card border-left-info shadow h-100 py-2">
	                <div class="card-body">
	                  <div class="row no-gutters align-items-center">
	                    <div class="col mr-2">
			            			<a href="<?=base_url('user')?>">
		                      <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Pengguna</div>
		                      <div class="row no-gutters align-items-center">
		                        <div class="col-auto">
		                          <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
		                          	<span style="font-size: 32px;"><?=$jumlah_pengguna;?></span>
		                          </div>
		                        </div>
		                      </div>
					        			</a>
	                    </div>
	                    <div class="col-auto">
	                      <i class="fa fa-users fa-2x text-gray-300"></i>
	                    </div>
	                  </div>
	                </div>
	              </div>
	            </div>
	            
	            <div class="col-xl-3 col-md-6 mb-4">
	              <div class="card border-left-info shadow h-100 py-2">
	                <div class="card-body">
	                  <div class="row no-gutters align-items-center">
	                    <div class="col mr-2">
	                    <a href="<?=base_url('barang')?>">
	                      <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Produk</div>
	                      <div class="row no-gutters align-items-center">
	                        <div class="col-auto">
	                          <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
	                          	<span style="font-size: 32px;"><?=$jumlah_barang;?></span>
	                          </div>
	                        </div>
	                      </div>
	                     </a>
	                    </div>
	                    <div class="col-auto">
	                      <i class="fa fa-list fa-2x text-gray-300"></i>
	                    </div>
	                  </div>
	                </div>
	              </div>
	            </div>

	            <div class="col-xl-3 col-md-6 mb-4">
	              <div class="card border-left-info shadow h-100 py-2">
	                <div class="card-body">
	                  <div class="row no-gutters align-items-center">
	                    <div class="col mr-2">
	                      <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Pemesanan</div>
	                      <div class="row no-gutters align-items-center">
	                        <div class="col-auto">
	                          <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
	                          	<span style="font-size: 32px;"><?=$total_pesanan;?></span>
	                          </div>
	                        </div>
	                      </div>
	                    </div>
	                    <div class="col-auto">
	                      <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
	                    </div>
	                  </div>
	                </div>
	              </div>
	            </div>

	            <div class="col-xl-3 col-md-6 mb-4">
	              <div class="card border-left-warning shadow h-100 py-2">
	                <div class="card-body">
	                  <div class="row no-gutters align-items-center">
	                    <div class="col mr-2">
	                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Pelanggan</div>
	                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$total_pelanggan;?></div>
	                    </div>
	                    <div class="col-auto">
	                      <i class="fa fa-users fa-2x text-gray-300"></i>
	                    </div>
	                  </div>
	                </div>
	              </div>
	            </div> -->
        			<?php } ?>


	            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
	            	<div class="card border-left-warning shadow h-100">
	                <div class="card-header bg-secondary text-light">
	                	Grafik Pesanan Tahun <?=date('Y')?>
	                </div>
	                <div class="card-body">
	                	<div class="row">
	                		<div class="col-xl-8 col-md-8 col-sm-12 col-12 px-3">
	                			<div id="resultChart"></div>
	                		</div>
	                		<div class="col-xl-4 col-md-4 col-sm-12 col-12 px-3">
	                			<div id="grafik"></div>
	                		</div>
	                	</div>
	                </div>
	              </div>
	            </div>

          	</div>

      </div>

   		<!-- <?php $this->load->view("component/footer.php") ?> -->
  	</div>

  <script src="<?=base_url('dist/highcharts.js')?>"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			$('#sidebarCollapse').on('click', function () {
			  $('#sidebar').toggleClass('active');
			});
		});

	  Highcharts.chart('grafik', {
		    chart: {
		        plotBackgroundColor: null,
		        plotBorderWidth: 0,
		        plotShadow: false
		    },
		    title: {
		        // text: 'Grafik Pesanan <br> Tahun '+<?=date("Y");?>,
		        text: '',
		        align: 'center',
		        verticalAlign: 'middle',
		        y: 150
		    },
		    tooltip: {
		        pointFormat: '{series.name}: <b>{point.y:.0f} Pesanan</b>'
		    },
		    accessibility: {
		        point: {
		            valueSuffix: ''
		        }
		    },
		    plotOptions: {
		        pie: {
		            dataLabels: {
		                enabled: true,
		                distance: -50,
		                style: {
		                    fontWeight: 'bold',
		                    color: 'white'
		                }
		            },
		            startAngle: -90,
		            endAngle: 90,
		            center: ['50%', '75%'],
		            size: '110%'
		        }
		    },
		    yAxis: {
		        allowDecimals: false,
		        title: {
		            text: 'Flats'
		        }
		    },
		    series: [{
		        type: 'pie',
		        name: 'Pesanan',
		        innerSize: '50%',
		        data: [
		            ['Sudah', <?php echo (int)$sudah ?>],
        				
		            ['Belum', <?php echo (int)$belum ?>],

		            ['Proses', <?php echo (int)$proses ?>],
		        ]
		    }]
		});

	  resultChartSelesai();

	  function resultChartSelesai() {
      $.ajax({
      		type: 'POST',
      		url: '<?php echo site_url('chartBulanan')?>',
      		async : true,
          dataType : 'json',
      		data: {},
      		success: function(vals) { 

	        var resurtGrafik = [];
	        for (var i = 0; i < vals.dtGrafik.length; i++) {
	          resurtGrafik[i] = { name: vals.dtGrafik[i].nama, data: vals.dtGrafik[i].jumlah };
	        }
	        var judul = vals.title;
	        var subJudul = vals.subTitle;
	        Highcharts.chart('resultChart', {
	          chart: {
	              type: 'column'
	          },
	          title: { text: judul },
	          subtitle: { text: subJudul },
	          xAxis: {
	              categories: [
	                  'Januari',
	                  'Februari',
	                  'Maret',
	                  'April',
	                  'Mei',
	                  'Juni',
	                  'Juli',
	                  'Agustus',
	                  'September',
	                  'Oktober',
	                  'November',
	                  'Desember'
	              ],
	              crosshair: true
	          },
	          yAxis: {
	              min: 0,
	              title: {
	                  text: 'Jumlah Pesanan'
	              }
	          },
	          tooltip: {
	              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	              pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                  '<td style="padding:0"><b>{point.y:.0f} Pesanan</b></td></tr>',
	              footerFormat: '</table>',
	              shared: true,
	              useHTML: true
	          },
	          plotOptions: {
	              column: {
	                  pointPadding: 0.2,
	                  borderWidth: 0
	              }
	          },
	          series: resurtGrafik
	        });
      	}
      });
    }
	</script>
  </body>
</html>