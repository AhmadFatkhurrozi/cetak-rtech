<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view("component/head.php") ?>
	  <link rel="stylesheet" type="text/css" href="<?=base_url('dist/css/pagination.css');?>">
	  <link rel="stylesheet" type="text/css" href="<?=base_url('dist/css/animate.min.css');?>">

  </head>
  <body>
    <div class="wrapper">
      <?php $this->load->view("component/sidebar.php") ?>

      <div id="content">
        <?php $this->load->view("component/navbar.php") ?>

      

        <section class="content-header">
        	<h1><?=$title1?></h1>
			    <ol class="breadcrumb" style="margin-bottom: 5px;">
						<li class="breadcrumb-item pl-3"><a href="<?=base_url('/dashboard')?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><?=$title1;?></li>
					</ol>
				</section>

        <section class="content">
          <div class="row">
            <div class="col-md-12">

              <div class="box main-layer">
              	<div class="box-header with-border my-1">
              		<div class="row">
              			<div class="col-md-6">
              				<?php if($this->session->userdata('auth_level_user') != 2) { ?>
              				<a href="javascript:void(0)" class="btn btn-primary btn-add"><i class="fa fa-plus"></i> Tambah</a>
              				<?php } ?>
              			</div>
              			<dic class="col-md-6">
              				<div class="form-inline float-right">
              					<div class="form-group">
              						<select class="form-control search-option"></select>
              					</div>
              					<div class="form-group">
              						<input type="text" class="form-control search-input" placeholder="Search...">
              					</div>
              				</div>
              			</dic>
              		</div>
              	</div>

              	<p class="text-muted"><?php echo $this->session->flashdata('msg'); ?></p>

                <div class="box-body table-responsive">
                  <table class="table table-striped table-light text-dark" id="datagrid"></table>
                </div>

                <div class="box-footer clearfix">
                  <div class="row">
                    <div class="col-md-4 form-inline">
                      <div class="form-group">
                        <select class="form-control option"></select>
                      </div>
                    </div>
                    <div class="col-md-4 text-center info"></div>
                    <div class="col-md-4">
                      <ul class="pagination pagination-sm no-margin pull-right pagination-main"></ul>
                    </div>
                  </div>
                </div>
              </div>

              <div class="other-page"></div>
            	<div class="modal-dialog"></div>

            </div>
          </div>
        </section>
            
      </div>

        <!-- <?php $this->load->view("component/footer.php") ?> -->
    </div>

   <script type="text/javascript">
      $(document).ready(function () {
          $('#sidebarCollapse').on('click', function () {
              $('#sidebar').toggleClass('active');
          });
      });

      var datagrid;
      $(document).ready(function() {
        datagrid = $("#datagrid").datagrid({
          url           			: '<?php echo base_url("get-kategori"); ?>', 
          primaryField      	: 'id_kategori', 
          rowNumber       		: true,
          searchInputElement  : '.search-input',
          searchFieldElement  : '.search-option',
          pagingElement       : '.pagination-main',
          optionPagingElement : '.option',
          pageInfoElement     : '.info',
          columns         		: [
              {field: 'nama_kategori', title: 'Nama Kategori', editable: true, sortable: true, width: 100, align: 'left', search: true},
              {field: 'menu', title: 'Menu', sortable: false, width: 200, align: 'center', search: false, 
                  rowStyler: function(rowData, rowIndex) {
                    return menu(rowData, rowIndex);
                  }
              }
            ]
          });
        datagrid.run();
      });

      $('.btn-add').click(function(){
      	$('.main-layer').hide();
      	$.ajax({
      		type: 'POST',
      		url: '<?php echo site_url('form-add-kategori')?>',
      		async : true,
          dataType : 'json',
      		data: {},
      		success: function(response) { 
      			if(response.status == 'success'){
		          $('.loading').hide();
		          $('.other-page').html(response.content).fadeIn();
		        } else {
		          $('.main-layer').show();
		        }
      		}
      	});
      });


      function menu(rowData, rowIndex) {
        var menu;

        menu = "";

        <?php if($this->session->userdata('auth_level_user') != 2) { ?>
	        menu += '<div class="btn-group"><a onclick="edit('+rowData.id_kategori+')" href="javascript:void(0);" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Edit</a>';
	        menu += '<a onclick="hapus('+rowData.id_kategori+')" href="javascript:void(0);" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Hapus</a>'+
	        '</div>';
	      <?php } ?>

        return menu;
      }

	    function edit(id) {
	    	$('.main-layer').hide();
      	$.ajax({
      		type: 'POST',
      		url: '<?php echo site_url('form-add-kategori')?>',
      		async : true,
          dataType : 'json',
      		data: {id:id},
      		success: function(response) { 
      			if(response.status == 'success'){
		          $('.loading').hide();
		          $('.other-page').html(response.content).fadeIn();
		        } else {
		          $('.main-layer').show();
		        }
      		}
      	});
	    }

	    function hapus(id) {
      	$.ajax({
      		type: 'POST',
      		url: '<?php echo site_url('hapus-kategori')?>',
      		async : true,
          dataType : 'json',
      		data: {id:id},
      		success: function(response) { 
      			if(response.status == 'success'){
		          location.reload();
		        }
      		}
      	});
	    }
    </script>
  </body>
</html>