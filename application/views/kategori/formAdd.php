<div class="box box-primary b-t-non" id='panel-add'>
    
    <!-- <hr class="m-t-0"> -->
    <form class="form-save card" action="<?=base_url('save-kategori');?>" method="POST" enctype="multipart/form-data">
    	<div class="card-header">
    		<h4 class="labelBlue">
		        <i class="fa fa-plus-square iconLabel m-r-15"></i> 
		        <?php if(!empty($kategori)){ echo "Ubah Data"; }else{ echo "Tambah Data"; } ?>
		    </h4>
		</div>

        <div class="card-body">
            <div class="row">
            	<div class="col-md-12">
            		<input type="hidden" name="id_kategori" <?php if(!empty($kategori)){ ?> value="<?=$kategori->id_kategori;?>" <?php } ?>>
            		<fieldset class="form-group">
						<label>Nama kategori</label>
						<input type="text" name="nama_kategori" <?php if(!empty($kategori)){ ?> value="<?=$kategori->nama_kategori;?>" <?php } ?> class="form-control" required="">
					</fieldset>
            	</div>
            </div>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-primary btn-submit btn-sm pull-right ml-2">Simpan <span class="fa fa-save"></span></button>
            <button type="button" class="btn btn-warning btn-cancel btn-sm pull-right"><span class="fa fa-chevron-left"></span> Kembali</button>
        </div>
    </form>
</div>

<script type="text/javascript">

    $('.btn-cancel').click(function(e){
        e.preventDefault();
        $('.other-page').fadeOut(function(){
            $('.other-page').empty();
            $('.main-layer').fadeIn();
        });
    });

</script>
