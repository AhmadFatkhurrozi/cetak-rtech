<div class="modal fade" id="detail-dialog" tabindex="-1" role="dialog" aria-labelledby="product-detail-dialog">
  <div class="modal-dialog modal-lg" >
    <div class="modal-content">
      <div class="modal-header" style="width: 100%">
          <?=$title;?>
        <span style="float:right"><a data-dismiss="modal">Close</a></span>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="form-group row m-b-0">
              <label class="label-text text-md-right col-form-label col-md-6 col-sm-6 col-xs-6 m-b-0">Nama Barang</label>
              <label class="col-form-label label-text m-t-0 m-b-0">: <?=$barang->nama_barang;?></label>
            </div>

            <div class="form-group row m-b-0">
              <label class="label-text text-md-right col-form-label col-md-6 col-sm-6 col-xs-6 m-b-0">Deskripsi</label>
              <label class="col-form-label label-text m-t-0 m-b-0">: <?=$barang->deskripsi;?></label>
            </div>
          </div>

          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
	          <div class="form-group row m-b-0">
              <?php if($barang->gambar == null){ ?>
              	<img width="250px" src="<?=base_url('dist/img/default.jpg')?>">
              <?php }else{ ?>
              	<img width="250px" src="<?=base_url('dist/img/barang/'.$barang->gambar)?>">
              <?php } ?>
            </div>
          </div>
      </div>

      </div>
      <div class="clearfix" style='padding-bottom:20px'></div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('#detail-dialog').find('.modal-dialog').css({
      'width'     : '80%'
    });
    $('#detail-dialog').modal('show');
  })();

  $('#detail-dialog').on('hidden.bs.modal', function () {
    $('.modal-dialog').html('');
  });

</script>
