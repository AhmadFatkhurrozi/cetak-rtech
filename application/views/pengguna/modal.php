<div class="modal fade" id="detail-dialog" tabindex="-1" role="dialog" aria-labelledby="product-detail-dialog">
  <div class="modal-dialog modal-lg" style="font-size: 12px;">
    <div class="modal-content">
      <div class="modal-header" style="width: 100%">
          <?=$title;?>
        <span style="float:right"><a data-dismiss="modal">Close</a></span>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="form-group row m-b-0">
              <label class="label-text text-md-right col-form-label col-md-4 col-sm-4 col-xs-4 m-b-0">Username</label>
              <label class="col-form-label label-text m-t-0 m-b-0 col-md-8 col-sm-8 col-xs-8">: <?=$user->username;?></label>
            </div>

            <div class="form-group row m-b-0">
              <label class="label-text text-md-right col-form-label col-md-4 col-sm-4 col-xs-4 m-b-0">Email</label>
              <label class="col-form-label label-text m-t-0 m-b-0 col-md-8 col-sm-8 col-xs-8">: <?=$user->email;?></label>
            </div>

            <div class="form-group row m-b-0">
              <label class="label-text text-md-right col-form-label col-md-4 col-sm-4 col-xs-4 m-b-0">Nama</label>
              <label class="col-form-label label-text m-t-0 m-b-0 col-md-8 col-sm-8 col-xs-8">: <?=$user->nama;?></label>
            </div>

            <div class="form-group row m-b-0">
              <label class="label-text text-md-right col-form-label col-md-4 col-sm-4 col-xs-4 m-b-0">Jenis Kelamin</label>
              <label class="col-form-label label-text m-t-0 m-b-0 col-md-8 col-sm-8 col-xs-8">: <?=$user->jenis_kelamin;?></label>
            </div>
          </div>

          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="form-group row m-b-0">
              <label class="label-text text-md-right col-form-label col-md-4 col-sm-4 col-xs-4 m-b-0">Alamat</label>
              <label class="col-form-label label-text m-t-0 m-b-0 col-md-8 col-sm-8 col-xs-8">: <?=$user->alamat;?></label>
            </div>

            <div class="form-group row m-b-0">
              <label class="label-text text-md-right col-form-label col-md-4 col-sm-4 col-xs-4 m-b-0">No Telp.</label>
              <label class="col-form-label label-text m-t-0 m-b-0 col-md-8 col-sm-8 col-xs-8">: <?=$user->no_telp;?></label>
            </div>

            <div class="form-group row m-b-0">
              <label class="label-text text-md-right col-form-label col-md-4 col-sm-4 col-xs-4 m-b-0">Level</label>
              <label class="col-form-label label-text m-t-0 m-b-0 col-md-8 col-sm-8 col-xs-8">: <?php if($user->level_user == 1){echo "Admin";}elseif($user->level_user == 2){echo "Karyawan";}elseif($user->level_user == 3){echo "Owner";}else{echo "CS";} ?></label>
            </div>
          </div>
      </div>

      </div>
      <div class="clearfix" style='padding-bottom:20px'></div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('#detail-dialog').find('.modal-dialog').css({
      'width'     : '95%'
    });
    $('#detail-dialog').modal('show');
  })();

  $('#detail-dialog').on('hidden.bs.modal', function () {
    $('.modal-dialog').html('');
  });

</script>
