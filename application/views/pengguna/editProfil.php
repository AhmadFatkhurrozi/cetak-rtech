<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view("component/head.php") ?>
      <link rel="stylesheet" type="text/css" href="./dist/css/pagination.css">
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
  </head>
  <body>
    <div class="wrapper">
      <?php $this->load->view("component/sidebar.php") ?>

      <div id="content">
        <?php $this->load->view("component/navbar.php") ?>

      

        <section class="content-header">
            <h1><?=$title1?></h1>
                <ol class="breadcrumb" style="margin-bottom: 5px;">
                        <li class="breadcrumb-item pl-3"><a href="<?=base_url('/dashboard')?>">Dashboard</a></li>
                        <li class="breadcrumb-item active"><?=$title2;?></li>
                    </ol>
                </section>

        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="other-page">
                <div class="box box-primary b-t-non" id='panel-add'>
    
                  <form class="form-save card" action="<?=base_url('save-profil');?>" method="POST" enctype="multipart/form-data">
                      <div class="card-body">
                          <div class="row">
                            <div class="col-md-6">
                              <input type="hidden" name="id" <?php if(!empty($user)){ ?> value="<?=$user->id;?>" <?php } ?>>
                              <fieldset class="form-group">
                                <label>Email</label>
                                <input type="email" name="email" <?php if(!empty($user)){ ?> value="<?=$user->email;?>" <?php } ?> class="form-control" required="">
                                <p class="text-muted"><?php echo $this->session->flashdata('msgEmail'); ?></p>
                              </fieldset>
                              <fieldset class="form-group">
                                <label>Username</label>
                                <input type="text" name="username" <?php if(!empty($user)){ ?> value="<?=$user->username;?>" <?php } ?>  class="form-control" readonly="" disabled></p>
                              </fieldset>
                              <fieldset class="form-group">
                                <label>Password</label>
                                <input type="password" name="password" class="form-control" placeholder="Kosongi Jika tidak ingin mengubah password"></p>
                              </fieldset>
                              <fieldset class="form-group">
                                <label>Nama</label>
                                <input type="nama" name="nama" <?php if(!empty($user)){ ?> value="<?=$user->nama;?>" <?php } ?> class="form-control" required="">
                              </fieldset>
                            </div>

                            <div class="col-md-6">
                              <fieldset class="form-group">
                                <label class="mr-2">Jenis Kelamin</label>
                                <label class="radio-inline">
                                  <input type="radio" name="jenis_kelamin" <?php if(!empty($user)){ echo ($user->jenis_kelamin == "Laki-Laki") ? "checked" : ""; } ?>  required="" value="Laki-Laki"> Laki-Laki
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="jenis_kelamin" <?php if(!empty($user)){ echo ($user->jenis_kelamin == "Perempuan") ? "checked" : "" ;} ?>  required="" value="Perempuan"> Perempuan
                                </label>
                              </fieldset>
                              <fieldset class="form-group">
                                <label>No Telepon</label>
                                <input type="no_telp" name="no_telp" <?php if(!empty($user)){ ?> value="<?=$user->no_telp;?>" <?php } ?> class="form-control" required="">
                              </fieldset>
                              <fieldset class="form-group">
                                <label>alamat</label>
                                <textarea name="alamat" class="form-control" required=""><?php if(!empty($user)){ echo $user->alamat; } ?></textarea>
                              </fieldset>
                              <fieldset class="form-group">
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 text-center">
                                    <div class="crop-edit">
                                        <center>
                                            <?php if(empty($user->foto)){ ?>
                                          <img id="preview-photo" src="<?=base_url('dist/img/foto.jpg')?>" style="object-fit: cover;" class="img-polaroid m-r-4 text-center" height="150px">
                                            <?php }else{ ?>
                                          <img id="preview-photo" src="<?=base_url('dist/upload/'.$user->foto);?>" class="img-polaroid m-r-4 text-center" style="object-fit: cover;" height="150px">
                                            <?php }?>
                                        </center>
                                    </div>
                                </div>
                                <div class='clearfix p-b-5'></div>

                                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'></label>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <input type="file" class="upload" onchange="loadFilePhoto(event)" name="photo" accept="image/*" class="form-control customInput input-sm col-md-7 col-xs-12">
                                </div>
                            </fieldset>
                            </div>
                          </div>
                      </div>

                      <div class="card-footer">
                          <button type="submit" class="btn btn-primary btn-submit btn-sm pull-right ml-2">Simpan <span class="fa fa-save"></span></button>
                          <button type="button" class="btn btn-warning btn-cancel btn-sm pull-right"><span class="fa fa-chevron-left"></span> Kembali</button>
                      </div>
                  </form>
              </div>
              </div>
            </div>
          </div>
        </section>
            
      </div>

    </div>

   <script type="text/javascript">
      $(document).ready(function () {
          $('#sidebarCollapse').on('click', function () {
              $('#sidebar').toggleClass('active');
          });
      });

      function loadFilePhoto(event) {
          var image = URL.createObjectURL(event.target.files[0]);
          $('#preview-photo').fadeOut(function(){
              $(this).attr('src', image).fadeIn().css({
                  '-webkit-animation' : 'showSlowlyElement 700ms',
                  'animation'         : 'showSlowlyElement 700ms'
              });
          });
      };

    </script>
  </body>
</html>