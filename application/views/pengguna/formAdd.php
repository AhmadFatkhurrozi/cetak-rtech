<div class="box box-primary b-t-non" id='panel-add'>
    
    <!-- <hr class="m-t-0"> -->
    <form class="form-save card" action="<?=base_url('save-user');?>" method="POST">
    	<div class="card-header">
    		<h4 class="labelBlue">
		        <i class="fa fa-plus-square iconLabel m-r-15"></i>
		        <?php if(!empty($user)){ echo "Ubah Data"; }else{ echo "Tambah Data"; } ?>
		    </h4>
		</div>

        <div class="card-body">
            <div class="row">
            	<div class="col-md-6">
            		<input type="hidden" name="id" <?php if(!empty($user)){ ?> value="<?=$user->id;?>" <?php } ?>>
            		<fieldset class="form-group">
						<label>Email</label>
						<input type="email" name="email" <?php if(!empty($user)){ ?> value="<?=$user->email;?>" <?php } ?> class="form-control" required="">
						<p class="text-muted"><?php echo $this->session->flashdata('msgEmail'); ?></p>
					</fieldset>
					<fieldset class="form-group">
						<label>Username</label>
						<input type="text" name="username" <?php if(!empty($user)){ ?> value="<?=$user->username;?>" <?php } ?>  class="form-control" required="">
						<p class="text-muted"><?php echo $this->session->flashdata('msgUsername'); ?></p>
					</fieldset>
					<fieldset class="form-group">
						<label>Nama</label>
						<input type="nama" name="nama" <?php if(!empty($user)){ ?> value="<?=$user->nama;?>" <?php } ?> class="form-control" required="">
					</fieldset>
					<fieldset class="form-group">
						<label class="mr-2">Jenis Kelamin</label>
						<label class="radio-inline">
							<input type="radio" name="jenis_kelamin" <?php if(!empty($user)){ echo ($user->jenis_kelamin == "Laki-Laki") ? "checked" : ""; } ?>  required="" value="Laki-Laki"> Laki-Laki
						</label>
						<label class="radio-inline">
							<input type="radio" name="jenis_kelamin" <?php if(!empty($user)){ echo ($user->jenis_kelamin == "Perempuan") ? "checked" : "" ;} ?>  required="" value="Perempuan"> Perempuan
						</label>
					</fieldset>
            	</div>
            	<div class="col-md-6">
            		<fieldset class="form-group">
						<label>No Telepon</label>
						<input type="no_telp" name="no_telp" <?php if(!empty($user)){ ?> value="<?=$user->no_telp;?>" <?php } ?> class="form-control" required="">
					</fieldset>
					<fieldset class="form-group">
						<label>alamat</label>
						<textarea name="alamat" class="form-control" required=""><?php if(!empty($user)){ echo $user->alamat; } ?></textarea>
					</fieldset>

      				<?php if($this->session->userdata('auth_level_user') == 3) { ?>
					<fieldset class="form-group">
						<label class="mr-2">Level</label>
						<label class="radio-inline">
							<input type="radio" name="level"  <?php if(!empty($user)){ echo ($user->level_user == 1) ? "checked" : ""; } ?> required="" value="1"> Admin
						</label>
						<label class="radio-inline">
							<input type="radio" name="level"  <?php if(!empty($user)){ echo ($user->level_user == 2) ? "checked" : ""; } ?> required="" value="2"> Karyawan
						</label>
						<label class="radio-inline">
							<input type="radio" name="level"  <?php if(!empty($user)){ echo ($user->level_user == 3) ? "checked" : ""; } ?> required="" value="3"> Owner
						</label>
					</fieldset>
					<?php }else{ ?>
						<?php if (!empty($user)): ?>
						<input type="hidden" name="level" value="<?=$user->level_user;?>">
						<?php endif ?>
					<?php } ?>
            	</div>
            </div>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-primary btn-submit btn-sm pull-right ml-2">Simpan <span class="fa fa-save"></span></button>
            <button type="button" class="btn btn-warning btn-cancel btn-sm pull-right"><span class="fa fa-chevron-left"></span> Kembali</button>
        </div>
    </form>
</div>

<script type="text/javascript">

    $('.btn-cancel').click(function(e){
        e.preventDefault();
        $('.other-page').fadeOut(function(){
            $('.other-page').empty();
            $('.main-layer').fadeIn();
        });
    });

  //   function loadFilePhoto(event) {
  //       var image = URL.createObjectURL(event.target.files[0]);
  //       $('#preview-photo').fadeOut(function(){
  //           $(this).attr('src', image).fadeIn().css({
  //               '-webkit-animation' : 'showSlowlyElement 700ms',
  //               'animation'         : 'showSlowlyElement 700ms'
  //           });
  //       });
  //   };

</script>
