<div class="box box-primary b-t-non" id='panel-add'>
    
    <!-- <hr class="m-t-0"> -->
    <form class="form-save card" action="<?=base_url('save-harga');?>" method="POST" enctype="multipart/form-data">
    	<div class="card-header">
    		<h4 class="labelBlue">
		        <i class="fa fa-plus-square iconLabel m-r-15"></i> 
		        <?php if(!empty($harga)){ echo "Ubah Data"; }else{ echo "Tambah Data"; } ?>
		    </h4>
		</div>

        <div class="card-body">
            <div class="row">
            	<div class="col-md-4">
            		<input type="hidden" name="id_barang" value="<?=$id_barang?>">
            		<input type="hidden" name="id" <?php if(!empty($harga)){ ?> value="<?=$harga->id_harga;?>" <?php } ?>>
            		<fieldset class="form-group">
						<label>Ukuran</label>
						<input type="text" name="ukuran" <?php if(!empty($harga)){ ?> value="<?=$harga->ukuran;?>" <?php } ?> class="form-control" required="">
					</fieldset>
            	</div>
            	<div class="col-md-4">
            		<fieldset class="form-group">
						<label>Harga Per Item</label>
						<input type="text" name="harga" <?php if(!empty($harga)){ ?> value="<?=$harga->harga;?>" <?php } ?> class="form-control" required="">
					</fieldset>
            	</div>
            	<div class="col-md-4">
            		<fieldset class="form-group">
						<label>Harga Member</label>
						<input type="text" name="harga_member" <?php if(!empty($harga)){ ?> value="<?=$harga->harga_member;?>" <?php } ?> class="form-control" required="">
					</fieldset>
            	</div>
            </div>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-primary btn-submit btn-sm pull-right ml-2">Simpan <span class="fa fa-save"></span></button>
            <button type="button" class="btn btn-warning btn-cancel btn-sm pull-right"><span class="fa fa-chevron-left"></span> Kembali</button>
        </div>
    </form>
</div>

<script type="text/javascript">

    $('.btn-cancel').click(function(e){
        e.preventDefault();
        $('.other-page').fadeOut(function(){
            $('.other-page').empty();
            $('.main-layer').fadeIn();
        });
    });

</script>
