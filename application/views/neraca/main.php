<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view("component/head.php") ?>
      <link rel="stylesheet" type="text/css" href="./dist/css/pagination.css">
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
      <script src="<?php echo base_url('assets/plugins/sweetalert/dist/sweetalert.min.js'); ?>"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/sweetalert/dist/sweetalert.css'); ?>">
    <link rel="stylesheet" type="text/css" href="https://app.cahayakhitan.com/css/bootstrap-datetimepicker.min.css">
  </head>
  <body>
    <div class="wrapper">
      <?php $this->load->view("component/sidebar.php") ?>

      <div id="content">
        <?php $this->load->view("component/navbar.php") ?>

      

        <section class="content-header">
        <h1><?=$title1?></h1>
            <ol class="breadcrumb" style="margin-bottom: 5px;">
                <li class="breadcrumb-item pl-3"><a href="<?=base_url('/dashboard')?>">Dashboard</a></li>
                <li class="breadcrumb-item active"><?=$title1;?></li>
            </ol>
        </section>

        <section class="content">
          <div class="row">
            <div class="col-md-12">

              <div class="box main-layer">
                <div class="box-header with-border my-1">
                    <div class="row">
                        <div class="col-md-6">
							           <!--  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-0">
							              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 input-group p-t-0 p-b-0">
							                <input type="text" name="month_search" value="12-2020" autocomplete="off" class="form-control input pull-right" id="date_search" data-date-format="mm-yyyy" placeholder="Bulan" required="required">
							              </div>
							            </div>
							            <div class="col-lg-5 col-md5 col-sm-5 col-xs-12 p-0" style="margin-left: 10px;">
							                <button type="button" class="btn btn-success doFilter" title="Filte"><i class="fa fa-search"></i>&nbsp;</button>
							                <button type="button" class="btn btn-danger doPrint" title="Cetak"><i class="fa fa-print"></i>&nbsp;</button>
							            </div> -->
							            <div class="row">
							            	<div class="col-md-6">
									            <div class="form-group">
				                          <input type="text" class="input-sm form-control bulan" data-date-format="yyyy-mm" placeholder="periode">
				                      </div>
							            	</div>
							            	<div class="col-md-6">
							            		<button type="button" class="btn btn-danger doPrint" title="Cetak"><i class="fa fa-print"></i>&nbsp; Cetak </button>
							            	</div>
							            </div>


                        </div>
                        <dic class="col-md-6">
                            <div class="form-inline float-right">
                               <!--  <div class="form-group">
                                    <select class="form-control search-option"></select>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control search-input" placeholder="Search...">
                                </div> -->
                            </div>
                        </dic>
                    </div>
                </div>

                <p class="text-muted"><?php echo $this->session->flashdata('msg'); ?></p>

                <div class="box-body table-responsive">
                  <table class="table table-striped table-light text-dark" id="datagrid"></table>
                </div>

                <div class="box-footer clearfix">
              	 	<div class="row">
			              <div class="col-lg-12">
			              	<div class="float-right mr-2">
				                <label>Total : </label>
			                  <span class="panelTotal font-weight-bold"></span>
			              	</div>
			              </div>
			            </div>
                  <div class="row">
                    <div class="col-md-4 form-inline">
                      <div class="form-group">
                        <select class="form-control option"></select>
                      </div>
                    </div>
                    <div class="col-md-4 text-center info"></div>
                    <div class="col-md-4">
                      <ul class="pagination pagination-sm no-margin pull-right pagination-main"></ul>
                    </div>
                  </div>
                </div>
              </div>

              <div class="other-page"></div>
                <div class="modal-dialog"></div>

            </div>
          </div>
        </section>
            
      </div>

        <!-- <?php $this->load->view("component/footer.php") ?> -->
    </div>

    <script type="text/javascript" src="https://app.cahayakhitan.com/js/bootstrap-datetimepicker.min.js"></script>
   <script type="text/javascript">
      $(document).ready(function () {
          $('#sidebarCollapse').on('click', function () {
              $('#sidebar').toggleClass('active');
          });
      });

      var datagrid;
      $(document).ready(function() {
        datagrid = $("#datagrid").datagrid({
          url                       : '<?php echo base_url("get-neraca"); ?>', 
          primaryField          : 'id_pemesanan', 
          rowNumber             : true,
          searchInputElement  : '.search-input',
          searchFieldElement  : '.search-option',
          pagingElement       : '.pagination-main',
          optionPagingElement : '.option',
          pageInfoElement     : '.info',
          queryParams             : {
		          'periode'    : $('.bulan').val(),
		      },
          columns               : [
          		{field: 'tgl', title: 'Tanggal', editable: false, sortable: true, width: 100, align: 'center', search: false,
              	rowStyler: function(rowData, rowIndex) {
                     return tgl(rowData, rowIndex);
                  }
            	},
              {field: 'nama_barang', title: 'Nama Barang', editable: true, sortable: true, width: 200, align: 'left', search: false},
              {field: 'pemroses', title: 'karyawan', editable: true, sortable: false, width: 80, align: 'center', search: false,
              rowStyler: function(rowData, rowIndex) {
                     return pemroses(rowData, rowIndex);
                  }
              },
              {field: 'harga', title: 'Biaya', editable: true, sortable: false, width: 180, align: 'right', search: false,
                rowStyler: function(rowData, rowIndex) {
                       return harga(rowData, rowIndex);
                    }
                },
              {field: 'tambahan', title: 'Biaya Tambahan', editable: true, sortable: false, width: 180, align: 'right', search: false,
                rowStyler: function(rowData, rowIndex) {
                       return tambahan(rowData, rowIndex);
                    }
                },
              {field: 'total', title: 'Total Biaya', sortable: false, width: 200, align: 'right', search: false, 
                  rowStyler: function(rowData, rowIndex) {
                    return total(rowData, rowIndex);
                  }
                }
            ]
          });
        datagrid.run();
      });

      $(".bulan").on("change", function(){
	        datagrid.queryParams({
	            'periode'    : $('.bulan').val()
	        });
	        datagrid.reload();
	        cekNilai();
	    });


      function images(rowData, rowIndex) {
        var gmbr = rowData.gambar;
        if (gmbr == null) {
            return '<span><img height="50px" src="<?=base_url('dist/img/default.jpg')?>"></span>';
        }else{
            return '<span><img height="50px" src="<?=base_url('dist/img/barang/')?>'+gmbr+'"></span>';
        }
      }

      function harga(rowData, rowIndex) {
        var hrg = rowData.total_harga;
        if (hrg == null) {
            return '<span>'+0+'</span>';
        }else{
            return '<span>'+hrg+'</span>';
        }
      }

      function tambahan(rowData, rowIndex) {
        var tmbh = rowData.biaya_tambahan;
        if (tmbh == null) {
            return '<span>'+0+'</span>';
        }else{
            return '<span>'+tmbh+'</span>';
        }
      }

      function total(rowData, rowIndex) {
      	if (rowData.biaya_tambahan == '' || rowData.biaya_tambahan == null) {
      		var tmhan = 0;
      	}else{
      		var tmhan = rowData.biaya_tambahan;
      	}
        var hrg = rowData.total_harga;
        var total = parseInt(hrg)+parseInt(tmhan);

      	return '<span>'+total+'</span>';
      }

       function pemroses(rowData, rowIndex) {
        var nm = rowData.nama;
        if (nm == null) {
            return '<span class="badge badge-success">belum ditentukan</span>';
        }else{
            return '<span>'+nm+'</span>';
        }
      }

      function tgl(rowData, rowIndex) {
      	var tgl = rowData.tanggal;
	      var pecahTgl = tgl.split(' ');
	      var pecahTgl2 = pecahTgl[0].split('-');
	      return pecahTgl2[2]+'/'+pecahTgl2[1]+'/'+pecahTgl2[0];
      }

      $('.bulan').datetimepicker({
        weekStart: 2,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 4,
        minView: 3,
        forceParse: 0,
      });

       $('.doPrint').click(function() {
        var bulan = $('.bulan').val();
        window.open("<?=base_url('cetak-pesanan')?>?bulan="+bulan, '_blank');
      });

    function cekNilai() {
        var bulan = $('.bulan').val();
        $.ajax({
      		type: 'POST',
      		url: '<?php echo site_url('getTotalNeraca')?>',
      		async : true,
          dataType : 'json',
      		data: {bulan:bulan},
      		success: function(response) { 
      			if(response.status == 'success'){
      				if (response.data.total != null) {
	      				var bilangan = response.data.total;
	              var reverse = bilangan.toString().split('').reverse().join(''),
	              ribuan     = reverse.match(/\d{1,3}/g);
	              ribuan = ribuan.join('.').split('').reverse().join('');
	              var total = 'Rp. '+ribuan;
      				}else{
	              var total = 'Rp. 0';
      				}

              $('.panelTotal').html(total);
		        } else {
		          return "";
		        }
      		}
      	});
    }



    $(document).ready(function() {
      cekNilai();
    });
    </script>
  </body>
</html>