<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>REKAP CAPAIAN KARYAWAN</title>
    <style media="screen">
      table {
          font-family: arial, sans-serif;
          border-collapse: collapse;
          width: 100%;
          padding: 40px; 
      } 
      .detail, .detail tbody td,.detail tbody tr, .detail thead tr,.detail thead th{
        /*border :0.5px solid black;*/
        border-spacing: 0.5px;
        /*text-align: center;*/
        padding-top: 5px;
        padding-bottom: 5px;
        font-size: 12px;
      }
      .detail thead tr {
        background-color: salmon;
        color: #fff;
      }
      .detail tbody td, .detail thead td, .detail tfoot td{
        padding : 5px;
        /*text-align: center;*/
      }
      .detail tbody tr:nth-child(odd){
        background: #ebebeb;
      }
      .detail tbody tr:nth-child(even){
        background: #d7d7d7;
      }
      @page { margin: 5px; }
      body{margin:5px;}
      .title, .dept {
        font-size: 18px;
        font-weight: bold;
        width: 100%;
      }
      .dept {
        font-size: 13px !important;
      }
    </style>
  </head>
  <body>
    <div style="margin: 30px; text-align: center;">
      <div class="title">REKAP PESANAN SELESAI</div>
      <?php if(!empty($this->input->GET('bulan'))){ ?>
      <div class="dept">Periode <?=$judul?></div>
    	<?php } ?>
    </div>
    <table class='detail' style="border-collapse: collapse;">
      <thead>
        <tr>
          <th><center>No</center></th>
          <th><center>Tanggal</center></th>
          <th><center>Nama Barang</center></th>
          <th><center>Nama Karyawan</center></th>
          <th><right>Harga</center></th>
          <th><right>Biaya Tambahan</center></th>
          <th><right>Total</center></th>
        </tr>
      </thead>
      <tbody>
        <?php $no = 1; $grand = 0;?>
        <?php if(count($data) > 0){ 
            foreach($data as $a) { ?>
              <tr>
                <td align='center'><?=$no++?></td>
                <td align='center'><?=date('d/m/Y', strtotime($a->tanggal));?></td>
                <td align='center'><?=$a->nama_barang;?></td>
                <td align='center'><?=$a->nama;?></td>
                <td align='right'><?=$a->total_harga;?></td>
                <td align='right'><?=$a->biaya_tambahan;?></td>
                <td align='right'><?=$a->total_harga+$a->biaya_tambahan;?></td>
              </tr>
      				<?php $total = $a->total_harga+$a->biaya_tambahan; $grand = $grand + $total; ?>
            <?php }}else{ ?>
          <tr>
            <td colspan="7" align="center"><i>- Data Tidak Ada -</i></td>
          </tr>
        <?php } ?>
      </tbody>
      <tfoot style="background-color: #FFBDBD;">
	      <tr>
	        <td colspan="6" align="right" style="padding: 10px 0px; font-weight: bold;">Total :</td>
	        <td align="right"><b><?php echo $grand; ?></b></td>
	      </tr>
	    </tfoot>
    </table>
  </body>
</html>
