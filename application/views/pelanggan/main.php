<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view("component/head.php") ?>
	  <link rel="stylesheet" type="text/css" href="./dist/css/pagination.css">
	  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
  </head>
  <body>
    <div class="wrapper">
      <?php $this->load->view("component/sidebar.php") ?>

      <div id="content">
        <?php $this->load->view("component/navbar.php") ?>

      

        <section class="content-header">
        	<h1><?=$title1?></h1>
			    <ol class="breadcrumb" style="margin-bottom: 5px;">
						<li class="breadcrumb-item pl-3"><a href="<?=base_url('/dashboard')?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><?=$title1;?></li>
					</ol>
				</section>

        <section class="content">
          <div class="row">
            <div class="col-md-12">

              <div class="box main-layer">
              	<div class="box-header with-border my-1">
              		<div class="row">
              			<div class="col-md-6">
              				<!-- <a href="javascript:void(0)" class="btn btn-primary btn-add"><i class="fa fa-plus"></i> Tambah</a> -->
              			</div>
              			<dic class="col-md-6">
              				<div class="form-inline float-right">
              					<div class="form-group">
              						<select class="form-control search-option"></select>
              					</div>
              					<div class="form-group">
              						<input type="text" class="form-control search-input" placeholder="Search...">
              					</div>
              				</div>
              			</dic>
              		</div>
              	</div>

              	<p class="text-muted"><?php echo $this->session->flashdata('msg'); ?></p>

                <div class="box-body table-responsive">
                  <table class="table table-striped table-light text-dark" id="datagrid"></table>
                </div>

                <div class="box-footer clearfix">
                  <div class="row">
                    <div class="col-md-4 form-inline">
                      <div class="form-group">
                        <select class="form-control option"></select>
                      </div>
                    </div>
                    <div class="col-md-4 text-center info"></div>
                    <div class="col-md-4">
                      <ul class="pagination pagination-sm no-margin pull-right pagination-main"></ul>
                    </div>
                  </div>
                </div>
              </div>

              <div class="other-page"></div>
            	<div class="modal-dialog"></div>

            </div>
          </div>
        </section>
            
      </div>

        <!-- <?php $this->load->view("component/footer.php") ?> -->
    </div>

   <script type="text/javascript">
      $(document).ready(function () {
          $('#sidebarCollapse').on('click', function () {
              $('#sidebar').toggleClass('active');
          });
      });

      var datagrid;
      $(document).ready(function() {
        datagrid = $("#datagrid").datagrid({
          url           			: '<?php echo base_url('get-pelanggan'); ?>', 
          primaryField      	: 'id_pelanggan', 
          rowNumber       		: true,
          searchInputElement  : '.search-input',
          searchFieldElement  : '.search-option',
          pagingElement       : '.pagination-main',
          optionPagingElement : '.option',
          pageInfoElement     : '.info',
          columns         		: [
              {field: 'username', title: 'Username', editable: true, sortable: true, width: 100, align: 'left', search: true},
              {field: 'email', title: 'Email', editable: true, sortable: true, width: 180, align: 'left', search: true},
             	{field: 'nama_lengkap', title: 'Nama', editable: true, sortable: true, width: 180, align: 'left', search: true},
             	{field: 'nomor_hp', title: 'Nomor HP', editable: true, sortable: true, width: 180, align: 'left', search: true},
             	{field: 'tipe_pelanggan', title: 'Tipe Pelanggan', editable: true, sortable: true, width: 180, align: 'center', search: true},
              {field: 'menu', title: 'Menu', sortable: false, width: 200, align: 'center', search: false, 
                  rowStyler: function(rowData, rowIndex) {
                    return menu(rowData, rowIndex);
                  }
                }
            ]
          });
        datagrid.run();
      });

 

      function menu(rowData, rowIndex) {
        if (rowData.tipe_pelanggan == "Reguler") {
	        var menu = 
	        '<div class="btn-group"><a onclick="detail('+rowData.id_pelanggan+')" href="javascript:void(0);" class="btn btn-sm btn-primary"><i class="fa fa-info"></i> Detail</a>'+
	        '<a onclick="member('+rowData.id_pelanggan+')" href="javascript:void(0);" class="btn btn-sm btn-warning"><i class="fa fa-user-plus"></i> Jadikan Member</a></div>';
        }else{
        	var menu = 
	        '<div class="btn-group"><a onclick="detail('+rowData.id_pelanggan+')" href="javascript:void(0);" class="btn btn-sm btn-primary"><i class="fa fa-info"></i> Detail</a>'+
	        '<a onclick="hapusmember('+rowData.id_pelanggan+')" href="javascript:void(0);" class="btn btn-sm btn-danger"><i class="fa fa-undo"></i> Hapus Member</a></div>';
        }

        return menu;
      }

      function detail(id) {
	    	$.ajax({
      		type: 'POST',
      		url: '<?php echo site_url('detail-pelanggan')?>',
      		async : true,
          dataType : 'json',
      		data: {id:id},
      		success: function(response) { 
      			if(response.status == 'success'){
		          $('.modal-dialog').html(response.content);
		        } else {
		          return "";
		        }
      		}
      	});
	    }


	    function member(id) {
      	$.ajax({
      		type: 'POST',
      		url: '<?php echo site_url('add-member')?>',
      		async : true,
          dataType : 'json',
      		data: {id:id},
      		success: function(response) { 
      			if(response.status == 'success'){
		          location.reload();
		        }
      		}
      	});
	    }

	    function hapusmember(id) {
	    	$.ajax({
      		type: 'POST',
      		url: '<?php echo site_url('hapus-member')?>',
      		async : true,
          dataType : 'json',
      		data: {id:id},
      		success: function(response) { 
      			if(response.status == 'success'){
		          location.reload();
		        }
      		}
      	});
	    }
    </script>
  </body>
</html>