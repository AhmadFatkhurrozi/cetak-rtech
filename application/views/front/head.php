<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="<?=base_url('dist/img/logo-full.png')?>">

<title>SIMAPTRA - RTECH</title>

<?php 
    echo __css('fontawesome');
    echo __css('bootstrap');
    echo __css('fontastic');

    echo __js('jquery'); 
    echo __js('popper');
    echo __js('bootstrap');
    echo __js('solid');
    echo __js('fontawesome5');
    ?>

    <style type="text/css">
        @media (min-width: 768px) {
            .container h3::before {
                background: #eee none repeat scroll 0 0;
                content: "";
                display: inline-block;
                height: 2px;
                left: 20%;
                position: absolute;
                top: 18px;
                width: 18%;
            }

            .container h3::after{
                background: #eee none repeat scroll 0 0;
                content: "";
                display: inline-block;
                height: 2px;
                position: absolute;
                right: 20%;
                top: 18px;
                width: 18%;
            }

            div.cekStatus{
                position: absolute; 
                z-index: 99; 
                margin: -540px 0px 200px 0px; 
                width: 100%;
            }

        }
        nav.b-color{
            background-color: rgb(162 132 177 / 0.9);
        }

        @media (max-width: 480px) {
            div.cekStatusMobile{
                z-index: 99; 
                margin-top: 10px; 
                width: 100%;
            }
        }

        .container h3 {
            font-family: "Dosis-Medium";
            font-size: 2.2em;
            position: relative;
            text-align: center;
        }

        .produkTerbaru{
            object-fit: cover;
        }

    </style>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('dist/css/produk_grid.css'); ?>">
    <script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/dist/js/app.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/datagrid/datagrid.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/sweetalert/dist/sweetalert.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>