<nav class="navbar navbar-expand-md navbar-dark fixed-top b-color">
  <a class="navbar-brand" href="<?=base_url();?>"><img src="<?=base_url('dist/img/logo-text.png')?>" height="40px"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse float-right" id="navbarCollapse">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link text-light" href="<?=base_url();?>">Beranda</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-light" href="<?=base_url('produk-kami');?>">Produk Kami</a>
      </li>

    	<?php $kategori = $this->db->get('kategori')->result(); ?>
    	<?php foreach ($kategori as $key): ?>
    	<li class="nav-item">
        <a href="<?=base_url('kategori/'.$key->id_kategori);?>" class="nav-link text-light"><?=$key->nama_kategori;?></a>
      </li>
    	<?php endforeach ?>
      
      <?php if($this->session->userdata('pelanggan_id') != ''){ ?> 
      <li class="nav-item">
        <a href="<?=base_url('pesanan-saya');?>" class="nav-link text-light">Pesanan Saya</a>
      </li>
    	<?php } ?>
    
    </ul>
    <form class="form-inline my-2 my-md-0">
        <!-- <input class="form-control" type="text" placeholder="Search"> -->
      	
      	<?php if($this->session->userdata('pelanggan_id') == ''){ ?> 
        <a class="nav-link btn btn-success btn-sm mr-2" href="<?=base_url('login')?>">Masuk</a>
        <a class="nav-link btn btn-secondary btn-sm" href="<?=base_url('register')?>">Daftar</a>
        <?php }else{ ?>
        <a class="nav-link text-light" href="#"><i class="fa fa-user"></i> <?=$this->session->userdata('pelanggan_nama')?> <span>(<?=$this->session->userdata('pelanggan_tipe')?>)</span></a>
        <a class="nav-link text-light" href="<?=base_url('logout'); ?>" onclick="return confirm('Keluar ?')"><i class="fas fa-sign-out-alt"></i> Keluar</a>
        <?php } ?>
    </form>
  </div>
</nav>