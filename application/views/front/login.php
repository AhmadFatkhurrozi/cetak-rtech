<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view("front/head.php") ?>
  <style type="text/css">
    .text-login{
      font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
        font-weight: 400;
        font-size: 14px;
        color: #666666;
    }
  </style>
</head>
<body>
  <?php $this->load->view("front/navbar.php") ?>

  <div class="container">
      <div class="row">
        <div class="col-lg-6 offset-md-3" style="padding: 100px 0px;">
            <div class="login-logo text-center">
              <img src="<?=base_url('dist/img/logo-color.png');?>" style="height: 100%; width: 180px;">
            </div>
            <div class="content" style="padding: 20px 40px;">
              <form action="<?=base_url('FrontEndController/doLogin');?>" method="post" class="form-validate">
                <div class="form-group">
                  <input id="login-username" type="text" name="username" autocomplete="off" autofocus="off" required data-msg="Please enter your username" class="form-control" placeholder="username">
                </div>

                <div class="form-group">
                  <input id="login-password" type="password" name="password" autocomplete="off" autofocus="off" required data-msg="Please enter your password" class="form-control" placeholder="password">
                </div>
                <br>
                <button type="submit" class="btn btn-primary btn-block mb-4"><i class="fa fa-sign-in-alt"></i> Login</button> 
              </form>
              <p id="notifications"><?php echo $this->session->flashdata('msg'); ?></p>
              <br>

            	<span style="padding-top: 140px;">Belum punya akun ? <a href="<?=base_url('register');?>" style="font-weight:bold; color: green;"> Daftar Sekarang</a></span>
            </div>
            
        </div>
      </div>
  </div>

  <?php $this->load->view("front/footer.php") ?>
</body>
</html>