<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("front/head.php") ?>
	<style type="text/css">
		.text-atas{
			font-family: "Permanent Marker",cursive;
    		font-weight: 400 !important;
		}
		h1{
			font-family: "Open Sans",sans-serif;
		}

		.blockquote-custom {
		  position: relative;
		  font-size: 1.1rem;
		}

		.blockquote-custom-icon {
		  width: 50px;
		  height: 50px;
		  border-radius: 50%;
		  display: flex;
		  align-items: center;
		  justify-content: center;
		  position: absolute;
		  top: -25px;
		  left: 50px;
		}
	</style>
</head>
<body>
	<?php $this->load->view("front/navbar.php") ?>
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
		</ol>
		<div class="carousel-inner">
			<div class="carousel-item active">
				<img class="first-slide" src="<?=base_url('dist/slide/slide1.jpg');?>" width="100%" alt="First slide">
			</div>
			<div class="carousel-item">
				<img class="second-slide" src="<?=base_url('dist/slide/slide2.jpg');?>" width="100%" alt="Second slide">
			</div>
			<div class="carousel-item">
				<img class="third-slide" src="<?=base_url('dist/slide/slide3.jpg');?>" width="100%" alt="Third slide">
			</div>
		</div>
		<a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>

	<div class="text-center cekStatus cekStatusMobile" style="padding: 50px 0px;">
		<!-- <h3 class="m-3">Cek Status Pesanan Anda</h3> -->
		<h1 class="text-atas m-3">
			<span class="text-dark">Cek Status</span>
			<span class="text-warning">Pesanan Anda</span>
		</h1>
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-12 offset-lg-4">
					<form action="<?=base_url('cek-pesanan');?>" method="POST">
						<fieldset class="form-group">
							<input type="text" name="kode" class="form-control" placeholder="Enter Code">
						</fieldset>
						<p><button class="btn btn-success" type="submit" role="button"><i class="fas fa-search"></i> Cek Status</button></p>
					</form>
              		<p id="notifications"><?php echo $this->session->flashdata('msg'); ?></p>
				</div>
			</div>
		</div>
	</div>

	<div class="container my-5">
		<div class="row">
            <div class="col-lg-6 mx-auto">

                <blockquote class="blockquote blockquote-custom bg-white p-5 shadow rounded">
                    <div class="blockquote-custom-icon bg-info shadow-sm"><i class="fa fa-quote-left text-white"></i></div>
                    <p class="mb-0 mt-2 font-italic">Jadi Member, dan dapatkan harga menarik! info lebih lanjut hubungi Admin</p>
                    <footer class="blockquote-footer pt-4 mt-4 border-top">
                        <cite title="Source Title">Email : <?=$this->db->where('level_user', 1)->get('users')->row()->email;?></cite>
                    </footer>
                    <footer class="blockquote-footer pt-4 mt-4 border-top">
                        <cite title="Source Title">No. Telp : <?=$this->db->where('level_user', 1)->get('users')->row()->no_telp;?></cite>
                    </footer>
                </blockquote>
            </div>
        </div>
	</div>
	<br>
	<div class="container">
		<h3>Produk Kami</h3><br/>
		<div class="row">
			<?php if ($produk->num_rows() > 0){ ?>
				<?php foreach ($produk->result() as $r){ ?>
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 feature-grid" style="width: 100% !important;">
						<a href="<?php echo base_url('checkout/'.$r->id_barang) ?>">
							<?php if($r->gambar !== null){?>
								<img width="100%" height="256" class="produkTerbaru" src="<?=base_url('dist/img/barang/'.$r->gambar)?>" alt=""/> 
							<?php }else{ ?>
								<img width="100%" height="256" class="produkTerbaru" src="<?=base_url('dist/img/default.jpg')?>" alt=""/> 
							<?php } ?>

							<div class="arrival-info text-center">
								<h4 class="text-dark"><?php echo $r->nama_barang; ?></h4>
                                <p><span class="badge badge-info"><?=$r->nama_kategori?></span></p>
							</div>
							<div class="viw">
								<a href="<?php echo base_url('checkout/'.$r->id_barang) ?>">
									<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
									View
								</a>
							</div>
						</a>
					</div>
				<?php }} ?>
			</div>
			<div class="text-center">
				<a href="<?=base_url('produk-kami')?>" class="btn btn-success m-3">Selengkapnya...</a>
			</div>
		</div>
	</div>
	
	<?php $this->load->view("front/footer.php") ?>
</body>
</html>