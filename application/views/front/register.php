<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view("front/head.php") ?>
  <style type="text/css">
    .text-login{
      font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
        font-weight: 400;
        font-size: 14px;
        color: #666666;
    }
  </style>
</head>
<body>
  <?php $this->load->view("front/navbar.php") ?>


  <div class="container" style="padding: 100px 0px;">
      <div class="login-logo text-center">
        <h4>Daftar Akun Baru</h4>
      </div>

      <form action="<?=base_url('simpan-pelanggan');?>" method="post" class="form-validate" enctype="multipart/form-data">
      <div class="row" style="padding: 20px 40px;">
        <div class="col-lg-6">
            <div class="content">
                <div class="form-group">
                  <label>Username</label>
                  <input type="text" name="username" onkeyup="cekUsername()" id="username" required placeholder="Masukkan Username" class="form-control">
                  <p class='messageError errorUsername'></p>
                  <input type='hidden' name='statusUsername' value='Exist' id='statusUsername' class='form-control'>
                </div>

                <div class="form-group">
                  <label>Password</label>
                  <input type="password" name="password" required placeholder="Masukkan Password" class="form-control">
                </div>

                <div class="form-group">
                  <label>Nama Lengkap</label>
                  <input type="text" name="nama" required placeholder="Masukkan Nama" class="form-control">
                </div>

                <div class="form-group">
                  <label>Email</label>
                  <input type="email" name="email" onkeyup="cekMail()" id="email" required placeholder="Masukkan Email" class="form-control">
                  <p class='messageError errorMail'></p>
                  <input type='hidden' name='statusMail' value='Exist' id='statusMail' class='form-control'>
                </div>

                <div class="form-group">
                  <label>Nomor Hp</label>
                  <input type="text" name="no_hp" required placeholder="Masukkan Nomor Hp" class="form-control">
                </div>
            </div>
        </div>

        <div class="col-lg-6">

          <div class="form-group">
            <label>Foto KTP</label>
            <input type="file" name="foto_ktp" accept="application/pdf, image/png, image/jpeg" required class="form-control">
          </div>

          <div class="form-group">
            <label>Swafoto (dengan memegang KTP)</label>
              <input type="file" name="swafoto" accept="application/pdf, image/png, image/jpeg" required class="form-control">
          </div>

          <div class="form-group">
            <label>Alamat</label>
            <textarea name="alamat" class="form-control" rows="4" placeholder="Masukkan Alamat"></textarea>
          </div>

          <button type="submit" class="btn btn-success btn-submit btn-block mb-3">Daftar</button> 
          <p id="notifications"><?php echo $this->session->flashdata('msg'); ?></p>

          <span style="padding-top: 140px;">Sudah punya akun ? <a href="<?=base_url('login');?>" style="font-weight:bold; color: blue;"> Masuk</a></span>
        </div>
      </div>
      </form>
  </div>

  <?php $this->load->view("front/footer.php") ?>

  <script type="text/javascript">
  	var onLoad = (function() {
        disabledBtn();
    })();

  	function disabledBtn() {
	    var stMail = $('#statusMail').val();
	    var stUsername = $('#statusUsername').val();
	    if (stMail == 'Ready' && stUsername == 'Ready') {
	      $('.btn-submit').removeAttr('disabled');
	    }else{
	      $('.btn-submit').attr('disabled', true);
	    }
	  }

  	function cekMail() {
    var email = $('#email').val();
    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");
    if (email != '') {
      if (atpos < 1 || dotpos < atpos+2 || dotpos+2 >= email.length) {
        $('#grMail').attr('class','form-group has-error');
        $('#icon_email').attr('class','iconStatus text-red');
        $('#icon_email').html('<i class="fa fa-warning"></i>');
        $('.errorMail').html('Email Tidak Sesuai');
        $('#statusMail').val('Exist');
        disabledBtn();
      }else{
      	$.ajax({
      		type: 'POST',
      		url: '<?php echo site_url('cek-email')?>',
      		async : true,
          dataType : 'json',
      		data: {email:email},
      		success: function(response) { 
	          if (response.status == 'success') {
	            $('#grMail').attr('class','form-group has-error');
	            $('#icon_email').attr('class','iconStatus text-red');
	            $('#icon_email').html('<i class="fa fa-warning"></i>');
	            $('.errorMail').html('Email Telah Terdaftar');
	            $('#statusMail').val('Exist');
	            disabledBtn();
	          }else{
	            $('#grMail').attr('class','form-group');
	            $('#icon_email').attr('class','iconStatus text-green');
	            $('#icon_email').html('<i class="fa fa-check-circle"></i>');
	            $('.errorMail').html('');
	            $('#statusMail').val('Ready');
	            disabledBtn();
	          }
	        }
        });
      }
    }else{
      $('#grMail').attr('class','form-group');
      $('#icon_email').attr('class','iconStatus');
      $('#icon_email').html('');
      $('.errorMail').html('');
      $('#statusMail').val('Exist');
      disabledBtn();
    }
  }

  	function cekUsername() {
    var username = $('#username').val();
    if (username != '') {
    	$.ajax({
      		type: 'POST',
      		url: '<?php echo site_url('cek-username')?>',
      		async : true,
          dataType : 'json',
      		data: {username:username},
      		success: function(response) { 
		        if (response.status == 'success') {
		          $('#grUsername').attr('class','form-group has-error');
		          $('#icon_username').attr('class','iconStatus text-red');
		          $('#icon_username').html('<i class="fa fa-warning"></i>');
		          $('.errorUsername').html('Username Telah Terdaftar');
		          $('#statusUsername').val('Exist');
		          disabledBtn();
		        }else{
		          $('#grUsername').attr('class','form-group');
		          $('#icon_username').attr('class','iconStatus text-green');
		          $('#icon_username').html('<i class="fa fa-check-circle"></i>');
		          $('.errorUsername').html('');
		          $('#statusUsername').val('Ready');
		          disabledBtn();
		        }
		      }
  		});
    }else{
      $('#grUsername').attr('class','form-group');
      $('#icon_username').attr('class','iconStatus');
      $('#icon_username').html('');
      $('.errorUsername').html('');
      $('#statusUsername').val('Exist');
      disabledBtn();
    }
  }
  </script>
</body>
</html>