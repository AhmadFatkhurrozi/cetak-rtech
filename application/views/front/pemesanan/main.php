<!DOCTYPE html>
<html lang="en">
<head>
    <?php $this->load->view("front/head.php") ?>
    <style type="text/css">
        .text-atas{
            font-family: "Permanent Marker",cursive;
            font-weight: 400 !important;
        }
        h1{
            font-family: "Open Sans",sans-serif;
        }
    </style>
</head>
<body>
    <?php $this->load->view("front/navbar.php") ?>

    <hr class="featurette-divider">
    <div class="container" style="padding-top: 60px;">
        <p id="notifications"><?php echo $this->session->flashdata('msg'); ?></p>

        <h3>Pesanan Saya</h3><br/>
        <?php foreach ($pemesanan->result() as $a){ ?>
        <div class="bg-light p-3">
            <div class="row">
                <div class="col-5 col-md-3 col-sm-12 text-center">
                    <img src="<?=base_url('dist/img/barang/'.$a->gambar)?>" height="100px" style="vertical-align: middle;">
                </div>
                <div class="col-7 col-md-9 col-sm-12">
                	<?php $query = $this->db->where('pemesanan_id', $a->id_pemesanan)->limit(1)->order_by('id_status', 'desc')->get('status_pemesanan')->row(); ?>
                    <small class="pull-right"><?=date('d-m-Y H:i', strtotime($query->tanggal));?> | <?=$query->status.' | '.$query->user;?></small>  
                    <h4 class="py-3"><?=$a->nama_barang;?></h4>
                    <p>Kode Pesanan : <b><?=$a->kode;?></b></p>  
                    <a href="<?=base_url('status/'.$a->id_pemesanan)?>" class="btn btn-success btn-sm pull-right mt-2"><i class="fa fa-info"></i> detail pesanan</a>
                </div>    
            </div>
        </div>
        <br>
        <?php } ?>
    </div>
    
    <?php $this->load->view("front/footer.php") ?>
</body>
</html>