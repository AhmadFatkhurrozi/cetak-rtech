<!DOCTYPE html>
<html lang="en">
<head>
    <?php $this->load->view("front/head.php") ?>

	<script src="<?php echo base_url('assets/plugins/sweetalert/dist/sweetalert.min.js'); ?>"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/sweetalert/dist/sweetalert.css'); ?>">

    <style type="text/css">
        .text-atas{
            font-family: "Permanent Marker",cursive;
            font-weight: 400 !important;
        }
        h1{
            font-family: "Open Sans",sans-serif;
        }
    </style>
</head>
<body>
    <?php $this->load->view("front/navbar.php") ?>

    <hr class="featurette-divider">
    <div class="container" style="padding-top: 60px;">
      	<?php if($this->session->userdata('pelanggan_id') == ''){ ?> 
        <h4 class="text-center">Detail Produk</h4><hr class="mb-4">
    	<?php } else {  ?>
        <h4 class="text-center">Konfirmasi Pemesanan</h4><hr class="mb-4">
        <?php } ?>
        <div class="row">
           <div class="col-md-6 text-center p-3">
                <?php if($pemesanan->gambar == null){ ?>
                    <img width="80%" class="img-responsive" src="<?=base_url('dist/img/default.jpg')?>">
                <?php }else{ ?>
                    <img width="80%" class="img-responsive" src="<?=base_url('dist/img/barang/'.$pemesanan->gambar)?>">
                <?php } ?>
           </div>

           <div class="col-md-6 p-3">
              <form id="form-pesanan">
                <fieldset class="form-group">
                  <input type="hidden" name="id_barang" id="id_barang" value="<?=$pemesanan->id_barang;?>">
                  <input type="hidden" name="id_pelanggan" value="<?=$this->session->userdata('pelanggan_id');?>">
                  <input type="text" class="form-control" name="nama_barang" value="<?=$pemesanan->nama_barang;?>" readonly>
                </fieldset>
				<fieldset class="form-group">
					<textarea name="deskripsi" id="deskripsi" class="form-control" rows="4" readonly=""><?=$pemesanan->deskripsi;?></textarea>
                </fieldset>
                <fieldset class="form-group">
                  <select class="form-control" name="ukuran" id="ukuran" onchange="getHarga()">
                  	<option selected="" disabled="" value="">.:: pilih ukuran ::.</option>
                  	<?php foreach ($harga->result() as $a){ ?>
                  		<option value="<?=$a->ukuran?>"><?=$a->ukuran?></option>
                  	<?php } ?>
                  	<option value="Custom">Custom</option>
                  </select>
                </fieldset>
                <fieldset class="form-group">
                  <input type="number" class="form-control" onchange="getHarga()" name="jumlah" id="jumlah" placeholder="jumlah">
                </fieldset>
                <fieldset class="form-group">
                  <input type="text" class="form-control" name="total" id="total" placeholder="total harga" readonly="">
                </fieldset>

      			<?php if($this->session->userdata('pelanggan_id') != ''){ ?> 
                <fieldset class="form-group">
                  <input type="text" class="form-control" name="no_wa" placeholder="No. WhatsApp">
                </fieldset>
                <fieldset class="form-group">
					<textarea name="catatan" id="catatan" class="form-control" rows="4" placeholder="catatan"></textarea>
					<span class="text-danger"><i>masukkan ukuran di catatan jika pilih ukuran Custom</i></span>
                </fieldset>
				<fieldset class="form-group">
				  <label for="desain">Desain <i>(Opsional)</i></label>
                  <input type="file" class="form-control" name="desain" id="desain">
                </fieldset>
                <div class="text-center">
                	<button type="submit" class="btn btn-success text-light btn-save">Konfirmasi Pesanan</button>
                </div>
            	<?php } ?>
              </form>
           </div>
        </div>
    </div>
    
    <?php $this->load->view("front/footer.php") ?>
    <script type="text/javascript">

    	function getHarga() {
    		var id_barang = $('#id_barang').val(); 
			var ukuran = $('#ukuran').val(); 
			var jumlah = $('#jumlah').val(); 
    		$.ajax({
	      		type: 'POST',
	      		url: '<?php echo site_url('getHarga')?>',
	      		async : true,
	          	dataType : 'json',
	      		data: {id_barang:id_barang, ukuran:ukuran},
	      		success: function(response) { 
	      			if(response.status == 'success'){
	      				if (response.tipe_pelanggan == "Reguler") {
			          		var harga = response.data.harga;
	      				}else{
			          		var harga = response.data.harga_member;
	      				}

						if (jumlah == "") {
							var total = harga;
						}else{
							var total = harga * jumlah;
						}

						$('#total').val(ribuan(total));
			        } else {
						$('#total').val(0);
			          return "";
			        }
	      		}
	      	});
    	}

    	function ribuan(nominal) {
	        var bilangan = nominal;
	        var reverse = bilangan.toString().split('').reverse().join(''),
	        ribuan  = reverse.match(/\d{1,3}/g);
	        ribuan  = ribuan.join('.').split('').reverse().join('');
	        return "Rp "+ ribuan;
	    }

 		$(document).on("submit", "#form-pesanan", function(e){
    		e.preventDefault()
			swal({
				title:"Konfirmasi Pesanan",
				text:"Yakin data sudah benar ?",
				type: "info",
				showCancelButton: true,
				confirmButtonText: "Konfirmasi",
				closeOnConfirm: true,
			},
				function(){
				 $.ajax({
					url:"<?php echo base_url('simpan-pesanan'); ?>",
					method: "POST",
					data: new FormData($('#form-pesanan')[0]),
			        contentType: false,
			        processData: false,
					success: function(response) { 
						var data = $.parseJSON(response);
		      			if(data.status == 'success'){
		      			  	window.setTimeout(function(){
			      			  	swal({
						            title: "Berhasil!",
						            text: "Pemesanan Berhasil!",
						            type: "success"
						        }, function() {
					          		window.location.href = "<?=base_url('status/')?>"+data.id;
						        });
				           	}, 2000);
				        } else {
				        	swal({
					            title: "Maaf!",
					            text: "Silahkan Login Terlebih Dahulu!",
					            type: "warning"
					        });
				        }
		      		}
				 });
			});
		});
    </script>
</body>
</html>