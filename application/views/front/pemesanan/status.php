<!DOCTYPE html>
<html lang="en">
<head>
    <?php $this->load->view("front/head.php") ?>

    <script src="<?php echo base_url('assets/plugins/sweetalert/dist/sweetalert.min.js'); ?>"></script>
    <link rel="stylesheet" type="text/css" href="">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/sweetalert/dist/sweetalert.css'); ?>">

    <style type="text/css">
        .text-atas{
            font-family: "Permanent Marker",cursive;
            font-weight: 400 !important;
        }
        h1{
            font-family: "Open Sans",sans-serif;
        }

        .vertical-timeline {
            width: 100%;
            position: relative;
            padding: 1.5rem 0 1rem
        }

        .vertical-timeline::before {
            content: '';
            position: absolute;
            top: 0;
            /*left: 67px;*/
            left: 115px;
            height: 100%;
            width: 4px;
            background: #e9ecef;
            border-radius: .25rem
        }

        .vertical-timeline-element {
            position: relative;
            margin: 0 0 1rem
        }

        .vertical-timeline--animate .vertical-timeline-element-icon.bounce-in {
            visibility: visible;
            animation: cd-bounce-1 .8s
        }

        .vertical-timeline-element-icon {
            position: absolute;
            top: 0;
            left: 108px;
        }

        .vertical-timeline-element-icon .badge-dot-xl {
            box-shadow: 0 0 0 5px #fff
        }

        .badge-dot-xl {
            width: 18px;
            height: 18px;
            position: relative
        }

        .badge:empty {
            display: none
        }

        .badge-dot-xl::before {
            content: '';
            width: 10px;
            height: 10px;
            border-radius: .25rem;
            position: absolute;
            left: 50%;
            top: 50%;
            margin: -5px 0 0 -5px;
            background: #fff
        }

        .vertical-timeline-element-content {
            position: relative;
            margin-left: 90px;
            font-size: .8rem
        }

        .kanan{
            margin-left: 50px;
        }

        .vertical-timeline-element-content .timeline-title {
            font-size: .8rem;
            text-transform: uppercase;
            margin: 0 0 .5rem;
            padding: 2px 0 0;
            font-weight: bold
        }

        .vertical-timeline-element-content .vertical-timeline-element-date {
            display: block;
            position: absolute;
            left: -90px;
            top: 0;
            padding-right: 100px;
            text-align: right;
            color: #adb5bd;
            font-size: .7619rem;
            white-space: nowrap
        }

        .vertical-timeline-element-content:after {
            content: "";
            display: table;
            clear: both
        }
    </style>
</head>
<body>
    <?php $this->load->view("front/navbar.php") ?>

    <hr class="featurette-divider">
    <div class="container" style="padding-top: 60px;">
        <h4 class="text-center">Status Pemesanan</h4><hr class="mb-4">
        <div class="row">
           <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center p-3">
                <?php if($pemesanan->gambar == null){ ?>
                    <img width="80%" class="img-responsive" src="<?=base_url('dist/img/default.jpg')?>">
                <?php }else{ ?>
                    <img width="80%" class="img-responsive" src="<?=base_url('dist/img/barang/'.$pemesanan->gambar)?>">
                <?php } ?>
           </div>

           <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-3">
              <form id="form-pesanan" action="<?=base_url('simpan-pesanan')?>" method="POST">
              	<div class="row">
                	<div class="col-5 col-md-3 col-sm-6 font-weight-bold">Kode Pesanan</div>
                  	<div class="col-7 col-md-9 col-sm-6">
                  		<span class="font-weight-bold"> : <?=$pemesanan->kode;?></span>
                  	</div>
                </div>                
                <hr>
                <div class="row">
                	<div class="col-5 col-md-3 col-sm-6">Nama Produk</div>
                  	<div class="col-7 col-md-9 col-sm-6">
                  		<input type="hidden" name="id_barang" value="<?=$pemesanan->id_barang;?>">
                  		<input type="hidden" name="id_pelanggan" value="<?=$this->session->userdata('pelanggan_id');?>">
                  		<span> : <?=$pemesanan->nama_barang;?></span>
                  	</div>
                </div>
                <div class="row">
                	<div class="col-5 col-md-3 col-sm-6">Ukuran</div>
                  	<div class="col-7 col-md-9 col-sm-6">
                  		<span> :  <?=$pemesanan->ukuran;?> </span>
                  	</div>
                </div>
                <div class="row">
                	<div class="col-5 col-md-3 col-sm-6">Jumlah</div>
                  	<div class="col-7 col-md-9 col-sm-6">
                  		<span> :  <?=$pemesanan->jumlah;?> </span>
                  	</div>
                </div>
                <div class="row">
                	<div class="col-5 col-md-3 col-sm-6">Nomor WA</div>
                  	<div class="col-7 col-md-9 col-sm-6">
                  		<span> :  <?=$pemesanan->no_wa;?> </span>
                  	</div>
                </div>
                <hr>
                <?php 
                	if ($pemesanan->biaya_tambahan == '') {
                		$biayatambahan = 0;
                	}else{
                		$biayatambahan = $pemesanan->biaya_tambahan;
                	}

                	$total = $pemesanan->total_harga+$biayatambahan;
                ?>
                <div class="row">
                	<div class="col-6 col-md-6 col-sm-6 font-weight-bold">Biaya</div>
                  	<div class="col-6 col-md-6 col-sm-6">
                  		<span class="font-weight-bold"> Rp <?=number_format($pemesanan->total_harga, 0,',','.');?></span>
                  	</div>
                </div>
                <div class="row">
                	<div class="col-6 col-md-6 col-sm-6 font-weight-bold">Biaya Tambahan</div>
                  	<div class="col-6 col-md-6 col-sm-6">
                  		<span class="font-weight-bold"> Rp <?=number_format($biayatambahan, 0,',','.');?></span>
                  	</div>
                </div>
                <hr>
                <div class="row">
                	<div class="col-6 col-md-6 col-sm-6 font-weight-bold">Total Biaya</div>
                  	<div class="col-6 col-md-6 col-sm-6">
                  		<span class="font-weight-bold"> Rp <?=number_format($total, 0,',','.');?></span>
                  	</div>
                </div>
                <div class="row">
                	<div class="col-6 col-md-6 col-sm-6 font-weight-bold">Sudah Dibayar</div>
                  	<div class="col-6 col-md-6 col-sm-6">
                  		<span class="font-weight-bold"> Rp <?=number_format($pemesanan->sudah_bayar, 0,',','.');?></span>
                  	</div>
                </div>
                <hr>
                <div class="row">
                	<div class="col-6 col-md-6 col-sm-6 font-weight-bold">Total yang Harus Dibayar</div>
                  	<div class="col-6 col-md-6 col-sm-6">
                  		<span class="font-weight-bold"> Rp <?=number_format($total-$pemesanan->sudah_bayar, 0,',','.');?></span>
                  	</div>
                </div>
              </form>
           </div>
        </div>

        <div class="row py-5">
          <div class="col-md-12">
          <h3>Status</h3>            
            <div class="row d-flex justify-content-center mt-70 mb-70">
              <div class="col-md-9">
                <div class="main-card mb-3 card bg-light">
                  <div class="card-body">
                    <!-- <h5 class="card-title">Status Pemesanan</h5> -->
                    <div class="vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
                      
                      <?php if ($status->num_rows() > 0){ ?>
                      <?php foreach ($status->result() as $r){ ?>
                      <div class="vertical-timeline-item vertical-timeline-element">
                        <div> <span class="vertical-timeline-element-icon bounce-in"> 
                          <i class="badge badge-dot badge-dot-xl badge-info"> </i> </span>
                          <div class="vertical-timeline-element-content bounce-in">
                            <div class="kanan">
                              <h4 class="timeline-title"><?=$r->status;?></h4>
                            <p><?=$r->user;?> <?php if($r->catatan != ''){?>- <?=$r->catatan;?> <?php } ?></p> 
                            </div>
                            <span class="vertical-timeline-element-date"><?=date('d-m-Y H:i', strtotime($r->tanggal));?></span>
                          </div>
                        </div>
                      </div>
                      <?php }} ?>


                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
    
    <?php $this->load->view("front/footer.php") ?>
    <script type="text/javascript">
 
    </script>
</body>
</html>