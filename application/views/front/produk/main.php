<!DOCTYPE html>
<html lang="en">
<head>
    <?php $this->load->view("front/head.php") ?>
    <style type="text/css">
        .text-atas{
            font-family: "Permanent Marker",cursive;
            font-weight: 400 !important;
        }
        h1{
            font-family: "Open Sans",sans-serif;
        }
    </style>
</head>
<body>
    <?php $this->load->view("front/navbar.php") ?>

    <hr class="featurette-divider">
    <div class="container" style="padding-top: 60px;">
        <h3>Semua Produk</h3><br/>
        <div class="row">
            <?php if ($produk->num_rows() > 0){ ?>
                <?php foreach ($produk->result() as $r){ ?>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 feature-grid" style="width: 100% !important;">
                        <a href="<?php echo base_url('checkout/'.$r->id_barang) ?>">
                            <?php if($r->gambar !== null){?>
                                <img width="100%" height="256" class="produkTerbaru" src="<?=base_url('dist/img/barang/'.$r->gambar)?>" alt=""/> 
                            <?php }else{ ?>
                                <img width="100%" height="256" class="produkTerbaru" src="<?=base_url('dist/img/default.jpg')?>" alt=""/> 
                            <?php } ?>

                            <div class="arrival-info text-center">
                                <h4 class="text-dark"><?php echo $r->nama_barang; ?></h4>
                                <p><span class="badge badge-info"><?=$r->nama_kategori?></span></p>
                            </div>
                            <div class="viw">
                                <a href="<?php echo base_url('checkout/'.$r->id_barang) ?>">
                                    <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                    View
                                </a>
                            </div>
                        </a>
                    </div>
                <?php }} ?>
            </div>
        </div>
    </div>
    
    <?php $this->load->view("front/footer.php") ?>
</body>
</html>