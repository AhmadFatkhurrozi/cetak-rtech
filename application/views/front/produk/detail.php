<!DOCTYPE html>
<html lang="en">
<head>
    <?php $this->load->view("front/head.php") ?>
    <style type="text/css">
        .text-atas{
            font-family: "Permanent Marker",cursive;
            font-weight: 400 !important;
        }
        h1{
            font-family: "Open Sans",sans-serif;
        }
    </style>
</head>
<body>
    <?php $this->load->view("front/navbar.php") ?>

    <hr class="featurette-divider">
    <div class="container" style="padding-top: 60px;">
        <h3>Detail Produk</h3><br/>
        <div class="row">
           <div class="col-md-6 text-center">
                <?php if($produk->gambar == null){ ?>
                    <img width="80%" class="img-responsive" src="<?=base_url('dist/img/default.jpg')?>">
                <?php }else{ ?>
                    <img width="80%" class="img-responsive" src="<?=base_url('dist/img/barang/'.$produk->gambar)?>">
                <?php } ?>
           </div>

           <div class="col-md-6 text-center">
           		<h2><?=$produk->nama_barang;?></h2>
           		<p class="py-2"><?=$produk->deskripsi;?></p>
               <?php if($this->session->userdata('pelanggan_id') == ''){ ?> 
               	<span>Silahkan <a href="<?=base_url('login')?>">Login</a> jika ingin memesan...</span>
               <?php } else { ?>
               	<a href="<?=base_url('checkout/'.$produk->id_barang);?>" class="btn btn-success">Pesan</a>
               <?php } ?>
           </div>
        </div>
    </div>
    
    <?php $this->load->view("front/footer.php") ?>
</body>
</html>