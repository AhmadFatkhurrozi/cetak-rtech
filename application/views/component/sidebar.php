<style type="text/css">
    #sidebar ul li a{
        font-size: 14px !important;
        color: #E7DDDD;
    }

</style>
<nav id="sidebar" style="z-index: 999;">
	<div class="sidebar-header text-center">
		<a href="<?=base_url('admin')?>">
			<img src="<?=base_url('dist/img/logo-text.png');?>" style="width: 70%;">
		</a>
	</div>
	<?php 
	if ($this->session->userdata('auth_level_user') == '1') {
		$jumlahPesanan = $this->db->where('status', 'dibuat')->get('pemesanan')->num_rows(); 
	}else{
		$jumlahPesanan = $this->db->where('status', 'Di Proses')->where('karyawan_id', $this->session->userdata('auth_id'))->get('pemesanan')->num_rows(); 
	}
	?>
    <div class="user-panel mx-2 text-center" style="padding: 10px 0;">
    	<a href="<?=base_url('edit-profil')?>">
	        <div class="user-pic">
	           	<?php if ($this->db->where('id', $this->session->userdata('auth_id'))->get('users')->row()->foto != '') { ?>
	                <img src="<?=base_url('dist/upload/'.$this->db->where('id', $this->session->userdata('auth_id'))->get('users')->row()->foto);?>" style="border-radius: 50%; object-fit: cover;" height="60px" width="60px" class="img-circle" alt="User Image">
	           	<?php } else { ?>
	                <img src="<?=base_url('dist/img/foto.jpg');?>" height="60px" width="60px" style="border-radius: 50%; object-fit: cover;" class="img-circle" alt="User Image">
				<?php } ?>   
	        </div>

	        <div class="user-info my-2" style="font-size: 9px;">
				<span class="user-name">
					<strong><?=$this->session->userdata('auth_nama'); ?></strong>
				</span><br>
				<span class="user-status">
					<i class="fa fa-circle text-success"></i>
					<span>Online</span>
				</span>
			</div>
		</a>
    </div>

    <ul class="list-unstyled components">
        <li class="<?php echo __menu_active('Dashboard', $title2 ); ?>">
            <a href="<?=base_url('admin')?>">
                <i class="fas fa-home fa-fw"></i> <span>Dashboard</span>
            </a>
        </li>

        <li class="<?php echo __menu_active('Pemesanan', $title2 ); ?>">
        	<a href="<?=base_url('pemesanan')?>"><i class="fa fa-clipboard-list"></i> Pemesanan 
        		<?php if ($jumlahPesanan > 0) { ?>
        			<span class="badge badge-danger"><?=$jumlahPesanan?></span>
        		<?php } ?>
        	</a> 
	    </li>
	    
        <?php if($this->session->userdata('auth_level_user') != 2) { ?>
        
        <li class="<?php echo __menu_active('User', $title2 ); ?>">
        	<a href="<?=base_url('user')?>"><i class="fa fa-users"></i> User</a>
	    </li>
        <?php } ?>

	    <li class="<?php echo __menu_active('Barang', $title2 ); ?>">
        	<a href="<?=base_url('barang')?>"><i class="fa fa-list"></i> Data Barang</a>
	    </li>

	    <li class="<?php echo __menu_active('Kategori', $title2 ); ?>">
        	<a href="<?=base_url('kategori')?>"><i class="fa fa-list"></i> Data Kategori</a>
	    </li>
	    
	    <li class="<?php echo __menu_active('Pelanggan', $title2 ); ?>">
        	<a href="<?=base_url('pelanggan')?>"><i class="fa fa-users"></i> Data Pelanggan</a>
	    </li>

	    <?php if($this->session->userdata('auth_level_user') == 2) { ?>
	    <li class="<?php echo __menu_active('Pekerjaan Saya', $title2 ); ?>">
        	<a href="<?=base_url('pekerjaan-saya')?>"><i class="fa fa-list"></i> Pekerjaan Saya</a>
	    </li>
	    <?php } ?>

	    <?php if($this->session->userdata('auth_level_user') == 3) { ?>
	     <li class="<?php echo __menu_active('Capaian Karyawan', $title2 ); ?>">
        	<a href="<?=base_url('capaian-karyawan')?>"><i class="fa fa-star"></i> Capaian Karyawan</a>
	    </li>
	    <?php } ?>

	    <?php if($this->session->userdata('auth_level_user') != 2) { ?>
	     <!-- <li class="<?php echo __menu_active('Neraca', $title2 ); ?>">
        	<a href="<?=base_url('neraca')?>"><i class="fa fa-balance-scale"></i> Neraca</a>
	    </li>

		<div class="sidebar-header text-center">
			<span style="font-size: 12px;"> Laporan </span>
		</div> -->

		<li class="<?php echo __menu_active('Rekap Pesanan', $title2 ); ?>">
        	<a href="<?=base_url('neraca')?>"><i class="fa fa-file"></i> Rekap Pesanan</a>
	    </li>
		<?php } ?>
	    
	    <?php if($this->session->userdata('auth_level_user') == 3) { ?>
	    <!-- <li class="<?php echo __menu_active('Rekap Capaian Karyawan', $title2 ); ?>">
        	<a href="<?=base_url('rekap-capaian-karyawan')?>"><i class="fa fa-file"></i> Rekap Capaian Karyawan</a>
	    </li> -->
	    <?php } ?>

    </ul>

    <ul class="list-unstyled">
        <li>
            <a href="<?=base_url('admin-logout'); ?>" class="text-center" onclick="return confirm('Keluar ?')"><i class="fa fa-sign-out-alt"></i> Keluar</a>
        </li>
    </ul>
</nav>