<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="./dist/img/logo-full.png">

<title>SIMAPTRA - RTECH</title>

<!-- <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css');?>"> -->

<?php 
    echo __css('fontawesome');
    echo __css('bootstrap');
    echo __css('fontastic');
    echo __css('default');
    echo __css('custom');
    echo __css('style4');

	echo __js('jquery'); 
    echo __js('popper');
    echo __js('bootstrap');
    echo __js('solid');
    echo __js('fontawesome5');
?>

<script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/dist/js/app.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/datagrid/datagrid.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/sweetalert/dist/sweetalert.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>