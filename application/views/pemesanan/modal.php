<div class="modal fade" id="detail-dialog" tabindex="-1" role="dialog" aria-labelledby="product-detail-dialog">
  <div class="modal-dialog modal-lg" >
    <div class="modal-content">
      <div class="modal-header" style="width: 100%">
          <?=$title;?>
        <span style="float:right"><a data-dismiss="modal">Close</a></span>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="row">
              <input type="hidden" name="id_pemesanan" id="id_pemesanan" value="<?=$pemesanan->id_pemesanan;?>">
              <label class="col-md-4">Produk</label>
              <label class="col-md-8"> : <?=$pemesanan->nama_barang;?></label>
            </div>

            <div class="row">
              <label class="col-md-4">Jumlah</label>
              <label class="col-md-8"> : <?=$pemesanan->jumlah;?></label>
            </div>

            <div class="row">
              <label class="col-md-4">Ukuran</label>
              <label class="col-md-8"> : <?=$pemesanan->ukuran;?></label>
            </div>

            <div class="row">
              <label class="col-md-4">Nama</label>
              <label class="col-md-8"> : <?=$user;?></label>
            </div>

            <div class="row">
              <label class="col-md-4">WhatsApp</label>
              <label class="col-md-8"> : <?=$pemesanan->no_wa;?></label>
            </div>

            <div class="row">
              <label class="col-md-4">Catatan Pelanggan</label>
              <label class="col-md-8"> : <?=$pemesanan->catatan;?></label>
            </div>

            <?php if ($pemesanan->desain != '') { ?>
            <div class="row">
              <label class="col-md-4">Desain</label>
            	<label class="col-md-8"> : <a href="<?=base_url('upload/desain/'.$pemesanan->desain);?>" class="text-primary">File Desain</a></label>
            </div>
            <?php } else {?>
            <div class="row">
              <label class="col-md-4">Desain</label>
            	<label class="col-md-8"> : -</label>
            </div>
            <?php } ?>

          	<?php if($this->session->userdata('auth_level_user') == 1) { ?>
            <div class="row">
              <label class="col-md-4">Total Harga</label>
            	<label class="col-md-8"> <input type="text" name="total_harga" id="total_harga" class="form-control" value="<?=$pemesanan->total_harga;?>"></label>
            </div>
          	<?php }else{  ?>
          	<div class="row">
              <label class="col-md-4">Total Harga</label>
            	<label class="col-md-8"> <input type="text" name="total_harga" id="total_harga" readonly="" class="form-control" value="<?=$pemesanan->total_harga;?>"></label>
            </div>
          	<?php } ?>
          </div>
          
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          	<?php if($this->session->userdata('auth_level_user') == 1) { ?>
           	<div class="row">
              <label class="col-md-4">Biaya Tambahan</label>
            	<label class="col-md-8"><input type="text" name="biaya_tambahan" id="biaya_tambahan" class="form-control"></label>
            </div>
            <div class="row">
              <label class="col-md-4">Sudah Dibayar</label>
            	<label class="col-md-8"><input type="text" name="sudah_bayar" id="sudah_bayar" class="form-control"></label>
            </div>
          	<?php } else { ?>
          	<div class="row">
              <label class="col-md-4">Biaya Tambahan</label>
            	<label class="col-md-8"><input type="text" name="biaya_tambahan" id="biaya_tambahan" value="<?=$pemesanan->biaya_tambahan;?>" class="form-control" readonly=""></label>
            </div>
            <div class="row">
              <label class="col-md-4">Sudah Dibayar</label>
            	<label class="col-md-8"><input type="text" name="sudah_bayar" id="sudah_bayar" value="<?=$pemesanan->sudah_bayar;?>" class="form-control" readonly=""></label>
            </div>
          	<?php } ?>

          	<?php if($this->session->userdata('auth_level_user') == 1) { ?>
          	<div class="row">
              <label class="col-md-12">Karyawan</label>
              <label class="col-md-12">
              	<select class="form-control" name="karyawan_id" id="karyawan_id">
              		<option value="" disabled="" selected="">.:: pilih karyawan ::.</option>
              		<?php foreach ($karyawan->result() as $key): ?>
              			<option value="<?=$key->id;?>"><?=$key->nama;?></option>
              		<?php endforeach ?>
              	</select>
              </label>
            </div>
          	<?php } ?>

          	<div class="row">
              <label class="col-md-12">Catatan</label>
              <label class="col-md-12"><textarea name="catatan" id="catatan" rows="3" class="form-control"></textarea></label>
            </div>
          </div>

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
             <a href="javascript:void(0)" onclick="validasi('Di Tolak')" class="btn btn-danger btn-sm">Tolak</a>
             <a href="javascript:void(0)" onclick="validasi('Di Proses')" class="btn btn-warning btn-sm">Di Proses</a>
             <a href="javascript:void(0)" onclick="validasi('Selesai')" class="btn btn-success btn-sm">Selesai</a>           
          </div>
      </div>

      </div>
      <div class="clearfix" style='padding-bottom:20px'></div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('#detail-dialog').find('.modal-dialog').css({
      'width'     : '80%'
    });
    $('#detail-dialog').modal('show');
  })();

  $('#detail-dialog').on('hidden.bs.modal', function () {
    $('.modal-dialog').html('');
  });

  function validasi(isi) {
    swal({
        title:"Ubah Status => "+isi+" ?",
        text:"",
        type: "info",
        showCancelButton: true,
        confirmButtonText: "Konfirmasi",
        closeOnConfirm: true,
      },function(){
        
        var id = $('#id_pemesanan').val();
        var catatan = $('#catatan').val();
        var karyawan_id = $('#karyawan_id').val();
        var biaya_tambahan = $('#biaya_tambahan').val();
        var sudah_bayar = $('#sudah_bayar').val();
        var total_harga = $('#total_harga').val();

        $.ajax({
          type: 'POST',
          url:"<?php echo base_url('ubah-status-pesanan')?>",
          async : true,
          dataType : 'json',
          data: {id:id, status:isi, catatan:catatan, karyawan_id:karyawan_id, biaya_tambahan:biaya_tambahan, sudah_bayar:sudah_bayar, total_harga:total_harga},
          success: function(response) { 
            if(response.status == 'success'){
              window.setTimeout(function(){
                swal({
                  title: "Berhasil!",
                  text: "Status Berhasil Diubah!",
                  type: "success"
                }, function() {
                    location.reload();
                });
              }, 1000);
            } else {
			        window.setTimeout(function(){
                swal({
                  title: "Maaf!",
                  text: response.message,
                  type: "warning"
                }, function() {
                    location.reload();
                });
              }, 500);
            }
          }
        });
         
      });
  }

</script>
