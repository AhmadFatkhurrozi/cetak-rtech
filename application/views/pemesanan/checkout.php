<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view("component/head.php") ?>
      <link rel="stylesheet" type="text/css" href="./dist/css/pagination.css">
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
      <script src="<?php echo base_url('assets/plugins/sweetalert/dist/sweetalert.min.js'); ?>"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/sweetalert/dist/sweetalert.css'); ?>">
  </head>
  <body>
    <div class="wrapper">
      <?php $this->load->view("component/sidebar.php") ?>

      <div id="content">
        <?php $this->load->view("component/navbar.php") ?>

      

        <section class="content-header">
          <h1><?=$title1?></h1>
          <ol class="breadcrumb" style="margin-bottom: 5px;">
              <li class="breadcrumb-item pl-3"><a href="<?=base_url('/dashboard')?>">Dashboard</a></li>
              <li class="breadcrumb-item active"><?=$title1;?></li>
          </ol>
        </section>

        <section class="content">
          <div class="container" style="padding-top: 10px;">
            <h4 class="text-center">Konfirmasi Pemesanan</h4><hr class="mb-4">
            <div class="row">
               <div class="col-md-6 text-center p-3">
                    <?php if($pemesanan->gambar == null){ ?>
                        <img width="80%" class="img-responsive" src="<?=base_url('dist/img/default.jpg')?>">
                    <?php }else{ ?>
                        <img width="80%" class="img-responsive" src="<?=base_url('dist/img/barang/'.$pemesanan->gambar)?>">
                    <?php } ?>
               </div>

               <div class="col-md-6 p-3">
                  <form id="form-pesanan">
                    <fieldset class="form-group">
                      <input type="hidden" name="id_barang" id="id_barang" value="<?=$pemesanan->id_barang;?>">
                      <input type="hidden" name="id_pelanggan" value="<?=$this->session->userdata('pelanggan_id');?>">
                      <input type="text" class="form-control" name="nama_barang" value="<?=$pemesanan->nama_barang;?>" readonly>
                    </fieldset>
                    <fieldset class="form-group">
                      <select class="form-control" name="ukuran" id="ukuran" onchange="getHarga()">
                        <option selected="" disabled="" value="">.:: pilih ukuran ::.</option>
                        <?php foreach ($harga->result() as $a){ ?>
                          <option value="<?=$a->ukuran?>"><?=$a->ukuran?></option>
                        <?php } ?>
                      </select>
                    </fieldset>
                    <fieldset class="form-group">
                      <input type="number" class="form-control" onchange="getHarga()" name="jumlah" id="jumlah" placeholder="jumlah">
                    </fieldset>
                    <fieldset class="form-group">
                      <input type="text" class="form-control" name="nama" placeholder="Nama Pelanggan">
                    </fieldset>
                    <fieldset class="form-group">
                      <input type="text" class="form-control" name="no_wa" placeholder="No. WhatsApp">
                    </fieldset>
                    <fieldset class="form-group">
                      <input type="text" class="form-control" name="total" id="total" placeholder="total harga" readonly="">
                    </fieldset>
                    <div class="text-center">
                      <button type="submit" class="btn btn-success text-light btn-save">Konfirmasi Pesanan</button>
                    </div>
                  </form>
               </div>
            </div>
          </div>
        </section>
            
      </div>

        <!-- <?php $this->load->view("component/footer.php") ?> -->
    </div>

   <script type="text/javascript">
      function getHarga() {
        var id_barang = $('#id_barang').val(); 
        var ukuran = $('#ukuran').val(); 
        var jumlah = $('#jumlah').val(); 
          $.ajax({
              type: 'POST',
              url: '<?php echo site_url('getHarga')?>',
              async : true,
                dataType : 'json',
              data: {id_barang:id_barang, ukuran:ukuran},
              success: function(response) { 
                if(response.status == 'success'){
                  var harga = response.data.harga;

                  if (jumlah == "") {
                    var total = response.data.harga;
                  }else{
                    var total = response.data.harga * jumlah;
                  }

                  $('#total').val(ribuan(total));
                } else {
                  return "";
                }
              }
            });
        }

      function ribuan(nominal) {
          var bilangan = nominal;
          var reverse = bilangan.toString().split('').reverse().join(''),
          ribuan  = reverse.match(/\d{1,3}/g);
          ribuan  = ribuan.join('.').split('').reverse().join('');
          return "Rp "+ ribuan;
      }

      $(document).on("submit", "#form-pesanan", function(e){
        e.preventDefault()
      swal({
        title:"Konfirmasi Pesanan",
        text:"Yakin data sudah benar ?",
        type: "info",
        showCancelButton: true,
        confirmButtonText: "Konfirmasi",
        closeOnConfirm: true,
      },
        function(){
         $.ajax({
          url:"<?php echo base_url('simpan-pesanan-offline'); ?>",
          method: "POST",
          data: new FormData($('#form-pesanan')[0]),
              contentType: false,
              processData: false,
          success: function(response) { 
            var data = $.parseJSON(response);
                if(data.status == 'success'){
                    window.setTimeout(function(){
                      swal({
                        title: "Berhasil!",
                        text: "Pemesanan Berhasil!",
                        type: "success"
                    }, function() {
                        window.location.href = "<?=base_url('pemesanan')?>";
                    });
                    }, 2000);
                } else {
                  swal({
                      title: "Maaf!",
                      text: "Silahkan Login Terlebih Dahulu!",
                      type: "warning"
                  });
                }
              }
         });
      });
    });
    </script>
  </body>
</html>