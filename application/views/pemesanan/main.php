<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view("component/head.php") ?>
      <link rel="stylesheet" type="text/css" href="./dist/css/pagination.css">
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
      <script src="<?php echo base_url('assets/plugins/sweetalert/dist/sweetalert.min.js'); ?>"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/sweetalert/dist/sweetalert.css'); ?>">
  </head>
  <body>
    <div class="wrapper">
      <?php $this->load->view("component/sidebar.php") ?>

      <div id="content">
        <?php $this->load->view("component/navbar.php") ?>

      

        <section class="content-header">
        <h1><?=$title1?></h1>
            <ol class="breadcrumb" style="margin-bottom: 5px;">
                <li class="breadcrumb-item pl-3"><a href="<?=base_url('/dashboard')?>">Dashboard</a></li>
                <li class="breadcrumb-item active"><?=$title1;?></li>
            </ol>
        </section>

        <section class="content">
          <div class="row">
            <div class="col-md-12">

              <div class="box main-layer">
                <div class="box-header with-border my-1">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="<?=base_url('listproduk')?>" class="btn btn-primary btn-add"><i class="fa fa-plus"></i> Tambah</a>
                        </div>
                        <dic class="col-md-6">
                            <div class="form-inline float-right">
                                <div class="form-group">
                                    <select class="form-control search-option"></select>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control search-input" placeholder="Search...">
                                </div>
                            </div>
                        </dic>
                    </div>
                </div>

                <p class="text-muted"><?php echo $this->session->flashdata('msg'); ?></p>

                <div class="box-body table-responsive">
                  <table class="table table-striped table-light table-sm text-dark" id="datagrid"></table>
                </div>

                <div class="box-footer clearfix">
                  <div class="row">
                    <div class="col-md-4 form-inline">
                      <div class="form-group">
                        <select class="form-control option"></select>
                      </div>
                    </div>
                    <div class="col-md-4 text-center info"></div>
                    <div class="col-md-4">
                      <ul class="pagination pagination-sm no-margin pull-right pagination-main"></ul>
                    </div>
                  </div>
                </div>
              </div>

              <div class="other-page"></div>
                <div class="modal-dialog"></div>

            </div>
          </div>
        </section>
            
      </div>

        <!-- <?php $this->load->view("component/footer.php") ?> -->
    </div>

   <script type="text/javascript">
      $(document).ready(function () {
          $('#sidebarCollapse').on('click', function () {
              $('#sidebar').toggleClass('active');
          });
      });

      var datagrid;
      $(document).ready(function() {
        datagrid = $("#datagrid").datagrid({
          url                       : '<?php echo base_url("get-pemesanan?idKaryawan=".$karyawan_id); ?>', 
          primaryField          : 'id_pemesanan', 
          rowNumber             : true,
          searchInputElement  : '.search-input',
          searchFieldElement  : '.search-option',
          pagingElement       : '.pagination-main',
          optionPagingElement : '.option',
          pageInfoElement     : '.info',
      		itemsPerPage	        : 10,
          columns               : [
              {field: 'tgl', title: 'Tanggal', editable: false, sortable: true, width: 100, align: 'center', search: true,
              	rowStyler: function(rowData, rowIndex) {
                     return tgl(rowData, rowIndex);
                  }
            	},
               {field: 'nama_barang', title: 'Nama Barang', editable: false, sortable: true, width: 200, align: 'left', search: true},
              {field: 'kode', title: 'Kode Pemesanan', editable: false, sortable: true, width: 200, align: 'left', search: true},
              {field: 'status', title: 'Status', editable: false, sortable: true, width: 180, align: 'center', search: true},
              {field: 'harga', title: 'Biaya', editable: false, sortable: false, width: 80, align: 'right', search: false,
                rowStyler: function(rowData, rowIndex) {
                       return harga(rowData, rowIndex);
                    }
                },
              {field: 'tambahan', title: 'Biaya Tambahan', editable: false, sortable: false, width: 80, align: 'right', search: false,
                rowStyler: function(rowData, rowIndex) {
                       return tambahan(rowData, rowIndex);
                    }
                },
             	{field: 'pemroses', title: 'Diproses Oleh', editable: false, sortable: false, width: 80, align: 'center', search: false,
              rowStyler: function(rowData, rowIndex) {
                     return pemroses(rowData, rowIndex);
                  }
              },
              {field: 'status_bayar', title: 'Status Bayar', editable: false, sortable: false, width: 80, align: 'center', search: false,
              rowStyler: function(rowData, rowIndex) {
                     return status_bayar(rowData, rowIndex);
                  }
              },
              {field: 'menu', title: 'Menu', sortable: false, width: 200, align: 'center', search: false, 
                  rowStyler: function(rowData, rowIndex) {
                    return menu(rowData, rowIndex);
                  }
                }
            ]
          });
        datagrid.run();
      });


      function images(rowData, rowIndex) {
        var gmbr = rowData.gambar;
        if (gmbr == null) {
            return '<span><img height="50px" src="<?=base_url('dist/img/default.jpg')?>"></span>';
        }else{
            return '<span><img height="50px" src="<?=base_url('dist/img/barang/')?>'+gmbr+'"></span>';
        }
      }

      function tgl(rowData, rowIndex) {
      	var tgl = rowData.tanggal;
	      var pecahTgl = tgl.split(' ');
	      var pecahTgl2 = pecahTgl[0].split('-');
	      return pecahTgl2[2]+'/'+pecahTgl2[1]+'/'+pecahTgl2[0];
      }

      function harga(rowData, rowIndex) {
        var hrg = rowData.total_harga;
        if (hrg == null) {
            return '<span>'+0+'</span>';
        }else{
            return '<span>'+hrg+'</span>';
        }
      }

      function tambahan(rowData, rowIndex) {
        var tmbh = rowData.biaya_tambahan;
        if (tmbh == null) {
            return '<span>'+0+'</span>';
        }else{
            return '<span>'+tmbh+'</span>';
        }
      }

      function status_bayar(rowData, rowIndex) {
        var tmbh = rowData.biaya_tambahan;
        var total_harga = rowData.total_harga;
        var sudah_bayar = rowData.sudah_bayar;
        
        var kurang = parseInt(total_harga) + parseInt(tmbh) - parseInt(sudah_bayar);

        var tag = '<span style="font-size: 14px;" class="badge badge-success"> sudah dibayar : ' +sudah_bayar+'</span>'+
        				'<span style="font-size: 14px;" class="badge badge-danger"> kurang : ' +kurang+'</span>';
        return tag;
      }

      function pemroses(rowData, rowIndex) {
        var nm = rowData.nama;
        if (nm == null) {
            return '<span class="badge badge-success">belum ditentukan</span>';
        }else{
            return '<span>'+nm+'</span>';
        }
      }

      function menu(rowData, rowIndex) {
      	var lvl = <?=$this->session->userdata('auth_level_user');?>;
      	var menu = '';
      	if (rowData.status != 'Selesai') {
      		if (rowData.status == 'dibuat' && lvl == 1) {
	      		menu += 
	      		'<div class="btn-group"><a onclick="detail('+rowData.id_pemesanan+')" href="javascript:void(0);" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> ubah status</a>';
	        }else if(rowData.status != 'dibuat' && lvl != 1){
	        	menu += 
	      		'<div class="btn-group"><a onclick="detail('+rowData.id_pemesanan+')" href="javascript:void(0);" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> ubah status</a>';
        		menu += '<div class="btn-group"><a onclick="show('+rowData.id_pemesanan+')" href="javascript:void(0);" class="btn btn-sm btn-secondary"><i class="fa fa-info"></i> Detail</a>';
	        }else{
	        	menu += '<div class="btn-group"><a onclick="show('+rowData.id_pemesanan+')" href="javascript:void(0);" class="btn btn-sm btn-secondary"><i class="fa fa-info"></i> Detail</a>';
	        }
	        
      	}else{
         	menu += '<div class="btn-group"><a onclick="show('+rowData.id_pemesanan+')" href="javascript:void(0);" class="btn btn-sm btn-secondary"><i class="fa fa-info"></i> Detail</a>';
      	}
       
        return menu;
      }

      function detail(id) {
            $.ajax({
            type: 'POST',
            url: '<?php echo site_url("detail-pemesanan")?>',
            async : true,
          dataType : 'json',
            data: {id:id},
            success: function(response) { 
                if(response.status == 'success'){
                  $('.modal-dialog').html(response.content);
                } else {
                  window.setTimeout(function(){
		                swal({
		                  title: "Maaf!",
		                  text: response.message,
		                  type: "warning"
		                }, function() {
		                    location.reload();
		                });
		              }, 500);
                }
            }
        });
        }

      function show(id) {
            $.ajax({
            type: 'POST',
            url: '<?php echo site_url('show-pemesanan')?>',
            async : true,
          dataType : 'json',
            data: {id:id},
            success: function(response) { 
                if(response.status == 'success'){
                  $('.modal-dialog').html(response.content);
                } else {
                  return "";
                }
            }
        });
        }

    </script>
  </body>
</html>