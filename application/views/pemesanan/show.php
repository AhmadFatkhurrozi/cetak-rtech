<div class="modal fade" id="detail-dialog" tabindex="-1" role="dialog" aria-labelledby="product-detail-dialog">
  <div class="modal-dialog modal-lg" >
    <div class="modal-content">
      <div class="modal-header" style="width: 100%">
          <?=$title;?>
        <span style="float:right"><a data-dismiss="modal">Close</a></span>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="row">
              <input type="hidden" name="id_pemesanan" id="id_pemesanan" value="<?=$pemesanan->id_pemesanan;?>">
              <label class="col-md-4">Produk</label>
              <label class="col-md-8"> : <?=$pemesanan->nama_barang;?></label>
            </div>

            <div class="row">
              <label class="col-md-4">Jumlah</label>
              <label class="col-md-8"> : <?=$pemesanan->jumlah;?></label>
            </div>

            <div class="row">
              <label class="col-md-4">Ukuran</label>
              <label class="col-md-8"> : <?=$pemesanan->ukuran;?></label>
            </div>
          </div>
          
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="row">
              <label class="col-md-4">Total Harga</label>
              <label class="col-md-8"> : <?=$pemesanan->total_harga;?></label>
            </div>
            
            <div class="row">
              <label class="col-md-4">Nama</label>
              <label class="col-md-8"> : <?=$user;?></label>
            </div>

            <div class="row">
              <label class="col-md-4">WhatsApp</label>
              <label class="col-md-8"> : <?=$pemesanan->no_wa;?></label>
            </div>
          </div>

         
      </div>

      </div>
      <div class="clearfix" style='padding-bottom:20px'></div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('#detail-dialog').find('.modal-dialog').css({
      'width'     : '80%'
    });
    $('#detail-dialog').modal('show');
  })();

  $('#detail-dialog').on('hidden.bs.modal', function () {
    $('.modal-dialog').html('');
  });

</script>
