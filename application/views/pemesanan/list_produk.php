<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view("component/head.php") ?>
      <link rel="stylesheet" type="text/css" href="./dist/css/pagination.css">
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
      <script src="<?php echo base_url('assets/plugins/sweetalert/dist/sweetalert.min.js'); ?>"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/sweetalert/dist/sweetalert.css'); ?>">
  </head>
  <body>
    <div class="wrapper">
      <?php $this->load->view("component/sidebar.php") ?>

      <div id="content">
        <?php $this->load->view("component/navbar.php") ?>

      

        <section class="content-header">
            <h1><?=$title1?></h1>
                <ol class="breadcrumb" style="margin-bottom: 5px;">
                        <li class="breadcrumb-item pl-3"><a href="<?=base_url('/dashboard')?>">Dashboard</a></li>
                        <li class="breadcrumb-item active"><?=$title1;?></li>
                    </ol>
                </section>

        <section class="content">
          <hr class="featurette-divider">
          <div class="container" style="padding-top: 10px;">
              <h3>Produk</h3><br/>
              <div class="row">
                  <?php if ($produk->num_rows() > 0){ ?>
                      <?php foreach ($produk->result() as $r){ ?>
                          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 feature-grid" style="width: 100% !important;">
                              <a href="<?php echo base_url('admin-checkout/'.$r->id_barang) ?>">
                                  <?php if($r->gambar !== null){?>
                                      <img width="100%" height="256" style="object-fit: cover;" class="produkTerbaru" src="<?=base_url('dist/img/barang/'.$r->gambar)?>" alt=""/> 
                                  <?php }else{ ?>
                                      <img width="100%" height="256" style="object-fit: cover;" class="produkTerbaru" src="<?=base_url('dist/img/default.jpg')?>" alt=""/> 
                                  <?php } ?>

                                  <div class="arrival-info text-center">
                                      <h4><?php echo $r->nama_barang; ?></h4>
                                      <!-- <p>Harga belum Tersedia</p> -->
                                  </div>
                              </a>
                          </div>
                      <?php }} ?>
                  </div>
              </div>

              <div class="card-footer">
                  <!-- <button type="submit" class="btn btn-primary btn-submit btn-sm pull-right ml-2">Simpan <span class="fa fa-save"></span></button> -->
                  <a href="<?=base_url('pemesanan')?>" class="btn btn-warning btn-cancel btn-sm pull-right"><span class="fa fa-chevron-left"></span> Kembali</a>
              </div>
          </div>
        </section>
            
      </div>

        <!-- <?php $this->load->view("component/footer.php") ?> -->
    </div>

   <script type="text/javascript">

    </script>
  </body>
</html>