<div class="modal fade" id="detail-dialog" tabindex="-1" role="dialog" aria-labelledby="product-detail-dialog">
  <div class="modal-dialog modal-lg" >
    <div class="modal-content">
      <div class="modal-header" style="width: 100%">
          <?=$title;?>
        <span style="float:right"><a data-dismiss="modal">Close</a></span>
      </div>
      <div class="modal-body">
        <form action="<?=base_url('doUploadBukti')?>" method="POST" enctype="multipart/form-data">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
              <input type="hidden" name="id_pemesanan" id="id_pemesanan" value="<?=$pemesanan->id_pemesanan;?>">
              <label class="col-md-4">Produk</label>
              <label class="col-md-8"> : <?=$pemesanan->nama_barang;?></label>
            </div>

            <div class="row">
              <label class="col-md-4">Kode Pemesanan</label>
              <label class="col-md-8"> : <?=$pemesanan->kode;?></label>
            </div>
                  
            <div class="row">
              <label class="col-md-4">Bukti</label>
              <label class="col-md-8">
                <input type="file" name="bukti" class="form-control">
              </label>
            </div>
          </div>

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-2">
             <button type="submit" class="btn btn-success btn-sm float-right pull-right">Simpan</button>  
          </div>
      </div>
      </form>

      </div>
      <div class="clearfix" style='padding-bottom:20px'></div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('#detail-dialog').find('.modal-dialog').css({
      'width'     : '80%'
    });
    $('#detail-dialog').modal('show');
  })();

  $('#detail-dialog').on('hidden.bs.modal', function () {
    $('.modal-dialog').html('');
  });
</script>
