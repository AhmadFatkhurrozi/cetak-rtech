<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view("component/head.php") ?>
      <link rel="stylesheet" type="text/css" href="./dist/css/pagination.css">
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
      <script src="<?php echo base_url('assets/plugins/sweetalert/dist/sweetalert.min.js'); ?>"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/sweetalert/dist/sweetalert.css'); ?>">
  </head>
  <body>
    <div class="wrapper">
      <?php $this->load->view("component/sidebar.php") ?>

      <div id="content">
        <?php $this->load->view("component/navbar.php") ?>

      

        <section class="content-header">
        <h1><?=$title1?></h1>
            <ol class="breadcrumb" style="margin-bottom: 5px;">
                <li class="breadcrumb-item pl-3"><a href="<?=base_url('/dashboard')?>">Dashboard</a></li>
                <li class="breadcrumb-item active"><?=$title1;?></li>
            </ol>
        </section>

        <section class="content">
          <div class="row">
            <div class="col-md-12">

              <div class="box main-layer">
                <div class="box-header with-border my-1">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="<?=base_url('listproduk')?>" class="btn btn-primary btn-add"><i class="fa fa-plus"></i> Tambah</a>
                        </div>
                        <dic class="col-md-6">
                            <div class="form-inline float-right">
                                <div class="form-group">
                                    <select class="form-control search-option"></select>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control search-input" placeholder="Search...">
                                </div>
                            </div>
                        </dic>
                    </div>
                </div>

                <p class="text-muted"><?php echo $this->session->flashdata('msg'); ?></p>

                <div class="box-body table-responsive">
                  <table class="table table-striped table-light text-dark" id="datagrid"></table>
                </div>

                <div class="box-footer clearfix">
                  <div class="row">
                    <div class="col-md-4 form-inline">
                      <div class="form-group">
                        <select class="form-control option"></select>
                      </div>
                    </div>
                    <div class="col-md-4 text-center info"></div>
                    <div class="col-md-4">
                      <ul class="pagination pagination-sm no-margin pull-right pagination-main"></ul>
                    </div>
                  </div>
                </div>
              </div>

              <div class="other-page"></div>
                <div class="modal-dialog"></div>

            </div>
          </div>
        </section>
            
      </div>

        <!-- <?php $this->load->view("component/footer.php") ?> -->
    </div>

   <script type="text/javascript">
      $(document).ready(function () {
          $('#sidebarCollapse').on('click', function () {
              $('#sidebar').toggleClass('active');
          });
      });

      var datagrid;
      $(document).ready(function() {
        datagrid = $("#datagrid").datagrid({
          url                       : '<?php echo base_url('getPekerjaanSaya'); ?>', 
          primaryField          : 'id_pemesanan', 
          rowNumber             : true,
          searchInputElement  : '.search-input',
          searchFieldElement  : '.search-option',
          pagingElement       : '.pagination-main',
          optionPagingElement : '.option',
          pageInfoElement     : '.info',
          columns               : [
          		{field: 'tgl', title: 'Tanggal', editable: false, sortable: true, width: 100, align: 'center', search: true,
              	rowStyler: function(rowData, rowIndex) {
                     return tgl(rowData, rowIndex);
                  }
            	},
              {field: 'nama_barang', title: 'Nama Barang', editable: true, sortable: true, width: 200, align: 'left', search: true},
              {field: 'kode', title: 'Kode Pemesanan', editable: true, sortable: true, width: 200, align: 'left', search: true},
              {field: 'status', title: 'Status', editable: true, sortable: true, width: 180, align: 'center', search: true},
              {field: 'buktinya', title: 'Bukti Pekerjaan', editable: true, sortable: false, width: 80, align: 'center', search: false,
                rowStyler: function(rowData, rowIndex) {
                       return buktinya(rowData, rowIndex);
                    }
              },
            ]
          });
        datagrid.run();
      });


      function buktinya(rowData, rowIndex) {
        var bukti = rowData.bukti;
        if (bukti == null) {
            return '<a href="javascript:void(0)" class="btn btn-success btn-sm" onclick="uploadBukti('+rowData.id_pemesanan+')">Upload Bukti<a>';
        }else{
            return '<span><img height="50px" src="<?=base_url('upload/bukti/')?>'+bukti+'"></span>';
        }
      }

      function tgl(rowData, rowIndex) {
      	var tgl = rowData.tanggal;
	      var pecahTgl = tgl.split(' ');
	      var pecahTgl2 = pecahTgl[0].split('-');
	      return pecahTgl2[2]+'/'+pecahTgl2[1]+'/'+pecahTgl2[0];
      }

      function uploadBukti(id) {
         $.ajax({
            type: 'POST',
            url: '<?php echo site_url("upload-bukti")?>',
            async : true,
          dataType : 'json',
            data: {id:id},
            success: function(response) { 
                if(response.status == 'success'){
                  $('.modal-dialog').html(response.content);
                } else {
                  window.setTimeout(function(){
                    swal({
                      title: "Maaf!",
                      text: response.message,
                      type: "warning"
                    }, function() {
                        location.reload();
                    });
                  }, 500);
                }
            }
        });
      }
    </script>
  </body>
</html>