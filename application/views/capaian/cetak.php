<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>REKAP CAPAIAN KARYAWAN</title>
    <style media="screen">
      table {
          font-family: arial, sans-serif;
          border-collapse: collapse;
          width: 100%;
          padding: 40px; 
      } 
      .detail, .detail tbody td,.detail tbody tr, .detail thead tr,.detail thead th{
        /*border :0.5px solid black;*/
        border-spacing: 0.5px;
        /*text-align: center;*/
        padding-top: 5px;
        padding-bottom: 5px;
        font-size: 12px;
      }
      .detail thead tr {
        background-color: salmon;
        color: #fff;
      }
      .detail tbody td{
        padding : 5px;
        /*text-align: center;*/
      }
      .detail tbody tr:nth-child(odd){
        background: #ebebeb;
      }
      .detail tbody tr:nth-child(even){
        background: #d7d7d7;
      }
      @page { margin: 5px; }
      body{margin:5px;}
      .title, .dept {
        font-size: 18px;
        font-weight: bold;
        width: 100%;
      }
      .dept {
        font-size: 13px !important;
      }
    </style>
  </head>
  <body>
    <div style="margin: 30px; text-align: center;">
      <div class="title">REKAP CAPAIAN KARYAWAN</div>
      <?php if(!empty($this->input->GET('bulan'))){ ?>
      <div class="dept">Periode <?=$judul?></div>
    	<?php } ?>
    </div>
    <table class='detail' style="border-collapse: collapse;">
      <thead>
        <tr>
          <th><center>No</center></th>
          <th><center>Nama</center></th>
          <th><center>Capaian</center></th>
        </tr>
      </thead>
      <tbody>
        <?php $no = 1; ?>
        <?php if(count($data) > 0){ 
            foreach($data as $a) { 
            	if ($getbulan != '') {
              	$jumlah = $this->db->like('tanggal', $getbulan)
                                ->where('karyawan_id', $a->id)
                                ->get('pemesanan')->num_rows();
            	}else{
            		$jumlah = $this->db->where('karyawan_id', $a->id)
                                ->get('pemesanan')->num_rows();
            	}?>

                                
              <tr>
                <td align='center'><?=$no++?></td>
                <td align='center'><?=$a->nama;?></td>
                <td align='center'><?=$jumlah;?></td>
              </tr>
            <?php }}else{ ?>
          <tr>
            <td colspan="3" align="center"><i>- Data Tidak Ada -</i></td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </body>
</html>
