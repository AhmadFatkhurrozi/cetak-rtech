<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view("component/head.php") ?>
      <link rel="stylesheet" type="text/css" href="<?=base_url('dist/css/pagination.css');?>">
      <link rel="stylesheet" type="text/css" href="<?=base_url('dist/css/animate.min.css');?>">
    <link rel="stylesheet" type="text/css" href="https://app.cahayakhitan.com/css/bootstrap-datetimepicker.min.css">

  </head>
  <body>
    <div class="wrapper">
      <?php $this->load->view("component/sidebar.php") ?>

      <div id="content">
        <?php $this->load->view("component/navbar.php") ?>

      

        <section class="content-header">
            <h1><?=$title1?></h1>
                <ol class="breadcrumb" style="margin-bottom: 5px;">
                        <li class="breadcrumb-item pl-3"><a href="<?=base_url('/dashboard')?>">Dashboard</a></li>
                        <li class="breadcrumb-item active"><?=$title1;?></li>
                    </ol>
                </section>

        <section class="content">
          <div class="row">
            <div class="col-md-12">

              <div class="box main-layer">
                <div class="box-header with-border my-1">
                    <div class="row">
                        <div class="col-md-6">
                          <!--   <a href="javascript:void(0)" class="btn btn-primary btn-add"><i class="fa fa-plus"></i> Tambah</a> -->
                          <div class="row">
							            	<div class="col-md-6">
									            <div class="form-group">
				                          <input type="text" class="input-sm form-control bulan" data-date-format="yyyy-mm" placeholder="periode">
				                      </div>
							            	</div>
							            	<div class="col-md-6">
							            		<button type="button" class="btn btn-danger doPrint" title="Cetak"><i class="fa fa-print"></i>&nbsp; Cetak </button>
							            	</div>
							            </div>
                        </div>
                        <dic class="col-md-6">
                            <div class="form-inline float-right">
                                <div class="form-group">
                                    <select class="form-control search-option"></select>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control search-input" placeholder="Search...">
                                </div>
                            </div>
                        </dic>
                    </div>
                </div>

                <p class="text-muted"><?php echo $this->session->flashdata('msg'); ?></p>

                <div class="box-body table-responsive">
                  <table class="table table-striped table-light text-dark" id="datagrid"></table>
                </div>

                <div class="box-footer clearfix">
                  <div class="row">
                    <div class="col-md-4 form-inline">
                      <div class="form-group">
                        <select class="form-control option"></select>
                      </div>
                    </div>
                    <div class="col-md-4 text-center info"></div>
                    <div class="col-md-4">
                      <ul class="pagination pagination-sm no-margin pull-right pagination-main"></ul>
                    </div>
                  </div>
                </div>
              </div>

              <div class="other-page"></div>
                <div class="modal-dialog"></div>

            </div>
          </div>
        </section>
            
      </div>

        <!-- <?php $this->load->view("component/footer.php") ?> -->
    </div>

    <script type="text/javascript" src="https://app.cahayakhitan.com/js/bootstrap-datetimepicker.min.js"></script>
   <script type="text/javascript">
      $(document).ready(function () {
          $('#sidebarCollapse').on('click', function () {
              $('#sidebar').toggleClass('active');
          });
      });

      var datagrid;
      $(document).ready(function() {
        datagrid = $("#datagrid").datagrid({
          url                       : '<?php echo base_url("get-capaian"); ?>', 
          primaryField          : 'id', 
          rowNumber             : true,
          searchInputElement  : '.search-input',
          searchFieldElement  : '.search-option',
          pagingElement       : '.pagination-main',
          optionPagingElement : '.option',
          pageInfoElement     : '.info',
          queryParams             : {
		          'periode'    : $('.bulan').val(),
		      },
          columns               : [
              {field: 'nama', title: 'Nama Karyawan', editable: false, sortable: true, width: 100, align: 'left', search: true},
              {field: 'jumlah', title: 'Capaian', editable: false, sortable: false, width: 180, align: 'center', search: false},
              {field: 'menu', title: 'Menu', sortable: false, width: 200, align: 'center', search: false, 
                  rowStyler: function(rowData, rowIndex) {
                    return menu(rowData, rowIndex);
                  }
              }
            ]
          });
        datagrid.run();
      });

      $(".bulan").on("change", function(){
	        datagrid.queryParams({
	            'periode'    : $('.bulan').val()
	        });
	        datagrid.reload();
	    });

       function menu(rowData, rowIndex) {
       	var idKaryawan = rowData.id;
        var menu = 
        '<div class="btn-group"><a href="<?=base_url('detail-capaian?idKaryawan=')?>'+idKaryawan+'" class="btn btn-sm btn-primary"><i class="fa fa-info"></i> Detail</a>'+
        '</div>';

        return menu;
      }

      $('.bulan').datetimepicker({
        weekStart: 2,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 4,
        minView: 3,
        forceParse: 0,
      });

       $('.doPrint').click(function() {
        var bulan = $('.bulan').val();
        window.open("<?=base_url('cetak-capaian')?>?bulan="+bulan, '_blank');
      });

    </script>
  </body>
</html>