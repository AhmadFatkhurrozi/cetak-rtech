<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view("component/head.php") ?>
      <link rel="stylesheet" type="text/css" href="./dist/css/pagination.css">
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
      <script src="<?php echo base_url('assets/plugins/sweetalert/dist/sweetalert.min.js'); ?>"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/sweetalert/dist/sweetalert.css'); ?>">
  </head>
  <body>
    <div class="wrapper">
      <?php $this->load->view("component/sidebar.php") ?>

      <div id="content">
        <?php $this->load->view("component/navbar.php") ?>

      

        <section class="content-header">
        <h1><?=$title1?></h1>
            <ol class="breadcrumb" style="margin-bottom: 5px;">
                <li class="breadcrumb-item pl-3"><a href="<?=base_url('/dashboard')?>">Dashboard</a></li>
                <li class="breadcrumb-item active"><?=$title1;?></li>
            </ol>
        </section>

        <section class="content">
          <div class="row">
            <div class="col-md-12">

              <div class="box main-layer">
                <div class="box-header with-border my-1">
                    <div class="row">
                      <div class="col-lg-2 col-md-12 col-sm-12 col-12 text-center py-2 mt-2">
                        <label>Pilih Periode</label>
                      </div> 
                      <div class="col-lg-3 col-md-12 col-sm-12 col-12 mt-2">
                        <input type="date" class="form-control form-inline" id="awal" name="awal" placeholder="dari">
                      </div>   
                      <div class="col-lg-1 col-md-12 col-sm-12 col-12 text-center py-2 mt-2">
                        <label>s/d</label>
                      </div>
                      <div class="col-lg-3 col-md-12 col-sm-12 col-12 mt-2">
                        <input type="date" class="form-control form-inline" id="akhir" name="akhir" placeholder="sampai">
                      </div>  
                      <div class="col-lg-2 col-md-12 col-sm-12 col-12 mt-2">
                        <a href="javascript:void(0)" class="btn btn-danger btn-block doPrint"><i class="fa fa-print"></i></a>
                      </div>  
                    </div>

                </div>

                <p class="text-muted"><?php echo $this->session->flashdata('msg'); ?></p>
              </div>
            </div>
          </div>
        </section>
            
      </div>

        <!-- <?php $this->load->view("component/footer.php") ?> -->
    </div>

   <script type="text/javascript">
      $(document).ready(function () {
          $('#sidebarCollapse').on('click', function () {
              $('#sidebar').toggleClass('active');
          });
      });

      $('.doPrint').click(function() {
        var awal = $('#awal').val();
        var akhir = $('#akhir').val();
        window.open("<?=base_url('cetak-capaian')?>?awal="+awal+"&akhir="+akhir, '_blank');
      });
    </script>
  </body>
</html>