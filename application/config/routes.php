<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'FrontEndController';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


$route['admin']         = 'LoginController';
$route['admin-logout']  = 'LoginController/logout';

// USER LOGIN
$route['login']         = 'FrontEndController/login';
$route['logout']         = 'FrontEndController/logout';
$route['register']      = 'FrontEndController/register';
$route['simpan-pelanggan'] = 'FrontEndController/simpanPelanggan';
$route['cek-email'] = 'FrontEndController/cekEmail';
$route['cek-username'] = 'FrontEndController/cekUsername';

$route['dashboard']     = 'DashboardController';
$route['chartBulanan'] 	= 'DashboardController/chartBulanan';

// FRONT END PRODUK KAMI
$route['produk-kami']   = 'MenuPelanggan/ProdukController';
$route['kategori/:any']   = 'MenuPelanggan/ProdukController/kategori';
$route['detail-produk/:any']   = 'MenuPelanggan/ProdukController/detail';

$route['pesanan-saya']   = 'MenuPelanggan/PesananController';
$route['checkout/:any']   = 'MenuPelanggan/PesananController/checkout';
$route['simpan-pesanan']   = 'MenuPelanggan/PesananController/simpan';
$route['status/:any']= 'MenuPelanggan/PesananController/cekStatus';
$route['cek-pesanan']= 'MenuPelanggan/PesananController/cekPesanan';
$route['getHarga']= 'MenuPelanggan/PesananController/getHarga';

// Data Pengguna
$route['user'] 	= 'UserController';
$route['get-user']= 'UserController/data';
$route['form-add-user']= 'UserController/form';
$route['save-user']= 'UserController/save';
$route['detail-user']= 'UserController/detail';
$route['hapus-user']= 'UserController/hapus';
$route['edit-profil']   = 'UserController/editMyprofil';
$route['save-profil'] 	= 'UserController/saveMyprofil';

// Data Barang
$route['barang']  = 'BarangController';
$route['get-barang']= 'BarangController/data';
$route['form-add-barang']= 'BarangController/form';
$route['save-barang']= 'BarangController/save';
$route['detail-barang']= 'BarangController/detail';
$route['hapus-barang']= 'BarangController/hapus';

// Data Kategori
$route['kategori']  = 'KategoriController';
$route['get-kategori']= 'KategoriController/data';
$route['form-add-kategori']= 'KategoriController/form';
$route['save-kategori']= 'KategoriController/save';
$route['detail-kategori']= 'KategoriController/detail';
$route['hapus-kategori']= 'KategoriController/hapus';

$route['setting-harga/:any']= 'HargaController';
$route['get-harga']= 'HargaController/data';
$route['form-add-harga/:any']= 'HargaController/form';
$route['save-harga']= 'HargaController/save';
$route['hapus-harga']= 'HargaController/hapus';

$route['pemesanan']  = 'PemesananController';
$route['get-pemesanan']= 'PemesananController/data';
$route['listproduk']= 'PemesananController/listproduk';
$route['detail-pemesanan']= 'PemesananController/ubahStatus';
$route['show-pemesanan']= 'PemesananController/show';
$route['ubah-status-pesanan']= 'PemesananController/valStatus';
$route['admin-checkout/:any']   = 'PemesananController/checkout';
$route['simpan-pesanan-offline']   = 'PemesananController/simpanpesanan';

$route['pekerjaan-saya'] = 'PemesananController/pekerjaanSaya';
$route['getPekerjaanSaya']= 'PemesananController/dataKu';
$route['upload-bukti']= 'PemesananController/uploadBukti';
$route['doUploadBukti']= 'PemesananController/doUploadBukti';

$route['detail-capaian'] = 'PemesananController/detailCapaian';
$route['get-detail-capaian'] = 'PemesananController/getDetailCapaian';

$route['capaian-karyawan'] = 'CapaianController';
$route['get-capaian'] = 'CapaianController/data';
$route['rekap-capaian-karyawan'] = 'CapaianController/rekap';
$route['cetak-capaian'] = 'CapaianController/cetak';

$route['neraca'] = 'NeracaController';
$route['get-neraca'] = 'NeracaController/data';
$route['getTotalNeraca'] = 'NeracaController/total';
$route['rekap-pesanan'] = 'NeracaController/rekap';
$route['cetak-pesanan'] = 'NeracaController/cetak';







// Data Pelanggan
$route['pelanggan']  = 'PelangganController';
$route['get-pelanggan']= 'PelangganController/data';
$route['detail-pelanggan']= 'PelangganController/detail';
$route['add-member']= 'PelangganController/member';
$route['hapus-member']= 'PelangganController/hapusmember';
